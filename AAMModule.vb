﻿Imports Microsoft.Exchange.WebServices
Imports Microsoft.Exchange.WebServices.Data
Imports Microsoft.Exchange.WebServices.Auth

Imports System.Data.OleDb
Module AAMModule
    Public kodeSite As String
    Public siteName As String
    Public username As String
    Public displayName As String
    Public password As String
    Public DBName As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=DB_ASSET_AUDIT_V2.accdb"
    Public Conn As OleDbConnection
    Public cmd As OleDbCommand
    Public cmdStr As String = ""

    Public Function Koneksi()
        Try
            Conn = New OleDbConnection(DBName)
            If Conn.State = ConnectionState.Closed Then
                Conn.Open()
            End If
        Catch ex As Exception
            MsgBox("Connection Failed")
        End Try
    End Function

    Public Function getCount(tblName As String, field As String, field2 As String, keyword As String, keyword2 As String, type As String, distinctField As String)
        Dim count As Int16
        If type = "count" Then
            cmdStr = "select count(*) From " + tblName + " Where [" + field + "] LIKE '%" + keyword + "%'"
        Else
            cmdStr = "select distinct(" + distinctField + ") FROM " + tblName + " WHERE [" + field + "] LIKE '%" + keyword + "%' and [" + field2 + "]  LIKE '%" + keyword2 + "%'"
        End If
        Call Koneksi()
            cmd = New OleDbCommand(cmdStr, Conn)
            count = Convert.ToInt16(cmd.ExecuteScalar())
            Conn.Close()
            Return count.ToString
    End Function

    Public Function delete(tblName As String, field As String, keyword As String)
        Dim strDel As String = "delete * from  " + tblName + " where [" + field + "]=?"
        cmd = New OleDbCommand(strDel, AAMModule.Conn)
        cmd.Parameters.AddWithValue(field, keyword)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
    End Function

    Public Function insert(query As String)
        cmd = New OleDbCommand(query, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
    End Function

    Public Function update(query As String)
        cmd = New OleDbCommand(query, Conn)
        cmd.ExecuteNonQuery()
        cmd.Dispose()
    End Function


    'Public datetimeOldAsset As String
    'Public max As Boolean = False
    'Public min As Boolean = True
    'Public cekYes As String
    'Public cekNo As String
    'Public totalGetComp As Integer
    'Public dataPerKapan As String 'untuk ambil tanggal yang akan di buat report
    'Public totalUserLtp As String

    ''-variabel untuk list server dso
    'Public currentDateBackupServerSummary As String
    'Public statusBackupServerSummary As String
    'Public tipeBackupServerSummary As String
    'Public tipeSummaryOnBS As String

    ''-variabel untuk list backup laptop
    'Public lokasiBackupLtp As String
    'Public columnTypeBackupLtp As String
    'Public logDateBackupLtp As String
    'Public emailBackupLaptop As String
    'Public domainBackupLtp As String

    'Public logDateDgvDetailLtp As String
    'Public idBiztechDgvDetailLtp As String
    'Public usernameDgvDetailLtp As String
    'Public statusDgvDetailLtp As String
    'Public fullSizeBackupDgvDetailLtp As String
    'Public lessSizeDetailLtp As String
    'Public moreSizeDgvDetailLtp As String
    'Public descDgvDetailLtp As String

    'Public countSuccessNew As Integer
    'Public countFailedNew As Integer
    'Public countCancelledNew As Integer
    'Public countScriptSuccessNew As Integer

    ''public var summary backup laptop
    'Public dataSummary(3) As String

    'Public tipeGetData As String

    Sub Main()
        TestExchangeServerConnection()
    End Sub

    Public Sub TestExchangeServerConnection()
        Dim serverURI As New Uri("https://serverName.com/ews/excange.asmx")
        'Dim exch As New Microsoft.Exchange.WebServices.Data.ExchangeService
        'exch.Url = serverURI
        'exch.UseDefaultCredentials = False
        'exch.Credentials = New System.Net.NetworkCredential

        'Dim iv As ItemView = New ItemView(999)
        'iv.Traversal = ItemTraversal.Shallow
        'Dim deleteItems As FindItemsResults(Of Item) = Nothing
        'deleteItems = exch.FindItems(WellKnownFolderName.DeletedItems, iv)
        'For Each i As Item In deleteItems
        '    Console.WriteLine(i.Subject)
        'Next
    End Sub


End Module
