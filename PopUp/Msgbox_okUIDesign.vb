﻿Public Class Msgbox_okUIDesign
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.Close()
    End Sub

    Private Sub Msgbox_okUIDesign_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        btnOk.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnOk.FlatAppearance.BorderSize = 0
        Panel1.BackColor = Color.FromArgb(255, 225, 229, 242)
    End Sub
End Class