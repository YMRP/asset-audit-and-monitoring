﻿Imports System.ComponentModel
Imports System.Data.OleDb
Public Class Login
    Dim Da As OleDbDataAdapter
    Dim Ds As DataSet

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        If String.IsNullOrEmpty(txtUsername.Text.Trim) Or String.IsNullOrEmpty(txtPassword.Text.Trim) Then

        Else
            Dim username As String = txtUsername.Text
            Dim password As String = txtPassword.Text

            Call Koneksi()
            Dim Sql As String = "SELECT * FROM Master_User where Username ='" + username + "' and Password='" + password + "'"
            Da = New OleDb.OleDbDataAdapter(Sql, Conn)

            Ds = New DataSet
            Da.Fill(Ds, "Master_User")

            If Ds.Tables("Master_User").Rows.Count = 1 Then

                AAMModule.kodeSite = Ds.Tables("Master_User").Rows(0)(0).ToString
                AAMModule.siteName = Ds.Tables("Master_User").Rows(0)(4).ToString
                AAMModule.username = Ds.Tables("Master_User").Rows(0)(2).ToString
                AAMModule.password = Ds.Tables("Master_User").Rows(0)(5).ToString
                AAMModule.displayName = Ds.Tables("Master_User").Rows(0)(1).ToString
                'MsgBox(AAMModule.siteName)
                MasterData.Show()
                Conn.Close()
                Me.Close()
            Else
                alert.Show()
            End If
        End If

    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        alert.Hide()

        Width = Screen.PrimaryScreen.WorkingArea.Width
        Height = Screen.PrimaryScreen.WorkingArea.Height

        Panel2.Location = New Point(Convert.ToInt32(Me.ClientSize.Width / 2 - Me.Panel2.Width / 2),
                                   Convert.ToInt32(Me.ClientSize.Height / 2 - Me.Panel2.Height / 2))
        alert.Location = New Point(Convert.ToInt32(Me.ClientSize.Width / 2 - Me.alert.Width / 2),
                                   Convert.ToInt32(Me.ClientSize.Height / 2 - Me.alert.Height / 2 - Me.Panel2.Height / 2 - Me.alert.Height))
        alert.BackColor = Color.FromArgb(255, 191, 219, 247)
        Panel1.BackColor = Color.FromArgb(255, 225, 229, 242)
        Panel2.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnLogin.BackColor = Color.FromArgb(255, 2, 43, 58)

        txtPassword.AutoSize = False
        txtPassword.Height = 35
        txtPassword.BorderStyle = BorderStyle.None

        txtUsername.AutoSize = False
        txtUsername.Height = 35
        txtUsername.BorderStyle = BorderStyle.None

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub

    Private Sub txtUsername_Validating(sender As Object, e As CancelEventArgs) Handles txtUsername.Validating
        If String.IsNullOrEmpty(txtUsername.Text.Trim) Then
            ErrorProvider1.SetError(txtUsername, "Username is Required")
        Else
            ErrorProvider1.SetError(txtUsername, String.Empty)
        End If
    End Sub

    Private Sub txtPassword_Validating(sender As Object, e As CancelEventArgs) Handles txtPassword.Validating
        If String.IsNullOrEmpty(txtPassword.Text.Trim) Then
            ErrorProvider1.SetError(txtPassword, "Password is Required")
        Else
            ErrorProvider1.SetError(txtPassword, String.Empty)
        End If
    End Sub

    Private Sub txtUsername_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUsername.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnLogin.PerformClick()
        End If
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnLogin.PerformClick()
        End If
    End Sub
End Class