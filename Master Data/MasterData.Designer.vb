﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MasterData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MasterData))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.header = New System.Windows.Forms.Panel()
        Me.lblSite = New System.Windows.Forms.Label()
        Me.btnBackup = New System.Windows.Forms.Button()
        Me.btnValidation = New System.Windows.Forms.Button()
        Me.lblLogout = New System.Windows.Forms.Label()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnMaster = New System.Windows.Forms.Button()
        Me.btnTransaction = New System.Windows.Forms.Button()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.container = New System.Windows.Forms.Panel()
        Me.progressBarAll = New System.Windows.Forms.ProgressBar()
        Me.PercentAll = New System.Windows.Forms.Label()
        Me.btnRefreshAll = New System.Windows.Forms.Button()
        Me.lblDateTimeComp = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnRefreshMasterAsset = New System.Windows.Forms.Button()
        Me.DataGridViewAsset = New System.Windows.Forms.DataGridView()
        Me.IDAssetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDAssetParentDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AssetLokalIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PCNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PTPemilikDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PTPenggunaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubBagianDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LokasiDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PosisiDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KategoriDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeskripsiAssetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MerkDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubKategoriDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KapasitasDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoSeriDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KondisiDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TglPerolehanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JtGaransiDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CatatanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MasterAssetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DBASSETAUDITV2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_ASSET_AUDIT_V2 = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2()
        Me.lblCountAsset = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.lblPercentMAsset = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.comboMasterAsset = New System.Windows.Forms.ComboBox()
        Me.txtSearchMasterAsset = New System.Windows.Forms.TextBox()
        Me.btnSearchAsset = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtSearchComputer = New System.Windows.Forms.TextBox()
        Me.btnSearchComputer = New System.Windows.Forms.Button()
        Me.ProgressBar2 = New System.Windows.Forms.ProgressBar()
        Me.btnRefreshMasterComputer = New System.Windows.Forms.Button()
        Me.lblPercentMComp = New System.Windows.Forms.Label()
        Me.lblCountCmp = New System.Windows.Forms.Label()
        Me.DataGridViewComputer = New System.Windows.Forms.DataGridView()
        Me.ComputerNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MasterComputerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.datetimeandversion = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DataGridViewArea = New System.Windows.Forms.DataGridView()
        Me.AreaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InitialDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MasterAreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.comboMasterArea = New System.Windows.Forms.ComboBox()
        Me.txtMasterArea = New System.Windows.Forms.TextBox()
        Me.btnSearchArea = New System.Windows.Forms.Button()
        Me.btnRefreshMasterArea = New System.Windows.Forms.Button()
        Me.lblPercentMArea = New System.Windows.Forms.Label()
        Me.lblAreaList = New System.Windows.Forms.Label()
        Me.ProgressBar3 = New System.Windows.Forms.ProgressBar()
        Me.lblDateTimeArea = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.DataGridViewUserLaptop = New System.Windows.Forms.DataGridView()
        Me.UsernameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmailUserLaptopDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MasterUserLaptopBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.comboMasterUserLaptop = New System.Windows.Forms.ComboBox()
        Me.txtMasterUserLaptop = New System.Windows.Forms.TextBox()
        Me.btnSearchUserLaptop = New System.Windows.Forms.Button()
        Me.lblPUserLaptop = New System.Windows.Forms.Label()
        Me.btnRefreshMasterUserLaptop = New System.Windows.Forms.Button()
        Me.ProgressBar5 = New System.Windows.Forms.ProgressBar()
        Me.lblUserLtp = New System.Windows.Forms.Label()
        Me.label_copyright = New System.Windows.Forms.Label()
        Me.BackgroundWorkerComputer = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorkerArea = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker4 = New System.ComponentModel.BackgroundWorker()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BackgroundWorker5 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker6 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorkerUserLaptop = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorkerAsset = New System.ComponentModel.BackgroundWorker()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.BackgroundWorkerRefreshAll = New System.ComponentModel.BackgroundWorker()
        Me.Master_AssetTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Master_AssetTableAdapter()
        Me.Master_ComputerTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Master_ComputerTableAdapter()
        Me.Master_AreaTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Master_AreaTableAdapter()
        Me.Master_UserLaptopTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Master_UserLaptopTableAdapter()
        Me.Panel4.SuspendLayout()
        Me.header.SuspendLayout()
        Me.container.SuspendLayout()
        Me.lblDateTimeComp.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.DataGridViewAsset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterAssetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBASSETAUDITV2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_ASSET_AUDIT_V2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridViewComputer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterComputerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridViewArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterAreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.DataGridViewUserLaptop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterUserLaptopBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.header)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.container)
        Me.Panel4.Location = New System.Drawing.Point(1, 2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1178, 667)
        Me.Panel4.TabIndex = 99
        '
        'header
        '
        Me.header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.header.BackColor = System.Drawing.Color.CornflowerBlue
        Me.header.Controls.Add(Me.lblSite)
        Me.header.Controls.Add(Me.btnBackup)
        Me.header.Controls.Add(Me.btnValidation)
        Me.header.Controls.Add(Me.lblLogout)
        Me.header.Controls.Add(Me.lblUsername)
        Me.header.Controls.Add(Me.Label19)
        Me.header.Controls.Add(Me.btnMaster)
        Me.header.Controls.Add(Me.btnTransaction)
        Me.header.Controls.Add(Me.btnReport)
        Me.header.ForeColor = System.Drawing.Color.White
        Me.header.Location = New System.Drawing.Point(-1, -1)
        Me.header.Name = "header"
        Me.header.Size = New System.Drawing.Size(1176, 44)
        Me.header.TabIndex = 99
        '
        'lblSite
        '
        Me.lblSite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSite.AutoSize = True
        Me.lblSite.BackColor = System.Drawing.Color.Transparent
        Me.lblSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSite.ForeColor = System.Drawing.Color.White
        Me.lblSite.Location = New System.Drawing.Point(969, 24)
        Me.lblSite.Name = "lblSite"
        Me.lblSite.Size = New System.Drawing.Size(28, 15)
        Me.lblSite.TabIndex = 103
        Me.lblSite.Text = "Site"
        Me.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnBackup
        '
        Me.btnBackup.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnBackup.FlatAppearance.BorderSize = 0
        Me.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBackup.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackup.ForeColor = System.Drawing.Color.White
        Me.btnBackup.Image = CType(resources.GetObject("btnBackup.Image"), System.Drawing.Image)
        Me.btnBackup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBackup.Location = New System.Drawing.Point(580, 0)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.Size = New System.Drawing.Size(142, 44)
        Me.btnBackup.TabIndex = 102
        Me.btnBackup.Text = "  Backup Data"
        Me.btnBackup.UseVisualStyleBackColor = False
        '
        'btnValidation
        '
        Me.btnValidation.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnValidation.FlatAppearance.BorderSize = 0
        Me.btnValidation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValidation.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidation.ForeColor = System.Drawing.Color.White
        Me.btnValidation.Image = CType(resources.GetObject("btnValidation.Image"), System.Drawing.Image)
        Me.btnValidation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidation.Location = New System.Drawing.Point(435, 0)
        Me.btnValidation.Name = "btnValidation"
        Me.btnValidation.Size = New System.Drawing.Size(142, 44)
        Me.btnValidation.TabIndex = 100
        Me.btnValidation.Text = "      Audit Data"
        Me.btnValidation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidation.UseVisualStyleBackColor = False
        '
        'lblLogout
        '
        Me.lblLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLogout.BackColor = System.Drawing.Color.Transparent
        Me.lblLogout.Image = CType(resources.GetObject("lblLogout.Image"), System.Drawing.Image)
        Me.lblLogout.Location = New System.Drawing.Point(1141, 9)
        Me.lblLogout.Name = "lblLogout"
        Me.lblLogout.Size = New System.Drawing.Size(24, 26)
        Me.lblLogout.TabIndex = 99
        Me.lblLogout.Text = "  "
        '
        'lblUsername
        '
        Me.lblUsername.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.ForeColor = System.Drawing.Color.White
        Me.lblUsername.Location = New System.Drawing.Point(969, 4)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(33, 15)
        Me.lblUsername.TabIndex = 99
        Me.lblUsername.Text = "User"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label19.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label19.Location = New System.Drawing.Point(13, 41)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(115, 2)
        Me.Label19.TabIndex = 99
        Me.Label19.Text = "  "
        '
        'btnMaster
        '
        Me.btnMaster.BackColor = System.Drawing.Color.White
        Me.btnMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMaster.ForeColor = System.Drawing.Color.Black
        Me.btnMaster.Image = CType(resources.GetObject("btnMaster.Image"), System.Drawing.Image)
        Me.btnMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMaster.Location = New System.Drawing.Point(0, 0)
        Me.btnMaster.Margin = New System.Windows.Forms.Padding(6, 3, 3, 3)
        Me.btnMaster.Name = "btnMaster"
        Me.btnMaster.Size = New System.Drawing.Size(142, 44)
        Me.btnMaster.TabIndex = 99
        Me.btnMaster.Text = " Master Data"
        Me.btnMaster.UseVisualStyleBackColor = False
        '
        'btnTransaction
        '
        Me.btnTransaction.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnTransaction.FlatAppearance.BorderSize = 0
        Me.btnTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTransaction.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransaction.ForeColor = System.Drawing.Color.White
        Me.btnTransaction.Image = CType(resources.GetObject("btnTransaction.Image"), System.Drawing.Image)
        Me.btnTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransaction.Location = New System.Drawing.Point(145, 0)
        Me.btnTransaction.Name = "btnTransaction"
        Me.btnTransaction.Size = New System.Drawing.Size(142, 44)
        Me.btnTransaction.TabIndex = 99
        Me.btnTransaction.Text = "Transaction"
        Me.btnTransaction.UseVisualStyleBackColor = False
        '
        'btnReport
        '
        Me.btnReport.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnReport.FlatAppearance.BorderSize = 0
        Me.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.ForeColor = System.Drawing.Color.White
        Me.btnReport.Image = CType(resources.GetObject("btnReport.Image"), System.Drawing.Image)
        Me.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReport.Location = New System.Drawing.Point(290, 0)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(142, 44)
        Me.btnReport.TabIndex = 99
        Me.btnReport.Text = "      Report"
        Me.btnReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReport.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Location = New System.Drawing.Point(927, 5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 25)
        Me.Label9.TabIndex = 99
        '
        'container
        '
        Me.container.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.container.BackColor = System.Drawing.Color.Maroon
        Me.container.Controls.Add(Me.progressBarAll)
        Me.container.Controls.Add(Me.PercentAll)
        Me.container.Controls.Add(Me.btnRefreshAll)
        Me.container.Controls.Add(Me.lblDateTimeComp)
        Me.container.Location = New System.Drawing.Point(0, 42)
        Me.container.Name = "container"
        Me.container.Size = New System.Drawing.Size(1179, 622)
        Me.container.TabIndex = 0
        '
        'progressBarAll
        '
        Me.progressBarAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progressBarAll.Location = New System.Drawing.Point(836, 10)
        Me.progressBarAll.Name = "progressBarAll"
        Me.progressBarAll.Size = New System.Drawing.Size(242, 24)
        Me.progressBarAll.TabIndex = 167
        '
        'PercentAll
        '
        Me.PercentAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PercentAll.BackColor = System.Drawing.SystemColors.Control
        Me.PercentAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PercentAll.Location = New System.Drawing.Point(626, 16)
        Me.PercentAll.Name = "PercentAll"
        Me.PercentAll.Size = New System.Drawing.Size(208, 13)
        Me.PercentAll.TabIndex = 167
        Me.PercentAll.Text = "Starting, please wait..."
        Me.PercentAll.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnRefreshAll
        '
        Me.btnRefreshAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefreshAll.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreshAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefreshAll.FlatAppearance.BorderSize = 0
        Me.btnRefreshAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreshAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshAll.ForeColor = System.Drawing.Color.Transparent
        Me.btnRefreshAll.Image = CType(resources.GetObject("btnRefreshAll.Image"), System.Drawing.Image)
        Me.btnRefreshAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefreshAll.Location = New System.Drawing.Point(1081, 9)
        Me.btnRefreshAll.Name = "btnRefreshAll"
        Me.btnRefreshAll.Size = New System.Drawing.Size(93, 24)
        Me.btnRefreshAll.TabIndex = 166
        Me.btnRefreshAll.Text = "Refresh All"
        Me.btnRefreshAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefreshAll.UseVisualStyleBackColor = False
        '
        'lblDateTimeComp
        '
        Me.lblDateTimeComp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDateTimeComp.Controls.Add(Me.TabPage1)
        Me.lblDateTimeComp.Controls.Add(Me.TabPage2)
        Me.lblDateTimeComp.Controls.Add(Me.TabPage3)
        Me.lblDateTimeComp.Controls.Add(Me.TabPage4)
        Me.lblDateTimeComp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTimeComp.ItemSize = New System.Drawing.Size(99, 33)
        Me.lblDateTimeComp.Location = New System.Drawing.Point(-2, 0)
        Me.lblDateTimeComp.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDateTimeComp.Multiline = True
        Me.lblDateTimeComp.Name = "lblDateTimeComp"
        Me.lblDateTimeComp.Padding = New System.Drawing.Point(0, 0)
        Me.lblDateTimeComp.SelectedIndex = 0
        Me.lblDateTimeComp.Size = New System.Drawing.Size(1181, 623)
        Me.lblDateTimeComp.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 37)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1173, 582)
        Me.TabPage1.TabIndex = 99
        Me.TabPage1.Text = "Master Asset"
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.btnRefreshMasterAsset)
        Me.GroupBox4.Controls.Add(Me.DataGridViewAsset)
        Me.GroupBox4.Controls.Add(Me.lblCountAsset)
        Me.GroupBox4.Controls.Add(Me.ProgressBar1)
        Me.GroupBox4.Controls.Add(Me.lblPercentMAsset)
        Me.GroupBox4.Controls.Add(Me.Panel1)
        Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 2)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox4.Size = New System.Drawing.Size(1173, 582)
        Me.GroupBox4.TabIndex = 106
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Asset List"
        '
        'btnRefreshMasterAsset
        '
        Me.btnRefreshMasterAsset.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreshMasterAsset.FlatAppearance.BorderSize = 0
        Me.btnRefreshMasterAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreshMasterAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshMasterAsset.ForeColor = System.Drawing.Color.Transparent
        Me.btnRefreshMasterAsset.Image = CType(resources.GetObject("btnRefreshMasterAsset.Image"), System.Drawing.Image)
        Me.btnRefreshMasterAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefreshMasterAsset.Location = New System.Drawing.Point(6, 16)
        Me.btnRefreshMasterAsset.Name = "btnRefreshMasterAsset"
        Me.btnRefreshMasterAsset.Size = New System.Drawing.Size(76, 24)
        Me.btnRefreshMasterAsset.TabIndex = 107
        Me.btnRefreshMasterAsset.Text = "Refresh"
        Me.btnRefreshMasterAsset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefreshMasterAsset.UseVisualStyleBackColor = False
        '
        'DataGridViewAsset
        '
        Me.DataGridViewAsset.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.DataGridViewAsset.AllowUserToAddRows = False
        Me.DataGridViewAsset.AllowUserToDeleteRows = False
        Me.DataGridViewAsset.AllowUserToOrderColumns = True
        Me.DataGridViewAsset.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewAsset.AutoGenerateColumns = False
        Me.DataGridViewAsset.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewAsset.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewAsset.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewAsset.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDAssetDataGridViewTextBoxColumn, Me.IDAssetParentDataGridViewTextBoxColumn, Me.AssetLokalIDDataGridViewTextBoxColumn, Me.PCNameDataGridViewTextBoxColumn, Me.UserDataGridViewTextBoxColumn, Me.PTPemilikDataGridViewTextBoxColumn, Me.PTPenggunaDataGridViewTextBoxColumn, Me.SubBagianDataGridViewTextBoxColumn, Me.LokasiDataGridViewTextBoxColumn, Me.PosisiDataGridViewTextBoxColumn, Me.KategoriDataGridViewTextBoxColumn, Me.DeskripsiAssetDataGridViewTextBoxColumn, Me.MerkDataGridViewTextBoxColumn, Me.ModelDataGridViewTextBoxColumn, Me.TipeDataGridViewTextBoxColumn, Me.SubKategoriDataGridViewTextBoxColumn, Me.KapasitasDataGridViewTextBoxColumn, Me.NoSeriDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn, Me.KondisiDataGridViewTextBoxColumn, Me.TglPerolehanDataGridViewTextBoxColumn, Me.JtGaransiDataGridViewTextBoxColumn, Me.CatatanDataGridViewTextBoxColumn})
        Me.DataGridViewAsset.DataSource = Me.MasterAssetBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewAsset.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewAsset.Location = New System.Drawing.Point(6, 44)
        Me.DataGridViewAsset.Name = "DataGridViewAsset"
        Me.DataGridViewAsset.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewAsset.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewAsset.Size = New System.Drawing.Size(1158, 515)
        Me.DataGridViewAsset.TabIndex = 101
        '
        'IDAssetDataGridViewTextBoxColumn
        '
        Me.IDAssetDataGridViewTextBoxColumn.DataPropertyName = "IDAsset"
        Me.IDAssetDataGridViewTextBoxColumn.HeaderText = "ID Asset"
        Me.IDAssetDataGridViewTextBoxColumn.Name = "IDAssetDataGridViewTextBoxColumn"
        Me.IDAssetDataGridViewTextBoxColumn.ReadOnly = True
        '
        'IDAssetParentDataGridViewTextBoxColumn
        '
        Me.IDAssetParentDataGridViewTextBoxColumn.DataPropertyName = "IDAssetParent"
        Me.IDAssetParentDataGridViewTextBoxColumn.HeaderText = "ID Asset Parent"
        Me.IDAssetParentDataGridViewTextBoxColumn.Name = "IDAssetParentDataGridViewTextBoxColumn"
        Me.IDAssetParentDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AssetLokalIDDataGridViewTextBoxColumn
        '
        Me.AssetLokalIDDataGridViewTextBoxColumn.DataPropertyName = "AssetLokalID"
        Me.AssetLokalIDDataGridViewTextBoxColumn.HeaderText = "Asset Lokal ID"
        Me.AssetLokalIDDataGridViewTextBoxColumn.Name = "AssetLokalIDDataGridViewTextBoxColumn"
        Me.AssetLokalIDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PCNameDataGridViewTextBoxColumn
        '
        Me.PCNameDataGridViewTextBoxColumn.DataPropertyName = "PCName"
        Me.PCNameDataGridViewTextBoxColumn.HeaderText = "PC Name"
        Me.PCNameDataGridViewTextBoxColumn.Name = "PCNameDataGridViewTextBoxColumn"
        Me.PCNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UserDataGridViewTextBoxColumn
        '
        Me.UserDataGridViewTextBoxColumn.DataPropertyName = "User"
        Me.UserDataGridViewTextBoxColumn.HeaderText = "User"
        Me.UserDataGridViewTextBoxColumn.Name = "UserDataGridViewTextBoxColumn"
        Me.UserDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PTPemilikDataGridViewTextBoxColumn
        '
        Me.PTPemilikDataGridViewTextBoxColumn.DataPropertyName = "PTPemilik"
        Me.PTPemilikDataGridViewTextBoxColumn.HeaderText = "PT Pemilik"
        Me.PTPemilikDataGridViewTextBoxColumn.Name = "PTPemilikDataGridViewTextBoxColumn"
        Me.PTPemilikDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PTPenggunaDataGridViewTextBoxColumn
        '
        Me.PTPenggunaDataGridViewTextBoxColumn.DataPropertyName = "PTPengguna"
        Me.PTPenggunaDataGridViewTextBoxColumn.HeaderText = "PT Pengguna"
        Me.PTPenggunaDataGridViewTextBoxColumn.Name = "PTPenggunaDataGridViewTextBoxColumn"
        Me.PTPenggunaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SubBagianDataGridViewTextBoxColumn
        '
        Me.SubBagianDataGridViewTextBoxColumn.DataPropertyName = "SubBagian"
        Me.SubBagianDataGridViewTextBoxColumn.HeaderText = "Sub Bagian"
        Me.SubBagianDataGridViewTextBoxColumn.Name = "SubBagianDataGridViewTextBoxColumn"
        Me.SubBagianDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LokasiDataGridViewTextBoxColumn
        '
        Me.LokasiDataGridViewTextBoxColumn.DataPropertyName = "Lokasi"
        Me.LokasiDataGridViewTextBoxColumn.HeaderText = "Lokasi"
        Me.LokasiDataGridViewTextBoxColumn.Name = "LokasiDataGridViewTextBoxColumn"
        Me.LokasiDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PosisiDataGridViewTextBoxColumn
        '
        Me.PosisiDataGridViewTextBoxColumn.DataPropertyName = "Posisi"
        Me.PosisiDataGridViewTextBoxColumn.HeaderText = "Posisi"
        Me.PosisiDataGridViewTextBoxColumn.Name = "PosisiDataGridViewTextBoxColumn"
        Me.PosisiDataGridViewTextBoxColumn.ReadOnly = True
        '
        'KategoriDataGridViewTextBoxColumn
        '
        Me.KategoriDataGridViewTextBoxColumn.DataPropertyName = "Kategori"
        Me.KategoriDataGridViewTextBoxColumn.HeaderText = "Kategori"
        Me.KategoriDataGridViewTextBoxColumn.Name = "KategoriDataGridViewTextBoxColumn"
        Me.KategoriDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DeskripsiAssetDataGridViewTextBoxColumn
        '
        Me.DeskripsiAssetDataGridViewTextBoxColumn.DataPropertyName = "DeskripsiAsset"
        Me.DeskripsiAssetDataGridViewTextBoxColumn.HeaderText = "Deskripsi Asset"
        Me.DeskripsiAssetDataGridViewTextBoxColumn.Name = "DeskripsiAssetDataGridViewTextBoxColumn"
        Me.DeskripsiAssetDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MerkDataGridViewTextBoxColumn
        '
        Me.MerkDataGridViewTextBoxColumn.DataPropertyName = "Merk"
        Me.MerkDataGridViewTextBoxColumn.HeaderText = "Merk"
        Me.MerkDataGridViewTextBoxColumn.Name = "MerkDataGridViewTextBoxColumn"
        Me.MerkDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ModelDataGridViewTextBoxColumn
        '
        Me.ModelDataGridViewTextBoxColumn.DataPropertyName = "Model"
        Me.ModelDataGridViewTextBoxColumn.HeaderText = "Model"
        Me.ModelDataGridViewTextBoxColumn.Name = "ModelDataGridViewTextBoxColumn"
        Me.ModelDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TipeDataGridViewTextBoxColumn
        '
        Me.TipeDataGridViewTextBoxColumn.DataPropertyName = "Tipe"
        Me.TipeDataGridViewTextBoxColumn.HeaderText = "Tipe"
        Me.TipeDataGridViewTextBoxColumn.Name = "TipeDataGridViewTextBoxColumn"
        Me.TipeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SubKategoriDataGridViewTextBoxColumn
        '
        Me.SubKategoriDataGridViewTextBoxColumn.DataPropertyName = "SubKategori"
        Me.SubKategoriDataGridViewTextBoxColumn.HeaderText = "Sub Kategori"
        Me.SubKategoriDataGridViewTextBoxColumn.Name = "SubKategoriDataGridViewTextBoxColumn"
        Me.SubKategoriDataGridViewTextBoxColumn.ReadOnly = True
        '
        'KapasitasDataGridViewTextBoxColumn
        '
        Me.KapasitasDataGridViewTextBoxColumn.DataPropertyName = "Kapasitas"
        Me.KapasitasDataGridViewTextBoxColumn.HeaderText = "Kapasitas"
        Me.KapasitasDataGridViewTextBoxColumn.Name = "KapasitasDataGridViewTextBoxColumn"
        Me.KapasitasDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NoSeriDataGridViewTextBoxColumn
        '
        Me.NoSeriDataGridViewTextBoxColumn.DataPropertyName = "NoSeri"
        Me.NoSeriDataGridViewTextBoxColumn.HeaderText = "No Seri"
        Me.NoSeriDataGridViewTextBoxColumn.Name = "NoSeriDataGridViewTextBoxColumn"
        Me.NoSeriDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'KondisiDataGridViewTextBoxColumn
        '
        Me.KondisiDataGridViewTextBoxColumn.DataPropertyName = "Kondisi"
        Me.KondisiDataGridViewTextBoxColumn.HeaderText = "Kondisi"
        Me.KondisiDataGridViewTextBoxColumn.Name = "KondisiDataGridViewTextBoxColumn"
        Me.KondisiDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TglPerolehanDataGridViewTextBoxColumn
        '
        Me.TglPerolehanDataGridViewTextBoxColumn.DataPropertyName = "TglPerolehan"
        Me.TglPerolehanDataGridViewTextBoxColumn.HeaderText = "Tgl Perolehan"
        Me.TglPerolehanDataGridViewTextBoxColumn.Name = "TglPerolehanDataGridViewTextBoxColumn"
        Me.TglPerolehanDataGridViewTextBoxColumn.ReadOnly = True
        '
        'JtGaransiDataGridViewTextBoxColumn
        '
        Me.JtGaransiDataGridViewTextBoxColumn.DataPropertyName = "JtGaransi"
        Me.JtGaransiDataGridViewTextBoxColumn.HeaderText = "Jt Garansi"
        Me.JtGaransiDataGridViewTextBoxColumn.Name = "JtGaransiDataGridViewTextBoxColumn"
        Me.JtGaransiDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CatatanDataGridViewTextBoxColumn
        '
        Me.CatatanDataGridViewTextBoxColumn.DataPropertyName = "Catatan"
        Me.CatatanDataGridViewTextBoxColumn.HeaderText = "Catatan"
        Me.CatatanDataGridViewTextBoxColumn.Name = "CatatanDataGridViewTextBoxColumn"
        Me.CatatanDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MasterAssetBindingSource
        '
        Me.MasterAssetBindingSource.DataMember = "Master_Asset"
        Me.MasterAssetBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'DBASSETAUDITV2BindingSource
        '
        Me.DBASSETAUDITV2BindingSource.DataSource = Me.DB_ASSET_AUDIT_V2
        Me.DBASSETAUDITV2BindingSource.Position = 0
        '
        'DB_ASSET_AUDIT_V2
        '
        Me.DB_ASSET_AUDIT_V2.DataSetName = "DB_ASSET_AUDIT_V2"
        Me.DB_ASSET_AUDIT_V2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblCountAsset
        '
        Me.lblCountAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCountAsset.AutoSize = True
        Me.lblCountAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCountAsset.Location = New System.Drawing.Point(3, 562)
        Me.lblCountAsset.Name = "lblCountAsset"
        Me.lblCountAsset.Size = New System.Drawing.Size(142, 15)
        Me.lblCountAsset.TabIndex = 100
        Me.lblCountAsset.Text = "Displaying 0 Asset List(s)"
        Me.lblCountAsset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(88, 17)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(242, 24)
        Me.ProgressBar1.TabIndex = 108
        '
        'lblPercentMAsset
        '
        Me.lblPercentMAsset.AutoSize = True
        Me.lblPercentMAsset.Location = New System.Drawing.Point(332, 22)
        Me.lblPercentMAsset.Name = "lblPercentMAsset"
        Me.lblPercentMAsset.Size = New System.Drawing.Size(111, 13)
        Me.lblPercentMAsset.TabIndex = 109
        Me.lblPercentMAsset.Text = "Starting, please wait..."
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.comboMasterAsset)
        Me.Panel1.Controls.Add(Me.txtSearchMasterAsset)
        Me.Panel1.Controls.Add(Me.btnSearchAsset)
        Me.Panel1.Location = New System.Drawing.Point(696, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(468, 30)
        Me.Panel1.TabIndex = 112
        '
        'comboMasterAsset
        '
        Me.comboMasterAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboMasterAsset.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboMasterAsset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboMasterAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboMasterAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboMasterAsset.FormattingEnabled = True
        Me.comboMasterAsset.ItemHeight = 15
        Me.comboMasterAsset.Items.AddRange(New Object() {"All", "ID Asset", "ID Asset Parent", "Asset Lokal ID", "PC Name", "User", "PT Pemilik", "PT Pengguna", "Sub Bagian", "Lokasi", "Posisi", "Kategori", "Deskripsi Asset", "Merk", "Model", "Tipe", "Sub Kategori", "Kapasitas", "No. Seri", "Status", "Kondisi"})
        Me.comboMasterAsset.Location = New System.Drawing.Point(50, 5)
        Me.comboMasterAsset.Name = "comboMasterAsset"
        Me.comboMasterAsset.Size = New System.Drawing.Size(213, 23)
        Me.comboMasterAsset.TabIndex = 111
        '
        'txtSearchMasterAsset
        '
        Me.txtSearchMasterAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchMasterAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchMasterAsset.Location = New System.Drawing.Point(266, 6)
        Me.txtSearchMasterAsset.Name = "txtSearchMasterAsset"
        Me.txtSearchMasterAsset.Size = New System.Drawing.Size(177, 22)
        Me.txtSearchMasterAsset.TabIndex = 99
        '
        'btnSearchAsset
        '
        Me.btnSearchAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchAsset.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchAsset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchAsset.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchAsset.ForeColor = System.Drawing.Color.White
        Me.btnSearchAsset.Image = CType(resources.GetObject("btnSearchAsset.Image"), System.Drawing.Image)
        Me.btnSearchAsset.Location = New System.Drawing.Point(443, 5)
        Me.btnSearchAsset.Name = "btnSearchAsset"
        Me.btnSearchAsset.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchAsset.TabIndex = 99
        Me.btnSearchAsset.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.datetimeandversion)
        Me.TabPage2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 37)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1173, 582)
        Me.TabPage2.TabIndex = 99
        Me.TabPage2.Text = "Master Computer"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.BackColor = System.Drawing.Color.White
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Controls.Add(Me.ProgressBar2)
        Me.GroupBox3.Controls.Add(Me.btnRefreshMasterComputer)
        Me.GroupBox3.Controls.Add(Me.lblPercentMComp)
        Me.GroupBox3.Controls.Add(Me.lblCountCmp)
        Me.GroupBox3.Controls.Add(Me.DataGridViewComputer)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 2)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Size = New System.Drawing.Size(1173, 583)
        Me.GroupBox3.TabIndex = 107
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Computer List"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.txtSearchComputer)
        Me.Panel2.Controls.Add(Me.btnSearchComputer)
        Me.Panel2.Location = New System.Drawing.Point(696, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(468, 30)
        Me.Panel2.TabIndex = 113
        '
        'txtSearchComputer
        '
        Me.txtSearchComputer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchComputer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSearchComputer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchComputer.Location = New System.Drawing.Point(266, 6)
        Me.txtSearchComputer.Name = "txtSearchComputer"
        Me.txtSearchComputer.Size = New System.Drawing.Size(177, 20)
        Me.txtSearchComputer.TabIndex = 99
        '
        'btnSearchComputer
        '
        Me.btnSearchComputer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchComputer.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchComputer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchComputer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchComputer.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchComputer.ForeColor = System.Drawing.Color.White
        Me.btnSearchComputer.Image = CType(resources.GetObject("btnSearchComputer.Image"), System.Drawing.Image)
        Me.btnSearchComputer.Location = New System.Drawing.Point(443, 5)
        Me.btnSearchComputer.Name = "btnSearchComputer"
        Me.btnSearchComputer.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchComputer.TabIndex = 99
        Me.btnSearchComputer.UseVisualStyleBackColor = False
        '
        'ProgressBar2
        '
        Me.ProgressBar2.Location = New System.Drawing.Point(88, 17)
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.Size = New System.Drawing.Size(242, 24)
        Me.ProgressBar2.TabIndex = 113
        '
        'btnRefreshMasterComputer
        '
        Me.btnRefreshMasterComputer.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreshMasterComputer.FlatAppearance.BorderSize = 0
        Me.btnRefreshMasterComputer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreshMasterComputer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshMasterComputer.ForeColor = System.Drawing.Color.Transparent
        Me.btnRefreshMasterComputer.Image = CType(resources.GetObject("btnRefreshMasterComputer.Image"), System.Drawing.Image)
        Me.btnRefreshMasterComputer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefreshMasterComputer.Location = New System.Drawing.Point(6, 16)
        Me.btnRefreshMasterComputer.Name = "btnRefreshMasterComputer"
        Me.btnRefreshMasterComputer.Size = New System.Drawing.Size(76, 24)
        Me.btnRefreshMasterComputer.TabIndex = 113
        Me.btnRefreshMasterComputer.Text = "Refresh"
        Me.btnRefreshMasterComputer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefreshMasterComputer.UseVisualStyleBackColor = False
        '
        'lblPercentMComp
        '
        Me.lblPercentMComp.AutoSize = True
        Me.lblPercentMComp.Location = New System.Drawing.Point(332, 22)
        Me.lblPercentMComp.Name = "lblPercentMComp"
        Me.lblPercentMComp.Size = New System.Drawing.Size(111, 13)
        Me.lblPercentMComp.TabIndex = 105
        Me.lblPercentMComp.Text = "Starting, please wait..."
        '
        'lblCountCmp
        '
        Me.lblCountCmp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCountCmp.AutoSize = True
        Me.lblCountCmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountCmp.Location = New System.Drawing.Point(3, 562)
        Me.lblCountCmp.Name = "lblCountCmp"
        Me.lblCountCmp.Size = New System.Drawing.Size(167, 15)
        Me.lblCountCmp.TabIndex = 102
        Me.lblCountCmp.Text = "Displaying 0 Computer List(s)"
        '
        'DataGridViewComputer
        '
        Me.DataGridViewComputer.AllowUserToAddRows = False
        Me.DataGridViewComputer.AllowUserToDeleteRows = False
        Me.DataGridViewComputer.AllowUserToOrderColumns = True
        Me.DataGridViewComputer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewComputer.AutoGenerateColumns = False
        Me.DataGridViewComputer.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewComputer.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewComputer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewComputer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewComputer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ComputerNameDataGridViewTextBoxColumn})
        Me.DataGridViewComputer.DataSource = Me.MasterComputerBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewComputer.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewComputer.Location = New System.Drawing.Point(6, 44)
        Me.DataGridViewComputer.Name = "DataGridViewComputer"
        Me.DataGridViewComputer.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewComputer.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewComputer.Size = New System.Drawing.Size(1158, 515)
        Me.DataGridViewComputer.TabIndex = 99
        '
        'ComputerNameDataGridViewTextBoxColumn
        '
        Me.ComputerNameDataGridViewTextBoxColumn.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn.Name = "ComputerNameDataGridViewTextBoxColumn"
        Me.ComputerNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MasterComputerBindingSource
        '
        Me.MasterComputerBindingSource.DataMember = "Master_Computer"
        Me.MasterComputerBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'datetimeandversion
        '
        Me.datetimeandversion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datetimeandversion.AutoSize = True
        Me.datetimeandversion.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datetimeandversion.Image = CType(resources.GetObject("datetimeandversion.Image"), System.Drawing.Image)
        Me.datetimeandversion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.datetimeandversion.Location = New System.Drawing.Point(1188, 724)
        Me.datetimeandversion.Name = "datetimeandversion"
        Me.datetimeandversion.Size = New System.Drawing.Size(216, 17)
        Me.datetimeandversion.TabIndex = 99
        Me.datetimeandversion.Text = "       31 Oktober 2019  |  version 1.0"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.lblDateTimeArea)
        Me.TabPage3.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 37)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1173, 582)
        Me.TabPage3.TabIndex = 99
        Me.TabPage3.Text = "Master Area"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.DataGridViewArea)
        Me.GroupBox2.Controls.Add(Me.Panel3)
        Me.GroupBox2.Controls.Add(Me.btnRefreshMasterArea)
        Me.GroupBox2.Controls.Add(Me.lblPercentMArea)
        Me.GroupBox2.Controls.Add(Me.lblAreaList)
        Me.GroupBox2.Controls.Add(Me.ProgressBar3)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1173, 584)
        Me.GroupBox2.TabIndex = 106
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Area List"
        '
        'DataGridViewArea
        '
        Me.DataGridViewArea.AllowUserToAddRows = False
        Me.DataGridViewArea.AllowUserToDeleteRows = False
        Me.DataGridViewArea.AllowUserToOrderColumns = True
        Me.DataGridViewArea.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewArea.AutoGenerateColumns = False
        Me.DataGridViewArea.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewArea.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewArea.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AreaDataGridViewTextBoxColumn, Me.InitialDataGridViewTextBoxColumn, Me.KodeDataGridViewTextBoxColumn})
        Me.DataGridViewArea.DataSource = Me.MasterAreaBindingSource
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewArea.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewArea.Location = New System.Drawing.Point(6, 44)
        Me.DataGridViewArea.Name = "DataGridViewArea"
        Me.DataGridViewArea.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewArea.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewArea.Size = New System.Drawing.Size(1157, 515)
        Me.DataGridViewArea.TabIndex = 114
        '
        'AreaDataGridViewTextBoxColumn
        '
        Me.AreaDataGridViewTextBoxColumn.DataPropertyName = "Area"
        Me.AreaDataGridViewTextBoxColumn.HeaderText = "Area"
        Me.AreaDataGridViewTextBoxColumn.Name = "AreaDataGridViewTextBoxColumn"
        Me.AreaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'InitialDataGridViewTextBoxColumn
        '
        Me.InitialDataGridViewTextBoxColumn.DataPropertyName = "Initial"
        Me.InitialDataGridViewTextBoxColumn.HeaderText = "Initial"
        Me.InitialDataGridViewTextBoxColumn.Name = "InitialDataGridViewTextBoxColumn"
        Me.InitialDataGridViewTextBoxColumn.ReadOnly = True
        '
        'KodeDataGridViewTextBoxColumn
        '
        Me.KodeDataGridViewTextBoxColumn.DataPropertyName = "Kode"
        Me.KodeDataGridViewTextBoxColumn.HeaderText = "Kode"
        Me.KodeDataGridViewTextBoxColumn.Name = "KodeDataGridViewTextBoxColumn"
        Me.KodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MasterAreaBindingSource
        '
        Me.MasterAreaBindingSource.DataMember = "Master_Area"
        Me.MasterAreaBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.comboMasterArea)
        Me.Panel3.Controls.Add(Me.txtMasterArea)
        Me.Panel3.Controls.Add(Me.btnSearchArea)
        Me.Panel3.Location = New System.Drawing.Point(696, 12)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(468, 30)
        Me.Panel3.TabIndex = 113
        '
        'comboMasterArea
        '
        Me.comboMasterArea.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboMasterArea.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboMasterArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboMasterArea.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboMasterArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboMasterArea.FormattingEnabled = True
        Me.comboMasterArea.ItemHeight = 15
        Me.comboMasterArea.Items.AddRange(New Object() {"All", "Area", "Initial", "Kode"})
        Me.comboMasterArea.Location = New System.Drawing.Point(50, 5)
        Me.comboMasterArea.Name = "comboMasterArea"
        Me.comboMasterArea.Size = New System.Drawing.Size(213, 23)
        Me.comboMasterArea.TabIndex = 111
        '
        'txtMasterArea
        '
        Me.txtMasterArea.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMasterArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMasterArea.Location = New System.Drawing.Point(266, 6)
        Me.txtMasterArea.Name = "txtMasterArea"
        Me.txtMasterArea.Size = New System.Drawing.Size(177, 20)
        Me.txtMasterArea.TabIndex = 99
        '
        'btnSearchArea
        '
        Me.btnSearchArea.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchArea.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchArea.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchArea.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchArea.ForeColor = System.Drawing.Color.White
        Me.btnSearchArea.Image = CType(resources.GetObject("btnSearchArea.Image"), System.Drawing.Image)
        Me.btnSearchArea.Location = New System.Drawing.Point(443, 5)
        Me.btnSearchArea.Name = "btnSearchArea"
        Me.btnSearchArea.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchArea.TabIndex = 99
        Me.btnSearchArea.UseVisualStyleBackColor = False
        '
        'btnRefreshMasterArea
        '
        Me.btnRefreshMasterArea.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreshMasterArea.FlatAppearance.BorderSize = 0
        Me.btnRefreshMasterArea.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreshMasterArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshMasterArea.ForeColor = System.Drawing.Color.Transparent
        Me.btnRefreshMasterArea.Image = CType(resources.GetObject("btnRefreshMasterArea.Image"), System.Drawing.Image)
        Me.btnRefreshMasterArea.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefreshMasterArea.Location = New System.Drawing.Point(6, 16)
        Me.btnRefreshMasterArea.Name = "btnRefreshMasterArea"
        Me.btnRefreshMasterArea.Size = New System.Drawing.Size(76, 24)
        Me.btnRefreshMasterArea.TabIndex = 114
        Me.btnRefreshMasterArea.Text = "Refresh"
        Me.btnRefreshMasterArea.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefreshMasterArea.UseVisualStyleBackColor = False
        '
        'lblPercentMArea
        '
        Me.lblPercentMArea.AutoSize = True
        Me.lblPercentMArea.Location = New System.Drawing.Point(332, 22)
        Me.lblPercentMArea.Name = "lblPercentMArea"
        Me.lblPercentMArea.Size = New System.Drawing.Size(111, 13)
        Me.lblPercentMArea.TabIndex = 114
        Me.lblPercentMArea.Text = "Starting, please wait..."
        '
        'lblAreaList
        '
        Me.lblAreaList.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAreaList.AutoSize = True
        Me.lblAreaList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAreaList.Location = New System.Drawing.Point(3, 562)
        Me.lblAreaList.Name = "lblAreaList"
        Me.lblAreaList.Size = New System.Drawing.Size(138, 15)
        Me.lblAreaList.TabIndex = 101
        Me.lblAreaList.Text = "Displaying 0 Area List(s)"
        '
        'ProgressBar3
        '
        Me.ProgressBar3.Location = New System.Drawing.Point(88, 17)
        Me.ProgressBar3.Name = "ProgressBar3"
        Me.ProgressBar3.Size = New System.Drawing.Size(242, 24)
        Me.ProgressBar3.TabIndex = 113
        '
        'lblDateTimeArea
        '
        Me.lblDateTimeArea.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDateTimeArea.AutoSize = True
        Me.lblDateTimeArea.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTimeArea.Image = CType(resources.GetObject("lblDateTimeArea.Image"), System.Drawing.Image)
        Me.lblDateTimeArea.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDateTimeArea.Location = New System.Drawing.Point(1182, 721)
        Me.lblDateTimeArea.Name = "lblDateTimeArea"
        Me.lblDateTimeArea.Size = New System.Drawing.Size(216, 17)
        Me.lblDateTimeArea.TabIndex = 99
        Me.lblDateTimeArea.Text = "       31 Oktober 2019  |  version 1.0"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Controls.Add(Me.GroupBox5)
        Me.TabPage4.Location = New System.Drawing.Point(4, 37)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1173, 582)
        Me.TabPage4.TabIndex = 101
        Me.TabPage4.Text = "Master User Laptop"
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox5.Controls.Add(Me.DataGridViewUserLaptop)
        Me.GroupBox5.Controls.Add(Me.Panel5)
        Me.GroupBox5.Controls.Add(Me.lblPUserLaptop)
        Me.GroupBox5.Controls.Add(Me.btnRefreshMasterUserLaptop)
        Me.GroupBox5.Controls.Add(Me.ProgressBar5)
        Me.GroupBox5.Controls.Add(Me.lblUserLtp)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(0, 2)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(1173, 584)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "User Laptop"
        '
        'DataGridViewUserLaptop
        '
        Me.DataGridViewUserLaptop.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.DataGridViewUserLaptop.AllowUserToAddRows = False
        Me.DataGridViewUserLaptop.AllowUserToDeleteRows = False
        Me.DataGridViewUserLaptop.AllowUserToOrderColumns = True
        Me.DataGridViewUserLaptop.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewUserLaptop.AutoGenerateColumns = False
        Me.DataGridViewUserLaptop.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewUserLaptop.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewUserLaptop.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewUserLaptop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewUserLaptop.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.UsernameDataGridViewTextBoxColumn, Me.EmailUserLaptopDataGridViewTextBoxColumn})
        Me.DataGridViewUserLaptop.DataSource = Me.MasterUserLaptopBindingSource
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewUserLaptop.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewUserLaptop.Location = New System.Drawing.Point(6, 44)
        Me.DataGridViewUserLaptop.Name = "DataGridViewUserLaptop"
        Me.DataGridViewUserLaptop.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewUserLaptop.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewUserLaptop.Size = New System.Drawing.Size(1157, 515)
        Me.DataGridViewUserLaptop.TabIndex = 115
        '
        'UsernameDataGridViewTextBoxColumn
        '
        Me.UsernameDataGridViewTextBoxColumn.DataPropertyName = "Username"
        Me.UsernameDataGridViewTextBoxColumn.HeaderText = "Username"
        Me.UsernameDataGridViewTextBoxColumn.Name = "UsernameDataGridViewTextBoxColumn"
        Me.UsernameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmailUserLaptopDataGridViewTextBoxColumn
        '
        Me.EmailUserLaptopDataGridViewTextBoxColumn.DataPropertyName = "EmailUserLaptop"
        Me.EmailUserLaptopDataGridViewTextBoxColumn.HeaderText = "Email"
        Me.EmailUserLaptopDataGridViewTextBoxColumn.Name = "EmailUserLaptopDataGridViewTextBoxColumn"
        Me.EmailUserLaptopDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MasterUserLaptopBindingSource
        '
        Me.MasterUserLaptopBindingSource.DataMember = "Master_UserLaptop"
        Me.MasterUserLaptopBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.Controls.Add(Me.comboMasterUserLaptop)
        Me.Panel5.Controls.Add(Me.txtMasterUserLaptop)
        Me.Panel5.Controls.Add(Me.btnSearchUserLaptop)
        Me.Panel5.Location = New System.Drawing.Point(696, 12)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(468, 30)
        Me.Panel5.TabIndex = 113
        '
        'comboMasterUserLaptop
        '
        Me.comboMasterUserLaptop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboMasterUserLaptop.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboMasterUserLaptop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboMasterUserLaptop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboMasterUserLaptop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboMasterUserLaptop.FormattingEnabled = True
        Me.comboMasterUserLaptop.ItemHeight = 15
        Me.comboMasterUserLaptop.Items.AddRange(New Object() {"All", "Username", "Email"})
        Me.comboMasterUserLaptop.Location = New System.Drawing.Point(50, 5)
        Me.comboMasterUserLaptop.Name = "comboMasterUserLaptop"
        Me.comboMasterUserLaptop.Size = New System.Drawing.Size(213, 23)
        Me.comboMasterUserLaptop.TabIndex = 111
        '
        'txtMasterUserLaptop
        '
        Me.txtMasterUserLaptop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMasterUserLaptop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMasterUserLaptop.Location = New System.Drawing.Point(266, 6)
        Me.txtMasterUserLaptop.Name = "txtMasterUserLaptop"
        Me.txtMasterUserLaptop.Size = New System.Drawing.Size(177, 22)
        Me.txtMasterUserLaptop.TabIndex = 99
        '
        'btnSearchUserLaptop
        '
        Me.btnSearchUserLaptop.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchUserLaptop.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchUserLaptop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchUserLaptop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchUserLaptop.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchUserLaptop.ForeColor = System.Drawing.Color.White
        Me.btnSearchUserLaptop.Image = CType(resources.GetObject("btnSearchUserLaptop.Image"), System.Drawing.Image)
        Me.btnSearchUserLaptop.Location = New System.Drawing.Point(443, 5)
        Me.btnSearchUserLaptop.Name = "btnSearchUserLaptop"
        Me.btnSearchUserLaptop.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchUserLaptop.TabIndex = 99
        Me.btnSearchUserLaptop.UseVisualStyleBackColor = False
        '
        'lblPUserLaptop
        '
        Me.lblPUserLaptop.AutoSize = True
        Me.lblPUserLaptop.Location = New System.Drawing.Point(332, 22)
        Me.lblPUserLaptop.Name = "lblPUserLaptop"
        Me.lblPUserLaptop.Size = New System.Drawing.Size(111, 13)
        Me.lblPUserLaptop.TabIndex = 3
        Me.lblPUserLaptop.Text = "Starting, please wait..."
        '
        'btnRefreshMasterUserLaptop
        '
        Me.btnRefreshMasterUserLaptop.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefreshMasterUserLaptop.FlatAppearance.BorderSize = 0
        Me.btnRefreshMasterUserLaptop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefreshMasterUserLaptop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshMasterUserLaptop.ForeColor = System.Drawing.Color.Transparent
        Me.btnRefreshMasterUserLaptop.Image = CType(resources.GetObject("btnRefreshMasterUserLaptop.Image"), System.Drawing.Image)
        Me.btnRefreshMasterUserLaptop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefreshMasterUserLaptop.Location = New System.Drawing.Point(6, 16)
        Me.btnRefreshMasterUserLaptop.Name = "btnRefreshMasterUserLaptop"
        Me.btnRefreshMasterUserLaptop.Size = New System.Drawing.Size(76, 24)
        Me.btnRefreshMasterUserLaptop.TabIndex = 113
        Me.btnRefreshMasterUserLaptop.Text = "Refresh"
        Me.btnRefreshMasterUserLaptop.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefreshMasterUserLaptop.UseVisualStyleBackColor = False
        '
        'ProgressBar5
        '
        Me.ProgressBar5.Location = New System.Drawing.Point(88, 17)
        Me.ProgressBar5.Name = "ProgressBar5"
        Me.ProgressBar5.Size = New System.Drawing.Size(242, 24)
        Me.ProgressBar5.TabIndex = 114
        '
        'lblUserLtp
        '
        Me.lblUserLtp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblUserLtp.AutoSize = True
        Me.lblUserLtp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserLtp.Location = New System.Drawing.Point(3, 562)
        Me.lblUserLtp.Name = "lblUserLtp"
        Me.lblUserLtp.Size = New System.Drawing.Size(308, 15)
        Me.lblUserLtp.TabIndex = 6
        Me.lblUserLtp.Text = "Displaying 0 User(s) Laptop HQ | 0 User(s) Laptop RSO"
        '
        'label_copyright
        '
        Me.label_copyright.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.label_copyright.AutoSize = True
        Me.label_copyright.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.label_copyright.Location = New System.Drawing.Point(378, 9)
        Me.label_copyright.Name = "label_copyright"
        Me.label_copyright.Size = New System.Drawing.Size(273, 13)
        Me.label_copyright.TabIndex = 99
        Me.label_copyright.Text = "Copyright @2019-2020 Djarum-SCM. All rights reserved. "
        '
        'BackgroundWorkerComputer
        '
        Me.BackgroundWorkerComputer.WorkerReportsProgress = True
        '
        'BackgroundWorkerArea
        '
        Me.BackgroundWorkerArea.WorkerReportsProgress = True
        '
        'BackgroundWorker4
        '
        Me.BackgroundWorker4.WorkerReportsProgress = True
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Jakarta", "Bandung ", "Semarang", "Kudus", "Krapyak", "Gondangmanis", "Surabaya"})
        Me.ComboBox1.Location = New System.Drawing.Point(61, 27)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(171, 24)
        Me.ComboBox1.TabIndex = 103
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(17, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 15)
        Me.Label2.TabIndex = 103
        Me.Label2.Text = "Site :"
        '
        'BackgroundWorker5
        '
        Me.BackgroundWorker5.WorkerReportsProgress = True
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'BackgroundWorker6
        '
        Me.BackgroundWorker6.WorkerReportsProgress = True
        '
        'BackgroundWorkerUserLaptop
        '
        Me.BackgroundWorkerUserLaptop.WorkerReportsProgress = True
        '
        'BackgroundWorkerAsset
        '
        Me.BackgroundWorkerAsset.WorkerReportsProgress = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(240, 24)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(94, 30)
        Me.Button3.TabIndex = 103
        Me.Button3.Text = "     Process"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'BackgroundWorkerRefreshAll
        '
        Me.BackgroundWorkerRefreshAll.WorkerReportsProgress = True
        '
        'Master_AssetTableAdapter
        '
        Me.Master_AssetTableAdapter.ClearBeforeFill = True
        '
        'Master_ComputerTableAdapter
        '
        Me.Master_ComputerTableAdapter.ClearBeforeFill = True
        '
        'Master_AreaTableAdapter
        '
        Me.Master_AreaTableAdapter.ClearBeforeFill = True
        '
        'Master_UserLaptopTableAdapter
        '
        Me.Master_UserLaptopTableAdapter.ClearBeforeFill = True
        '
        'MasterData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1179, 669)
        Me.Controls.Add(Me.Panel4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MasterData"
        Me.Text = "Asset Audit and Monitoring"
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.header.ResumeLayout(False)
        Me.header.PerformLayout()
        Me.container.ResumeLayout(False)
        Me.lblDateTimeComp.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.DataGridViewAsset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterAssetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBASSETAUDITV2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_ASSET_AUDIT_V2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DataGridViewComputer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterComputerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridViewArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterAreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.DataGridViewUserLaptop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterUserLaptopBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents label_copyright As System.Windows.Forms.Label
    Friend WithEvents btnTransaction As System.Windows.Forms.Button
    Friend WithEvents btnMaster As System.Windows.Forms.Button
    Friend WithEvents btnReport As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents lblLogout As System.Windows.Forms.Label
    Friend WithEvents header As System.Windows.Forms.Panel
    Friend WithEvents BackgroundWorkerComputer As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorkerArea As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker4 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnValidation As System.Windows.Forms.Button
    Friend WithEvents btnBackup As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker5 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker6 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorkerUserLaptop As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorkerAsset As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorkerRefreshAll As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblSite As Label
    Friend WithEvents DBASSETAUDITV2BindingSource As BindingSource
    Friend WithEvents DB_ASSET_AUDIT_V2 As DB_ASSET_AUDIT_V2
    Friend WithEvents MasterAssetBindingSource As BindingSource
    Friend WithEvents Master_AssetTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Master_AssetTableAdapter
    Friend WithEvents MasterComputerBindingSource As BindingSource
    Friend WithEvents Master_ComputerTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Master_ComputerTableAdapter
    Friend WithEvents MasterAreaBindingSource As BindingSource
    Friend WithEvents Master_AreaTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Master_AreaTableAdapter
    Friend WithEvents MasterUserLaptopBindingSource As BindingSource
    Friend WithEvents Master_UserLaptopTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Master_UserLaptopTableAdapter
    Friend WithEvents container As Panel
    Friend WithEvents progressBarAll As ProgressBar
    Friend WithEvents PercentAll As Label
    Friend WithEvents btnRefreshAll As Button
    Friend WithEvents lblDateTimeComp As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btnRefreshMasterAsset As Button
    Friend WithEvents DataGridViewAsset As DataGridView
    Friend WithEvents IDAssetDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IDAssetParentDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AssetLokalIDDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PCNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PTPemilikDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PTPenggunaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SubBagianDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LokasiDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PosisiDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents KategoriDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DeskripsiAssetDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MerkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ModelDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TipeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SubKategoriDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents KapasitasDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NoSeriDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents KondisiDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TglPerolehanDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents JtGaransiDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CatatanDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents lblCountAsset As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents lblPercentMAsset As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents comboMasterAsset As ComboBox
    Friend WithEvents txtSearchMasterAsset As TextBox
    Friend WithEvents btnSearchAsset As Button
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtSearchComputer As TextBox
    Friend WithEvents btnSearchComputer As Button
    Friend WithEvents ProgressBar2 As ProgressBar
    Friend WithEvents btnRefreshMasterComputer As Button
    Friend WithEvents lblPercentMComp As Label
    Friend WithEvents lblCountCmp As Label
    Friend WithEvents DataGridViewComputer As DataGridView
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents datetimeandversion As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents DataGridViewArea As DataGridView
    Friend WithEvents AreaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InitialDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents KodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Panel3 As Panel
    Friend WithEvents comboMasterArea As ComboBox
    Friend WithEvents txtMasterArea As TextBox
    Friend WithEvents btnSearchArea As Button
    Friend WithEvents btnRefreshMasterArea As Button
    Friend WithEvents lblPercentMArea As Label
    Friend WithEvents lblAreaList As Label
    Friend WithEvents ProgressBar3 As ProgressBar
    Friend WithEvents lblDateTimeArea As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents DataGridViewUserLaptop As DataGridView
    Friend WithEvents UsernameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EmailUserLaptopDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Panel5 As Panel
    Friend WithEvents comboMasterUserLaptop As ComboBox
    Friend WithEvents txtMasterUserLaptop As TextBox
    Friend WithEvents btnSearchUserLaptop As Button
    Friend WithEvents lblPUserLaptop As Label
    Friend WithEvents btnRefreshMasterUserLaptop As Button
    Friend WithEvents ProgressBar5 As ProgressBar
    Friend WithEvents lblUserLtp As Label
End Class
