﻿Imports System.Data.OleDb
Imports System.IO

Public Class MasterData
    Dim pos As Integer
    Dim tabledataasset As DataTable
    Dim Da As OleDbDataAdapter
    Dim Ds As DataSet
    Dim tableasset As DataTable
    Dim tablecomputer As DataTable
    Dim tablearea As DataTable
    Dim tableIP As DataTable
    Dim tableDSO As DataTable
    Dim dragging As Boolean = False
    Dim startPoint As Point = New Point(0, 0)
    Dim pathfileComp As String = ""
    Dim pathfileIP As String = ""
    Dim pathfileDSO As String = ""
    Dim pathFileAsset = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\ASMTR06.xlsx"
    Dim pathFileComputer = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\Master_Computer_ADUC.xlsx"
    Dim pathfileArea = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\Master_Area.xlsx"
    Dim pathfileUserLaptop = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\Master_UserLaptop.xlsx"
    'Dim pathFileAsset = "D:\asset-audit-and-monitoring\Supported Files\ASMTR06.xlsx"
    'Dim pathFileComputer = "D:\asset-audit-and-monitoring\Supported Files\Master_Computer_ADUC.xlsx"
    'Dim pathfileArea = "D:\asset-audit-and-monitoring\Supported Files\Master_Area.xlsx"
    'Dim pathfileUserLaptop = "D:\asset-audit-and-monitoring\Supported Files\Master_UserLaptop.xlsx"

    Dim totalCountAsset = 0
    Dim totalCountComputer = 0
    Dim totalCountArea = 0
    Dim totalCountUserLaptop = 0
    Dim flagCountAsset = 0
    Dim flagCountComputer = 0
    Dim flagCountArea = 0
    Dim flagCountUserLaptopHQ = 0
    Dim flagCountUserLaptopRSO = 0
    Dim textRefreshAll As String = ""

    Private Sub MasterData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Master_AssetTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Asset)
        Me.Master_ComputerTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Computer)
        Me.Master_AreaTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Area)
        Me.Master_UserLaptopTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_UserLaptop)

        WindowState = FormWindowState.Maximized

        header.BackColor = Color.FromArgb(255, 2, 43, 58)
        Label19.BackColor = Color.FromArgb(255, 2, 43, 58)

        btnMaster.FlatStyle = FlatStyle.Flat
        btnMaster.FlatAppearance.BorderSize = 0
        btnTransaction.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnReport.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnValidation.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnBackup.BackColor = Color.FromArgb(255, 2, 43, 58)

        lblUsername.Text = displayName
        lblSite.Text = siteName

        btnRefreshAll.BackColor = Color.FromArgb(255, 31, 122, 140)

        btnRefreshMasterAsset.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnRefreshMasterComputer.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnRefreshMasterArea.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnRefreshMasterUserLaptop.BackColor = Color.FromArgb(255, 31, 122, 140)

        btnSearchAsset.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSearchComputer.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSearchArea.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSearchUserLaptop.BackColor = Color.FromArgb(255, 31, 122, 140)

        txtSearchMasterAsset.BorderStyle = BorderStyle.None
        txtSearchMasterAsset.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearchMasterAsset.Height = 21
        txtSearchMasterAsset.AutoSize = False

        txtSearchComputer.BorderStyle = BorderStyle.None
        txtSearchComputer.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearchComputer.Height = 21
        txtSearchComputer.AutoSize = False

        txtMasterArea.BorderStyle = BorderStyle.None
        txtMasterArea.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtMasterArea.Height = 21
        txtMasterArea.AutoSize = False

        txtMasterUserLaptop.BorderStyle = BorderStyle.None
        txtMasterUserLaptop.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtMasterUserLaptop.Height = 21
        txtMasterUserLaptop.AutoSize = False

        comboMasterAsset.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboMasterAsset.Items.Count > 0 Then
            comboMasterAsset.SelectedIndex = 0    ' The first item has index 0 '
        End If

        comboMasterArea.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboMasterArea.Items.Count > 0 Then
            comboMasterArea.SelectedIndex = 0    ' The first item has index 0 '
        End If

        comboMasterUserLaptop.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboMasterUserLaptop.Items.Count > 0 Then
            comboMasterUserLaptop.SelectedIndex = 0    ' The first item has index 0 '
        End If

        ProgressBar1.BackColor = Color.FromArgb(255, 225, 229, 242)
        ProgressBar1.ForeColor = Color.FromArgb(255, 2, 43, 58)
        ProgressBar2.BackColor = Color.FromArgb(255, 225, 229, 242)
        ProgressBar2.ForeColor = Color.FromArgb(255, 2, 43, 58)
        ProgressBar3.BackColor = Color.FromArgb(255, 225, 229, 242)
        ProgressBar3.ForeColor = Color.FromArgb(255, 2, 43, 58)
        ProgressBar5.BackColor = Color.FromArgb(255, 225, 229, 242)
        ProgressBar5.ForeColor = Color.FromArgb(255, 2, 43, 58)
        progressBarAll.BackColor = Color.FromArgb(255, 225, 229, 242)
        progressBarAll.ForeColor = Color.FromArgb(255, 2, 43, 58)

        Try
            progressBarAll.Hide()
            PercentAll.Hide()

            ProgressBar1.Hide()
            lblPercentMAsset.Hide()
            lblCountAsset.Text = "Displaying " & getCount("Master_Asset", "KodeSite", "", kodeSite, "", "count", "") & " Asset List(s)"

            ProgressBar2.Hide()
            lblPercentMComp.Hide()
            lblCountCmp.Text = "Displaying " & getCount("Master_Computer", "KodeSite", "", kodeSite, "", "count", "") & " Computer List(s)"

            ProgressBar3.Hide()
            lblPercentMArea.Hide()
            lblAreaList.Text = "Displaying " & getCount("Master_Area", "KodeSite", "", kodeSite, "", "count", "") & " Area List"

            ProgressBar5.Hide()
            lblPUserLaptop.Hide()
            lblUserLtp.Text = "Displaying " & getCount("Master_UserLaptop", "KodeSite", "EmailUserLaptop", kodeSite, "djarum", "distinct", "Username") & " User(s) Laptop HQ | " & getCount("Master_UserLaptop", "KodeSite", "EmailUserLaptop", kodeSite, "sumbercipta", "distinct", "Username") & " User(s) Laptop RSO"

        Catch ex As Exception
            MsgBox("Information Message : " & vbCrLf & "MasterData_Load : " & vbCrLf & ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnRefreshMasterAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshMasterAsset.Click
        If (MessageBox.Show("Do you really want to update? " & vbCrLf & "All your Master Asset data will be changed.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Try
                ProgressBar1.Value = 0
                ProgressBar1.Show()
                lblPercentMAsset.Show()
                BackgroundWorkerAsset.RunWorkerAsync()

            Catch ex As Exception
                MsgBox("Information Message : " & vbCrLf & "Master-Asset : " & vbCrLf & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub btnRefreshMasterComputer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshMasterComputer.Click

        If (MessageBox.Show("Do you really want to update? " & vbCrLf & "All your Master Computer data will be changed.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Try

                If pathFileComputer.IndexOf(".xlsx") <> -1 Then
                    ProgressBar2.Value = 0
                    ProgressBar2.Show()
                    lblPercentMComp.Show()

                    BackgroundWorkerComputer.RunWorkerAsync()
                Else
                    MsgBox("Invalid data : " & "Please choose file with correctfully!")
                End If
            Catch ex As Exception
                MsgBox("Master-Computer : " & vbCrLf & ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnRefreshMasterArea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshMasterArea.Click
        If (MessageBox.Show("Do you really want to update? " & vbCrLf & "All your Master Area data will be changed.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Try
                If pathfileArea.IndexOf(".xlsx") <> -1 Then
                    ProgressBar3.Value = 0
                    ProgressBar3.Show()
                    lblPercentMArea.Show()
                    BackgroundWorkerArea.RunWorkerAsync()
                Else
                    Msgbox_InformationDialog.Show()
                End If

            Catch ex As Exception
                MsgBox("Information Message : " & vbCrLf & "Master-Area : " & vbCrLf & ex.Message.ToString)
            End Try
        Else
        End If
    End Sub

    Private Sub btnRefreshUserLaptop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshMasterUserLaptop.Click
        If (MessageBox.Show("Do you really want to update? " & vbCrLf & "All your Master User Laptop data will be changed.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Try
                ProgressBar5.Value = 0
                ProgressBar5.Show()
                lblPUserLaptop.Show()
                BackgroundWorkerUserLaptop.RunWorkerAsync()
            Catch ex As Exception
                MsgBox("Information Message : " & vbCrLf & "Master-UserLaptop : " & vbCrLf & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub btnRefreshAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshAll.Click
        Try
            If (MessageBox.Show("Do you really want to update?" & vbCrLf & "All your Master Data will be changed.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
                progressBarAll.Show()
                PercentAll.Show()
                BackgroundWorkerRefreshAll.RunWorkerAsync()
            End If
        Catch ex As Exception
            MsgBox("Refresh All Message: " & ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorkerAsset_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerAsset.DoWork
        Try
            Call Koneksi()
            Call delete("Master_Asset", "KodeSite", kodeSite)

            Dim MyConnection As System.Data.OleDb.OleDbConnection
            Dim dataSet As System.Data.DataSet
            Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
            Dim percent = 0
            flagCountAsset = 0

            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFileAsset + ";Extended Properties=Excel 12.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter("Select * from [ASMTR06$]", MyConnection)

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)
            totalCountAsset = dataSet.Tables(0).Rows.Count
            For i = 0 To dataSet.Tables(0).Rows.Count - 1
                If i <> 0 And i <> 1 Then 'cek baris dibaca bukan dari title
                    If dataSet.Tables(0).Rows(i)(0).ToString() <> "" Then

                        percent = (i * 100) / dataSet.Tables(0).Rows.Count
                        BackgroundWorkerAsset.ReportProgress(percent)
                        'BackgroundWorkerRefreshAll.ReportProgress(percent)
                        If dataSet.Tables(0).Rows(i)(18).ToString() <> "RETIRED" Then
                            flagCountAsset += 1
                            Dim str As String
                            str = "Insert into Master_Asset([KodeSite],[IDAsset],[IDAssetParent],[AssetLokalID],[PCName],[User],[PTPemilik],[PTPengguna],[SubBagian],[Lokasi],[Posisi],[Kategori],[DeskripsiAsset],[Merk],[Model],[Tipe],[SubKategori],[Kapasitas],[NoSeri],[Status],[Kondisi],[TglPerolehan],[JtGaransi],[Catatan]) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                            cmd = New OleDbCommand(str, Conn)
                            cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                            cmd.Parameters.Add(New OleDbParameter("IDAsset", dataSet.Tables(0).Rows(i)(0).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("IDAssetParent", dataSet.Tables(0).Rows(i)(1).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("AssetLokalID", dataSet.Tables(0).Rows(i)(2).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PCName", dataSet.Tables(0).Rows(i)(3).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("User", dataSet.Tables(0).Rows(i)(4).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PTPemilik", dataSet.Tables(0).Rows(i)(5).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PTPengguna", dataSet.Tables(0).Rows(i)(6).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("SubBagian", dataSet.Tables(0).Rows(i)(7).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Lokasi", dataSet.Tables(0).Rows(i)(8).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Posisi", dataSet.Tables(0).Rows(i)(9).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kategori", dataSet.Tables(0).Rows(i)(10).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("DeskripsiAsset", dataSet.Tables(0).Rows(i)(11).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Merk", dataSet.Tables(0).Rows(i)(12).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Model", dataSet.Tables(0).Rows(i)(13).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Tipe", dataSet.Tables(0).Rows(i)(14).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("SubKategori", dataSet.Tables(0).Rows(i)(15).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kapasitas", dataSet.Tables(0).Rows(i)(16).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("NoSeri", dataSet.Tables(0).Rows(i)(17).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Status", dataSet.Tables(0).Rows(i)(18).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kondisi", dataSet.Tables(0).Rows(i)(19).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("TglPerolehan", dataSet.Tables(0).Rows(i)(20).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("JtGaransi", dataSet.Tables(0).Rows(i)(21).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Catatan", dataSet.Tables(0).Rows(i)(22).ToString()))
                            cmd.ExecuteNonQuery()
                            cmd.Dispose()
                        End If
                    End If
                End If
            Next
            Conn.Close()
        Catch ex As Exception
            MsgBox("Background Worker ASMTR : " & ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorkerAsset_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerAsset.ProgressChanged
        ProgressBar1.Value = e.ProgressPercentage
        lblPercentMAsset.Text = "Uploading" + totalCountAsset.ToString + " data... " + e.ProgressPercentage.ToString + "%"
    End Sub

    Private Sub BackgroundWorkerAsset_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerAsset.RunWorkerCompleted
        Try
            Msgbox_okUIDesign.lblMsg.Text = "Data Saved Successfully!"
            Msgbox_okUIDesign.Show()


            lblCountAsset.Text = "Displaying " + flagCountAsset.ToString + " Asset List(s)"

            Me.Master_AssetTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Asset)
            ProgressBar1.Hide()
            lblPercentMAsset.Hide()
            lblPercentMAsset.Text = "Starting, Please Wait... "
        Catch ex As Exception
            MsgBox("background complete: " & ex.Message)
        End Try

    End Sub

    Private Sub BackgroundWorkerComputer_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerComputer.DoWork
        Call Koneksi()
        Call delete("Master_Computer", "KodeSite", kodeSite)

        Dim percent = 0
        flagCountComputer = 0

        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim dataSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim query = "Select * FROM  [MasterComp$] Where [Computer Name] Is Not Null"
        MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFileComputer + ";Extended Properties=Excel 12.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter(query, MyConnection)

        dataSet = New System.Data.DataSet
        MyCommand.Fill(dataSet)

        For rowCounter As Integer = 0 To dataSet.Tables(0).Rows.Count - 1
            Dim emptyColumnCount As Integer = 0
            Dim row As DataRow = dataSet.Tables(0).Rows(rowCounter)

            For Each rowItem In row.ItemArray()
                If rowItem Is Nothing Or rowItem Is DBNull.Value Or rowItem.Equals("") Then
                    emptyColumnCount += 1
                End If
            Next

            If emptyColumnCount = dataSet.Tables(0).Columns.Count Then
                dataSet.Tables(0).Rows.Remove(row)
            End If
        Next
        totalCountComputer = dataSet.Tables(0).Rows.Count
        For i = 0 To dataSet.Tables(0).Rows.Count - 1
            ' If i <> 0 Then
            flagCountComputer += 1
            'Thread.Sleep(25)
            percent = (i * 100) / dataSet.Tables(0).Rows.Count
            BackgroundWorkerComputer.ReportProgress(percent)
            ' If dataSet.Tables(0).Rows(i)(0).ToString() <> "" And dataSet.Tables(0).Rows(i)(0).ToString() <> Nothing Then
            Dim str As String
            str = "Insert into Master_Computer([KodeSite], [ComputerName]) Values (?,?)"
            cmd = New OleDbCommand(str, Conn)
            cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
            cmd.Parameters.Add(New OleDbParameter("ComputerName", dataSet.Tables(0).Rows(i)(0).ToString()))
            cmd.ExecuteNonQuery()
            cmd.Dispose()
        Next
        Conn.Close()
    End Sub

    Private Sub BackgroundWorkerComputer_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerComputer.ProgressChanged
        ProgressBar2.Value = e.ProgressPercentage
        lblPercentMComp.Text = "Uploading" + totalCountComputer.ToString + " data..." + e.ProgressPercentage.ToString & "%"
    End Sub

    Private Sub BackgroundWorkerComputer_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerComputer.RunWorkerCompleted
        Msgbox_okUIDesign.Show()
        Me.Master_ComputerTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Computer)
        ProgressBar2.Hide()
        lblPercentMComp.Hide()
        lblPercentMComp.Text = "Starting, please wait..."
        lblCountCmp.Text = "Displaying " & flagCountComputer.ToString & " Computer List(s)"
    End Sub

    Private Sub BackgroundWorkerArea_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerArea.DoWork
        Call Koneksi()
        Call delete("Master_Area", "KodeSite", kodeSite)

        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim dataSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim percent = 0
        FlagCountArea = 0
        MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathfileArea + ";Extended Properties=Excel 12.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("Select * from [Ref$]", MyConnection)

        dataSet = New System.Data.DataSet
        MyCommand.Fill(dataSet)
        totalCountArea = dataSet.Tables(0).Rows.Count
        For i = 0 To dataSet.Tables(0).Rows.Count - 1
            If dataSet.Tables(0).Rows(i)(0).ToString() <> "" Then
                flagCountArea += 1
                percent = (i * 100) / dataSet.Tables(0).Rows.Count
                BackgroundWorkerArea.ReportProgress(percent)

                Dim str As String
                str = "Insert into Master_Area([KodeSite],[Area],[Kode],[Initial]) Values (?,?,?,?)"
                cmd = New OleDbCommand(str, Conn)
                cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                cmd.Parameters.Add(New OleDbParameter("Area", dataSet.Tables(0).Rows(i)(0).ToString()))
                cmd.Parameters.Add(New OleDbParameter("Kode", dataSet.Tables(0).Rows(i)(1).ToString()))
                cmd.Parameters.Add(New OleDbParameter("Initial", dataSet.Tables(0).Rows(i)(2).ToString()))
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            End If
        Next
        Conn.Close()
    End Sub

    Private Sub BackgroundWorkerArea_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerArea.ProgressChanged
        ProgressBar3.Value = e.ProgressPercentage
        lblPercentMArea.Text = "Uploading" + totalCountArea.ToString + " data..." + e.ProgressPercentage.ToString & "%"
    End Sub

    Private Sub BackgroundWorkerArea_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerArea.RunWorkerCompleted
        Msgbox_okUIDesign.Show()
        Me.Master_AreaTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Area)
        lblAreaList.Text = "Displaying " & FlagCountArea.ToString & " Area List(s)"
        ProgressBar3.Hide()
        lblPercentMArea.Hide()
        lblPercentMArea.Text = "Starting, please wait..."
    End Sub

    Private Sub BackgroundWorkerUserLaptop_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerUserLaptop.DoWork
        Try
            Call Koneksi()
            Dim pathUserLaptop = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\"
            'delete data
            Dim deleteData As String = "Delete * from Master_UserLaptop where [KodeSite]=?"
            cmd = New OleDbCommand(deleteData, Conn)
            cmd.Parameters.AddWithValue("KodeSite", kodeSite)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            If kodeSite = "021" Then
                If File.Exists(".\Jakarta-LaptopUsers.csv") Then
                    pathUserLaptop += "Jakarta-LaptopUsers.csv"
                Else
                    pathUserLaptop += "JKT-LaptopUsers.csv"
                End If
            ElseIf kodeSite = "031" Then
                pathUserLaptop += "Bandung-LaptopUsers.csv"
            ElseIf kodeSite = "041" Then
                pathUserLaptop += "Semarang-LaptopUsers.csv"
            ElseIf kodeSite = "011" Then
                pathUserLaptop += "KDS-LaptopUsers.csv"
            ElseIf kodeSite = "012" Then
                pathUserLaptop += "KPY-LaptopUsers.csv"
            ElseIf kodeSite = "015" Then
                pathUserLaptop += "GDM-LaptopUsers.csv"
            ElseIf kodeSite = "051" Then
                pathUserLaptop += ".\Surabaya-LaptopUsers.csv"
            End If

            Dim posSatu = 0
            Dim percent = 0
            flagCountUserLaptopHQ = 0
            flagCountUserLaptopRSO = 0
            totalCountUserLaptop = IO.File.ReadAllLines(pathUserLaptop).Length
            Dim lineCount As Integer = IO.File.ReadAllLines(pathUserLaptop).Length - 1
            Using objReader As New StreamReader(pathUserLaptop)
                Do While objReader.Peek() <> -1

                    Dim line As String = objReader.ReadLine
                    Dim splitData() = line.Split(",")
                    If line <> "" And posSatu <> 0 Then

                        Dim email = ""
                        If splitData.Count > 3 Then
                            email = splitData(1) & "@djarum.com"
                            flagCountUserLaptopHQ += 1
                        Else
                            flagCountUserLaptopRSO += 1
                            email = splitData(1) & "@sumbercipta.com"
                        End If

                        Dim str As String
                        str = "Insert into Master_UserLaptop([KodeSite],[EmailUserLaptop],[Username]) Values (?,?,?)"
                        cmd = New OleDbCommand(str, Conn)
                        cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                        cmd.Parameters.Add(New OleDbParameter("EmailUserLaptop", email.Replace("""", "")))
                        cmd.Parameters.Add(New OleDbParameter("Username", splitData(0).Replace("""", "")))
                        cmd.ExecuteNonQuery()
                        cmd.Dispose()
                    End If
                    percent = posSatu / lineCount * 100
                    BackgroundWorkerUserLaptop.ReportProgress(percent)
                    posSatu += 1
                Loop
            End Using

            Conn.Close()
        Catch ex As Exception
            MsgBox("Master User Laptop: " & ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorkerUserLaptop_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerUserLaptop.ProgressChanged
        ProgressBar5.Value = e.ProgressPercentage
        lblPUserLaptop.Text = "Uploading" & totalCountUserLaptop.ToString & " data..." + e.ProgressPercentage.ToString & "%"
    End Sub

    Private Sub BackgroundWorkerUserLaptop_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerUserLaptop.RunWorkerCompleted
        Msgbox_okUIDesign.Show()
        Me.Master_UserLaptopTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_UserLaptop)
        lblUserLtp.Text = "Displaying " & flagCountUserLaptopHQ.ToString & " User(s) Laptop HQ | " & flagCountUserLaptopRSO.ToString & " User(s) Laptop RSO"

        ProgressBar5.Hide()
        lblPUserLaptop.Hide()
        lblPUserLaptop.Text = "Starting, please wait..."
    End Sub


    Private Sub BackgroundWorkerRefreshAll_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerRefreshAll.DoWork
        Try
            Dim percent = 0
            textRefreshAll = "Uploading Master Asset... "
            Call Koneksi()
            Call delete("Master_Asset", "KodeSite", kodeSite)

            Dim MyConnection As System.Data.OleDb.OleDbConnection
            Dim dataSet As System.Data.DataSet
            Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
            flagCountAsset = 0

            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFileAsset + ";Extended Properties=Excel 12.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter("Select * from [ASMTR06$]", MyConnection)

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)
            totalCountAsset = dataSet.Tables(0).Rows.Count
            For i = 0 To dataSet.Tables(0).Rows.Count - 1
                If i <> 0 And i <> 1 Then 'cek baris dibaca bukan dari title
                    If dataSet.Tables(0).Rows(i)(0).ToString() <> "" Then

                        percent = (i * 100) / dataSet.Tables(0).Rows.Count
                        BackgroundWorkerRefreshAll.ReportProgress(percent)
                        'BackgroundWorkerRefreshAll.ReportProgress(percent)
                        If dataSet.Tables(0).Rows(i)(18).ToString() <> "RETIRED" Then
                            flagCountAsset += 1
                            Dim str As String
                            str = "Insert into Master_Asset([KodeSite],[IDAsset],[IDAssetParent],[AssetLokalID],[PCName],[User],[PTPemilik],[PTPengguna],[SubBagian],[Lokasi],[Posisi],[Kategori],[DeskripsiAsset],[Merk],[Model],[Tipe],[SubKategori],[Kapasitas],[NoSeri],[Status],[Kondisi],[TglPerolehan],[JtGaransi],[Catatan]) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                            cmd = New OleDbCommand(str, Conn)
                            cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                            cmd.Parameters.Add(New OleDbParameter("IDAsset", dataSet.Tables(0).Rows(i)(0).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("IDAssetParent", dataSet.Tables(0).Rows(i)(1).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("AssetLokalID", dataSet.Tables(0).Rows(i)(2).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PCName", dataSet.Tables(0).Rows(i)(3).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("User", dataSet.Tables(0).Rows(i)(4).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PTPemilik", dataSet.Tables(0).Rows(i)(5).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("PTPengguna", dataSet.Tables(0).Rows(i)(6).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("SubBagian", dataSet.Tables(0).Rows(i)(7).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Lokasi", dataSet.Tables(0).Rows(i)(8).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Posisi", dataSet.Tables(0).Rows(i)(9).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kategori", dataSet.Tables(0).Rows(i)(10).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("DeskripsiAsset", dataSet.Tables(0).Rows(i)(11).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Merk", dataSet.Tables(0).Rows(i)(12).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Model", dataSet.Tables(0).Rows(i)(13).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Tipe", dataSet.Tables(0).Rows(i)(14).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("SubKategori", dataSet.Tables(0).Rows(i)(15).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kapasitas", dataSet.Tables(0).Rows(i)(16).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("NoSeri", dataSet.Tables(0).Rows(i)(17).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Status", dataSet.Tables(0).Rows(i)(18).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Kondisi", dataSet.Tables(0).Rows(i)(19).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("TglPerolehan", dataSet.Tables(0).Rows(i)(20).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("JtGaransi", dataSet.Tables(0).Rows(i)(21).ToString()))
                            cmd.Parameters.Add(New OleDbParameter("Catatan", dataSet.Tables(0).Rows(i)(22).ToString()))
                            cmd.ExecuteNonQuery()
                            cmd.Dispose()
                        End If
                    End If
                End If
            Next
            '-----
            Call delete("Master_Computer", "KodeSite", kodeSite)

            flagCountComputer = 0
            textRefreshAll = "Uploading Master Computer... "
            Dim query = "Select * FROM  [MasterComp$] Where [Computer Name] Is Not Null"
            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathFileComputer + ";Extended Properties=Excel 12.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter(query, MyConnection)

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)

            For rowCounter As Integer = 0 To dataSet.Tables(0).Rows.Count - 1
                Dim emptyColumnCount As Integer = 0
                Dim row As DataRow = dataSet.Tables(0).Rows(rowCounter)

                For Each rowItem In row.ItemArray()
                    If rowItem Is Nothing Or rowItem Is DBNull.Value Or rowItem.Equals("") Then
                        emptyColumnCount += 1
                    End If
                Next

                If emptyColumnCount = dataSet.Tables(0).Columns.Count Then
                    dataSet.Tables(0).Rows.Remove(row)
                End If
            Next
            totalCountComputer = dataSet.Tables(0).Rows.Count
            For i = 0 To dataSet.Tables(0).Rows.Count - 1
                ' If i <> 0 Then
                flagCountComputer += 1
                'Thread.Sleep(25)
                percent = (i * 100) / dataSet.Tables(0).Rows.Count
                BackgroundWorkerRefreshAll.ReportProgress(percent)
                ' If dataSet.Tables(0).Rows(i)(0).ToString() <> "" And dataSet.Tables(0).Rows(i)(0).ToString() <> Nothing Then
                Dim str As String
                str = "Insert into Master_Computer([KodeSite], [ComputerName]) Values (?,?)"
                cmd = New OleDbCommand(str, Conn)
                cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                cmd.Parameters.Add(New OleDbParameter("ComputerName", dataSet.Tables(0).Rows(i)(0).ToString()))
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Next
            '----
            Call delete("Master_Area", "KodeSite", kodeSite)

            flagCountArea = 0
            textRefreshAll = "Uploading Master Area... "
            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathfileArea + ";Extended Properties=Excel 12.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter("Select * from [Ref$]", MyConnection)

            dataSet = New System.Data.DataSet
            MyCommand.Fill(dataSet)
            totalCountArea = dataSet.Tables(0).Rows.Count
            For i = 0 To dataSet.Tables(0).Rows.Count - 1
                If dataSet.Tables(0).Rows(i)(0).ToString() <> "" Then
                    flagCountArea += 1
                    percent = (i * 100) / dataSet.Tables(0).Rows.Count
                    BackgroundWorkerRefreshAll.ReportProgress(percent)

                    Dim str As String
                    str = "Insert into Master_Area([KodeSite],[Area],[Kode],[Initial]) Values (?,?,?,?)"
                    cmd = New OleDbCommand(str, Conn)
                    cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                    cmd.Parameters.Add(New OleDbParameter("Area", dataSet.Tables(0).Rows(i)(0).ToString()))
                    cmd.Parameters.Add(New OleDbParameter("Kode", dataSet.Tables(0).Rows(i)(1).ToString()))
                    cmd.Parameters.Add(New OleDbParameter("Initial", dataSet.Tables(0).Rows(i)(2).ToString()))
                    cmd.ExecuteNonQuery()
                    cmd.Dispose()
                End If
            Next
            '----
            Dim pathUserLaptop = "C:\Users\Kudus Share\source\repos\asset-audit-and-monitoring\Supported Files\"
            Dim deleteData As String = "Delete * from Master_UserLaptop where [KodeSite]=?"
            cmd = New OleDbCommand(deleteData, Conn)
            cmd.Parameters.AddWithValue("KodeSite", kodeSite)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            If kodeSite = "021" Then
                If File.Exists(".\Jakarta-LaptopUsers.csv") Then
                    pathUserLaptop += "Jakarta-LaptopUsers.csv"
                Else
                    pathUserLaptop += "JKT-LaptopUsers.csv"
                End If
            ElseIf kodeSite = "031" Then
                pathUserLaptop += "Bandung-LaptopUsers.csv"
            ElseIf kodeSite = "041" Then
                pathUserLaptop += "Semarang-LaptopUsers.csv"
            ElseIf kodeSite = "011" Then
                pathUserLaptop += "KDS-LaptopUsers.csv"
            ElseIf kodeSite = "012" Then
                pathUserLaptop += "KPY-LaptopUsers.csv"
            ElseIf kodeSite = "015" Then
                pathUserLaptop += "GDM-LaptopUsers.csv"
            ElseIf kodeSite = "051" Then
                pathUserLaptop += ".\Surabaya-LaptopUsers.csv"
            End If

            Dim posSatu = 0
            textRefreshAll = "Uploading Master User Laptop... "
            flagCountUserLaptopHQ = 0
            flagCountUserLaptopRSO = 0
            totalCountUserLaptop = IO.File.ReadAllLines(pathUserLaptop).Length
            Dim lineCount As Integer = IO.File.ReadAllLines(pathUserLaptop).Length - 1
            Using objReader As New StreamReader(pathUserLaptop)
                Do While objReader.Peek() <> -1

                    Dim line As String = objReader.ReadLine
                    Dim splitData() = line.Split(",")
                    If line <> "" And posSatu <> 0 Then
                        'MsgBox("Total Data: " & splitData.Length & vbCrLf & "data 1: " & splitData(0) & vbCrLf & "data 2: " & splitData(1) & vbCrLf & "data 3: " & splitData(2))

                        Dim email = ""
                        If splitData.Count > 3 Then
                            email = splitData(1) & "@djarum.com"
                            flagCountUserLaptopHQ += 1
                        Else
                            flagCountUserLaptopRSO += 1
                            email = splitData(1) & "@sumbercipta.com"
                        End If

                        'MsgBox("nama: " & splitData(0).Replace("""", "") & vbCrLf & "email: " & email.Replace("""", ""))
                        Dim str As String
                        str = "Insert into Master_UserLaptop([KodeSite],[EmailUserLaptop],[Username]) Values (?,?,?)"
                        cmd = New OleDbCommand(str, Conn)
                        cmd.Parameters.Add(New OleDbParameter("KodeSite", kodeSite))
                        cmd.Parameters.Add(New OleDbParameter("EmailUserLaptop", email.Replace("""", "")))
                        cmd.Parameters.Add(New OleDbParameter("Username", splitData(0).Replace("""", "")))
                        cmd.ExecuteNonQuery()
                        cmd.Dispose()
                    End If
                    percent = posSatu / lineCount * 100
                    BackgroundWorkerRefreshAll.ReportProgress(percent)
                    posSatu += 1
                Loop
            End Using
            Conn.Close()

        Catch ex As Exception
            MsgBox("RefreshAll_Do Work_Message: " & ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorkerRefreshAll_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerRefreshAll.ProgressChanged
        progressBarAll.Value = e.ProgressPercentage
        PercentAll.Text = textRefreshAll & e.ProgressPercentage.ToString & "%"

    End Sub

    Private Sub BackgroundWorkerRefreshAll_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerRefreshAll.RunWorkerCompleted

        Me.Master_AssetTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Asset)
        lblCountAsset.Text = "Displaying " + flagCountAsset.ToString + " Asset List(s)"

        Me.Master_ComputerTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Computer)
        lblCountCmp.Text = "Displaying " & flagCountComputer.ToString & " Computer List(s)"

        Me.Master_AreaTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_Area)
        lblAreaList.Text = "Displaying " & flagCountArea.ToString & " Area List(s)"

        Me.Master_UserLaptopTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_UserLaptop)
        lblUserLtp.Text = "Displaying " & flagCountUserLaptopHQ.ToString & " User(s) Laptop HQ | " & flagCountUserLaptopRSO.ToString & " User(s) Laptop RSO"

        progressBarAll.Hide()
        PercentAll.Hide()
        PercentAll.Text = "Starting, please wait..."
        progressBarAll.Value = 0
        Msgbox_okUIDesign.lblMsg.Text = "Data Saved Successfully."
        Msgbox_okUIDesign.Show()
    End Sub
    Private Sub btnSearchAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAsset.Click

        If txtSearchMasterAsset.Text = "" Then
            MasterAssetBindingSource.RemoveFilter()
            lblCountAsset.Text = "Displaying " + flagCountAsset.ToString + " Asset List(s)"
            Exit Sub
        Else

            If comboMasterAsset.Text = "All" Then
                MasterAssetBindingSource.Filter = "(IDAsset LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (IDAssetParent LIKE '%" & txtSearchMasterAsset.Text & "%') OR (AssetLokalID LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (PCName LIKE '%" & txtSearchMasterAsset.Text & "%') OR (User LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (PTPemilik LIKE '%" & txtSearchMasterAsset.Text & "%') OR (PTPengguna LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (SubBagian LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Lokasi LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (Posisi LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Kategori LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (DeskripsiAsset LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Merk LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (Model LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Tipe LIKE '" & txtSearchMasterAsset.Text & "%')" &
                "OR (SubKategori LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Kapasitas LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (NoSeri LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Status LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (Kondisi LIKE '%" & txtSearchMasterAsset.Text & "%') OR (TglPerolehan LIKE '%" & txtSearchMasterAsset.Text & "%')" &
                "OR (JtGaransi LIKE '%" & txtSearchMasterAsset.Text & "%') OR (Catatan LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "ID Asset" Then
                MasterAssetBindingSource.Filter = "(IDAsset LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "ID Asset Parent" Then
                MasterAssetBindingSource.Filter = "(IDAssetParent LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Asset Lokal ID" Then
                MasterAssetBindingSource.Filter = "(AssetLokalID LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "PC Name" Then
                MasterAssetBindingSource.Filter = "(PCName LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "User" Then
                MasterAssetBindingSource.Filter = "(User LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "PT Pemilik" Then
                MasterAssetBindingSource.Filter = "(PTPemilik LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "PT Pengguna" Then
                MasterAssetBindingSource.Filter = "(PTPengguna LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Sub Bagian" Then
                MasterAssetBindingSource.Filter = "(SubBagian LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Lokasi" Then
                MasterAssetBindingSource.Filter = "(Lokasi LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Posisi" Then
                MasterAssetBindingSource.Filter = "(Posisi LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Kategori" Then
                MasterAssetBindingSource.Filter = "(Kategori LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Deskripsi Asset" Then
                MasterAssetBindingSource.Filter = "(DeskripsiAsset LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Merk" Then
                MasterAssetBindingSource.Filter = "(Merk LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Model" Then
                MasterAssetBindingSource.Filter = "(Model LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Tipe" Then
                MasterAssetBindingSource.Filter = "(Tipe LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Sub Kategori" Then
                MasterAssetBindingSource.Filter = "(SubKategori LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Kapasitas" Then
                MasterAssetBindingSource.Filter = "(Kapasitas LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "No Seri" Then
                MasterAssetBindingSource.Filter = "(NoSeri LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Status" Then
                MasterAssetBindingSource.Filter = "(Status LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Kondisi" Then
                MasterAssetBindingSource.Filter = "(Kondisi LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Tgl Perolehan" Then
                MasterAssetBindingSource.Filter = "(TglPerolehan LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Jt Garansi" Then
                MasterAssetBindingSource.Filter = "(JtGaransi LIKE '%" & txtSearchMasterAsset.Text & "%')"
            ElseIf comboMasterAsset.Text = "Catatan" Then
                MasterAssetBindingSource.Filter = "(Catatan LIKE '%" & txtSearchMasterAsset.Text & "%')"
            End If
            If MasterAssetBindingSource.Count <> 0 Then
                With DataGridViewAsset
                    .DataSource = MasterAssetBindingSource
                End With
                lblCountAsset.Text = "Displaying " + MasterAssetBindingSource.Count.ToString + " Asset List(s)"
            Else
                MsgBox("The Search item was not found")
                'MasterAssetBindingSource.Filter = Nothing
                With DataGridViewAsset
                    .DataSource = MasterAssetBindingSource
                End With
            End If
        End If
    End Sub

    Private Sub btnSearchComputer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchComputer.Click
        If txtSearchComputer.Text = "" Then
            MasterComputerBindingSource.RemoveFilter()
            lblCountCmp.Text = "Displaying " + flagCountComputer.ToString + " Computer List(s)"
            Exit Sub
        Else
            MasterComputerBindingSource.Filter = "(ComputerName LIKE '%" & txtSearchComputer.Text & "%')"

            If MasterComputerBindingSource.Count <> 0 Then
                With DataGridViewComputer
                    .DataSource = MasterComputerBindingSource
                End With
                lblCountCmp.Text = "Displaying " + MasterComputerBindingSource.Count.ToString + " Computer List(s)"
            Else
                MsgBox("The Search item was not found")
                'MasterComputerBindingSource.Filter = Nothing
                With DataGridViewComputer
                    .DataSource = MasterComputerBindingSource
                End With
            End If
        End If
    End Sub

    Private Sub btnSearchArea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchArea.Click

        If txtMasterArea.Text = "" Then
            MasterAreaBindingSource.RemoveFilter()
            lblAreaList.Text = "Displaying " + FlagCountArea.ToString + " Area List(s)"
            Exit Sub
        Else

            If comboMasterArea.Text = "All" Then
                MasterAreaBindingSource.Filter = "(Area LIKE '%" & txtMasterArea.Text & "%')" &
                "OR (Initial LIKE '%" & txtMasterArea.Text & "%') OR (Kode LIKE '%" & txtMasterArea.Text & "%')"

            ElseIf comboMasterArea.Text = "Area" Then
                MasterAreaBindingSource.Filter = "(Area LIKE '%" & txtMasterArea.Text & "%')"
            ElseIf comboMasterArea.Text = "Initial" Then
                MasterAreaBindingSource.Filter = "(Initial LIKE '%" & txtMasterArea.Text & "%')"
            ElseIf comboMasterArea.Text = "Kode" Then
                MasterAreaBindingSource.Filter = "(Kode LIKE '%" & txtMasterArea.Text & "%')"
            End If

            If MasterAreaBindingSource.Count <> 0 Then
                With DataGridViewArea
                    .DataSource = MasterAreaBindingSource
                End With
                lblAreaList.Text = "Displaying " + MasterAreaBindingSource.Count.ToString + " Area List(s)"
            Else
                MsgBox("The Search item was not found")
                'MasterAreaBindingSource.Filter = Nothing
                With DataGridViewArea
                    .DataSource = MasterAreaBindingSource
                End With
            End If
        End If
    End Sub

    Private Sub BtnSearchUserLaptop_Click(sender As Object, e As EventArgs) Handles btnSearchUserLaptop.Click
        If txtMasterUserLaptop.Text = "" Then
            MasterUserLaptopBindingSource.RemoveFilter()
            lblUserLtp.Text = "Displaying " & flagCountUserLaptopHQ.ToString & " User(s) Laptop HQ | " & flagCountUserLaptopRSO.ToString & " User(s) Laptop RSO"
            Exit Sub
        Else

            If comboMasterUserLaptop.Text = "All" Then
                MasterUserLaptopBindingSource.Filter = "(Username LIKE '%" & txtMasterUserLaptop.Text & "%')" &
                "OR (EmailUserLaptop LIKE '%" & txtMasterUserLaptop.Text & "%')"

            ElseIf comboMasterUserLaptop.Text = "Username" Then
                MasterUserLaptopBindingSource.Filter = "(Username LIKE '%" & txtMasterUserLaptop.Text & "%')"
            ElseIf comboMasterUserLaptop.Text = "Email" Then
                MasterUserLaptopBindingSource.Filter = "(EmailUserLaptop LIKE '%" & txtMasterUserLaptop.Text & "%')"
            End If

            If MasterUserLaptopBindingSource.Count <> 0 Then

                With DataGridViewUserLaptop
                    .DataSource = MasterUserLaptopBindingSource
                End With
                Dim countHQ = (From row As DataGridViewRow In DataGridViewUserLaptop.Rows
                               Where row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value Like "*djarum*" And row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value IsNot DBNull.Value
                               Select row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value).Count()
                Dim countRSO = (From row As DataGridViewRow In DataGridViewUserLaptop.Rows
                                Where row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value Like "*sumbercipta*" And row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value IsNot DBNull.Value
                                Select row.Cells("EmailUserLaptopDataGridViewTextBoxColumn").Value).Count()
                lblUserLtp.Text = "Displaying " & countHQ.ToString & " User(s) Laptop HQ | " & countRSO.ToString & " User(s) Laptop RSO"
            Else
                MsgBox("The Search item was not found")
                'MasterUserLaptopBindingSource.Filter = Nothing
                With DataGridViewUserLaptop
                    .DataSource = MasterUserLaptopBindingSource
                End With
            End If
        End If
    End Sub

    Private Sub txtSearchMasterAsset_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchMasterAsset.KeyDown

        If e.KeyCode = Keys.Enter Then
            btnSearchAsset.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub txtSearchComputer_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchComputer.KeyDown

        If e.KeyCode = Keys.Enter Then
            btnSearchComputer.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub txtMasterArea_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMasterArea.KeyDown

        If e.KeyCode = Keys.Enter Then
            btnSearchArea.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub txtMasterUserLaptop_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMasterUserLaptop.KeyDown

        If e.KeyCode = Keys.Enter Then
            btnSearchUserLaptop.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub btnTransaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransaction.Click
        Transaction.Show()
        Me.Close()
    End Sub

    Private Sub btnMenuReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Report.Show()
        Me.Close()
    End Sub

    Private Sub btnMenuBackupData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        BackupData.Show()
        Me.Close()
    End Sub

    Private Sub LblLogout_Click(sender As Object, e As EventArgs) Handles lblLogout.Click
        Try
            Login.Show()
            Me.Close()
        Catch ex As Exception
            MsgBox("MasterData_Logout : " & vbCrLf & ex.Message.ToString)
        End Try
    End Sub

End Class