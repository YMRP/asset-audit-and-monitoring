﻿Imports System.ComponentModel
Imports System.IO
Imports Excel = Microsoft.Office.Interop.Excel

Public Class Transaction
    Public Property SplitContainer1 As Object
    Public Property ErrorProvider1 As Object

    Private Sub Transaction_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Trans_AuditAssetTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Trans_AuditAsset)
        Me.CompareDataTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.CompareData)
        Me.Master_NewAssetTableAdapter.Fill(Me.DB_ASSET_AUDIT_V2.Master_NewAsset)

        WindowState = FormWindowState.Maximized
        btnTransaction.FlatStyle = FlatStyle.Flat
        btnTransaction.FlatAppearance.BorderSize = 0

        btnMaster.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnReport.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnValidation.BackColor = Color.FromArgb(255, 2, 43, 58)
        btnBackup.BackColor = Color.FromArgb(255, 2, 43, 58)
        panelMenu.BackColor = Color.FromArgb(255, 2, 43, 58)

        lblUsername.Text = displayName
        lblSite.Text = siteName
        Label7.BackColor = Color.FromArgb(255, 2, 43, 58)

        txtSearchAssetBaru.BorderStyle = BorderStyle.None
        txtSearchAssetBaru.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearchAssetBaru.Height = 21
        txtSearchAssetBaru.AutoSize = False

        txtSearchTransaction_ConnectedData.BorderStyle = BorderStyle.None
        txtSearchTransaction_ConnectedData.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearchTransaction_ConnectedData.Height = 21
        txtSearchTransaction_ConnectedData.AutoSize = False

        txtSearch_NotConnected.BorderStyle = BorderStyle.None
        txtSearch_NotConnected.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearch_NotConnected.Height = 21
        txtSearch_NotConnected.AutoSize = False

        txtSearchTransaction_AccessDenied.BorderStyle = BorderStyle.None
        txtSearchTransaction_AccessDenied.BackColor = Color.FromArgb(255, 225, 229, 242)
        txtSearchTransaction_AccessDenied.Height = 21
        txtSearchTransaction_AccessDenied.AutoSize = False

        comboSearchAssetBaru.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboSearchAssetBaru.Items.Count > 0 Then
            comboSearchAssetBaru.SelectedIndex = 0
        End If

        ComboBoxCategoryRunDataField.BackColor = Color.FromArgb(255, 225, 229, 242)
        If ComboBoxCategoryRunDataField.Items.Count > 0 Then
            ComboBoxCategoryRunDataField.SelectedIndex = 0
        End If

        ComboBoxAreaField.BackColor = Color.FromArgb(255, 225, 229, 242)
        If ComboBoxAreaField.Items.Count > 0 Then
            ComboBoxAreaField.SelectedIndex = 0
        End If

        ComboBoxSubAreaField.BackColor = Color.FromArgb(255, 225, 229, 242)
        If ComboBoxSubAreaField.Items.Count > 0 Then
            ComboBoxSubAreaField.SelectedIndex = 0
        End If

        comboTransaction_ConnectedData.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboTransaction_ConnectedData.Items.Count > 0 Then
            comboTransaction_ConnectedData.SelectedIndex = 0
        End If

        comboField_NotConnected.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboField_NotConnected.Items.Count > 0 Then
            comboField_NotConnected.SelectedIndex = 0
        End If

        comboTransaction_AccessDenied.BackColor = Color.FromArgb(255, 225, 229, 242)
        If comboTransaction_AccessDenied.Items.Count > 0 Then
            comboTransaction_AccessDenied.SelectedIndex = 0
        End If

        btnGetComputer.BackColor = Color.FromArgb(255, 31, 122, 140)

        'Hide field dso/ryn pada get computer asset dan progress run data
        lblSubAreaTitle.Hide()
        lblSubAreaRequired.Hide()
        ComboBoxSubAreaField.Hide()
        ProgressBarProcessPhysicAsset.Hide()
        lblShowPleaseWaitGetPhysicAsset.Hide()

        'Change color button in 'Asset Baru' Tab
        btnDownload_AssetBaru.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSaveNewAsset.BackColor = Color.FromArgb(255, 31, 122, 140)
        'btnDeleteNewAsset.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnUpload_AssetBaru.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSeach_AssetBaru.BackColor = Color.FromArgb(255, 31, 122, 140)

        btnDownloadPhysicAsset.BackColor = Color.FromArgb(255, 31, 122, 140)
        '> connected data
        btnSearchTransaction_ConnectedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnDownload_ConnectedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSave_ConnectedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        '> not connected data
        btnSearch_NotConnected.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnDownload_NotConnectedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSave_NotConnectedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        '> denied data
        btnSearchTransaction_AccessDenied.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnDownload_DeniedData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSave_DeniedData.BackColor = Color.FromArgb(255, 31, 122, 140)

        'Change color button in 'Compare Data' Tab
        btnSave_CompareData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnShow_CompareData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnDownload_ComparableData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnRefresh_CompareData.BackColor = Color.FromArgb(255, 31, 122, 140)
        btnSearch_ComparableData.BackColor = Color.FromArgb(255, 31, 122, 140)

        'Hide progress bar, label percent, & label text process data
        progressBar_AssetBaru.Hide()
        lblPercentage_AssetBaru.Hide()
        lblStatus_AssetBaru.Hide()

        'Hide progressbar, label percent, label text in compare data
        progressBarCompareData.Hide()
        lblPercent_CompareData.Hide()

        'Hide progrssbar, label percent da label please wait in New Asset Tab
        lblStatus_AssetBaru.Hide()
        lblPercentage_AssetBaru.Hide()
        progressBar_AssetBaru.Hide()

        'Declare variable to tamp result of filtering data bindingsource 
        Dim dataViewAssetFisik As New DataView
        Dim dataTableAssetFisik As DataTable
        Dim totalAssetFisik = 0

        'Filter Asset Fisik Binding Source - Connected Data
        AssetFisikBindingSource.Filter = "(KodeSite = '" & AAMModule.kodeSite & "') and (Keterangan LIKE 'Connected%')"
        dataViewAssetFisik = CType(AssetFisikBindingSource.List, DataView)
        dataTableAssetFisik = dataViewAssetFisik.ToTable()
        dgvAssetFisik_ConnectedData.DataSource = dataTableAssetFisik
        totalAssetFisik += dataTableAssetFisik.Rows.Count
        lblTotal_ConnectedData.Text = dataTableAssetFisik.Rows.Count
        'Displaying label total asset with connected status
        lblTotalData_Connected.Text = "Displaying " + dataTableAssetFisik.Rows.Count.ToString + " Physic Asset Lists"

        'Filter Asset Fisik Binding Source - Not Connected Data
        AssetFisikBindingSource.Filter = "(KodeSite = '" & AAMModule.kodeSite & "') and (Keterangan LIKE 'Not Connected%')"
        dataViewAssetFisik = CType(AssetFisikBindingSource.List, DataView)
        dataTableAssetFisik = dataViewAssetFisik.ToTable()
        dgvAssetFisik_NotConnectedData.DataSource = dataTableAssetFisik
        totalAssetFisik += dataTableAssetFisik.Rows.Count
        lblTotal_NotConnectedData.Text = dataTableAssetFisik.Rows.Count
        'Displaying label total asset with connected status
        lblTotalData_NotConnected.Text = "Displaying " + dataTableAssetFisik.Rows.Count.ToString + " Physic Asset Lists"

        'Filter Asset Fisik Binding Source - Denied Data
        AssetFisikBindingSource.Filter = "(KodeSite = '" & AAMModule.kodeSite & "') and (Keterangan LIKE 'Access Denied%')"
        dataViewAssetFisik = CType(AssetFisikBindingSource.List, DataView)
        dataTableAssetFisik = dataViewAssetFisik.ToTable()
        dgvAssetFisik_DeniedData.DataSource = dataTableAssetFisik
        totalAssetFisik += dataTableAssetFisik.Rows.Count
        lblTotal_AccessDeniedData.Text = dataTableAssetFisik.Rows.Count
        'Displaying label total asset with connected status
        lblTotalData_AccessDenied.Text = "Displaying " + dataTableAssetFisik.Rows.Count.ToString + " Physic Asset Lists"

        'Total Physic Asset
        lblTotal_All.Text = totalAssetFisik.ToString

        'Total New Asset
        lblTotalData_AssetBaru.Text = "Displaying " + NewPhysicAssetBindingSource.Count.ToString + " New Asset Lists"
    End Sub
    '----transaksi asset fisik--------------------------------
    Private Sub ComboBoxCategoryRunDataField_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxCategoryRunDataField.SelectedValueChanged
        Dim valueCombo = ComboBoxCategoryRunDataField.SelectedItem
        If valueCombo = "All" Then 'pilih get data apa
            'part show run data 
            lblRunDataFromTitle.Show()
            lblRequiredArea.Show()
            ComboBoxAreaField.Show()

            'part show area
            lblAreaTitle.Show()
            lblRequiredArea.Show()
            ComboBoxAreaField.Show()

        Else 'hanya untuk data not connected atau denied
            'part hide area
            lblAreaTitle.Hide()
            lblRequiredArea.Hide()
            ComboBoxAreaField.Hide()

            'part hide sub area
            lblSubAreaTitle.Hide()
            lblSubAreaRequired.Hide()
            ComboBoxSubAreaField.Hide()

            'ProgressBarProcessPhysicAsset.Location = New Point(267, 201)
            'lblShowPleaseWaitGetPhysicAsset.Location = New Point(267, 201)
        End If
    End Sub

    Private Sub ComboBoxAreaField_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBoxAreaField.SelectedValueChanged
        Dim value = ComboBoxAreaField.SelectedItem

    End Sub

    '---------------------------------------------------------

    Private Sub btnMaster_Click(sender As Object, e As EventArgs) Handles btnMaster.Click
        MasterData.Show()
        Me.Close()
    End Sub



    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Try
            Login.Show()
            Me.Close()
        Catch ex As Exception
            MsgBox("MasterData_Logout : " & vbCrLf & ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnSearchTransaction_ConnectedData_Click(sender As Object, e As EventArgs) Handles btnSearchTransaction_ConnectedData.Click
        AssetFisikBindingSource.RemoveFilter()
        AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Connected%')"
        If txtSearchTransaction_ConnectedData.Text = "" Then
            lblTotalData_Connected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Physic Asset Lists"
        Else

            If comboTransaction_ConnectedData.Text = "All" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (ComputerName LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (Domain LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (Model LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (OSVersion LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (ServiceTagSN LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemSN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemCapacity1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemManufacturer1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemCapacity2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemManufacturer2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemSN3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemCapacity3 LIKE '" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemManufacturer3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemSN4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MemCapacity4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MemManufacturer4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDSN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDCapacity1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDModel1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDCapacity2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDModel2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDSN3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDCapacity3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDModel3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDSN4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (HDDCapacity4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (HDDModel4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MonSN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MonModel1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (MonSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (MonModel2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (UPSSN LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') OR (UPSTimeLeft LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (Keterangan LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')"

            ElseIf comboTransaction_ConnectedData.Text = "Status" Then
                Try
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status = '" & txtSearchTransaction_ConnectedData.Text & "') and (Keterangan LIKE 'Connected%')"
                Catch ex As Exception
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status <> 'true') and (Status <> 'false') and (Keterangan LIKE 'Connected%')"
                    'MsgBox("The Search item was not found" & vbCrLf & "Fill Status Filter with: True (The data has been checked) or False (The data has not been checked)")
                End Try


            ElseIf comboTransaction_ConnectedData.Text = "Uploaded" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Computer Name" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ComputerName LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Domain" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Domain LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Model" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Model LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%' and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "OS Version" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (OSVersion LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Service Tag SN" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ServiceTagSN LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory SN 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemorySN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Capacity 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Manufacturer 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory SN 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Capacity 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Manufacturer 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory SN 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Capacity 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Manufacturer 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory SN 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Capacity 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Memory Manufacturer 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD SN 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Capacity 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Model 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD SN 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Capacity 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Model 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD SN 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Capacity 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Model 3" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel3 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD SN 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Capacity 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "HDD Model 4" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel4 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Monitor SN 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MonSN1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Monitor Model 1" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MonModel1 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Monitor SN 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MonSN2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Monitor Model 2" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MonModel2 LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "UPS SN" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (UPSSN LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "UPS Time Left" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (UPSTimeLeft LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"

            ElseIf comboTransaction_ConnectedData.Text = "Keterangan" Then
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%') and (Keterangan LIKE 'Connected%')"
            End If

            If AssetFisikBindingSource.Count <> 0 Then
                With dgvAssetFisik_ConnectedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_Connected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Asset Lists"
            Else
                MsgBox("The Search item was not found")
                With dgvAssetFisik_ConnectedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_Connected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Asset Lists"
            End If
        End If
    End Sub

    Private Sub btnSearch_NotConnected_Click(sender As Object, e As EventArgs) Handles btnSearch_NotConnected.Click
        AssetFisikBindingSource.RemoveFilter()
        AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Not Connected%')"
        If txtSearch_NotConnected.Text = "" Then
            lblTotalData_NotConnected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Physic Asset Lists"
        Else

            If comboField_NotConnected.Text = "All" Then 'Filter txt kosong OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearch_NotConnected.Text & "%') OR (ComputerName LIKE '%" & txtSearch_NotConnected.Text & "%') and (Keterangan LIKE 'Not Connected%')"

            ElseIf comboField_NotConnected.Text = "Status" Then 'Filter boolean : true/false, outside of that 'Null'
                Try
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status = '" & txtSearch_NotConnected.Text & "') and (Keterangan LIKE 'Not Connected%')"
                Catch ex As Exception
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status <> 'true') and (Status <> 'false') and (Keterangan LIKE 'Not Connected%')"
                End Try

            ElseIf comboField_NotConnected.Text = "Uploaded" Then 'Filter OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearch_NotConnected.Text & "%') and (Keterangan LIKE 'Not Connected%')"

            ElseIf comboField_NotConnected.Text = "Computer Name" Then 'Filter OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ComputerName LIKE '%" & txtSearch_NotConnected.Text & "%') and (Keterangan LIKE 'Not Connected%')"
            End If

            If AssetFisikBindingSource.Count <> 0 Then
                With dgvAssetFisik_NotConnectedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_NotConnected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Asset Lists"
            Else
                MsgBox("The Search item was not found")
                With dgvAssetFisik_NotConnectedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_NotConnected.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Asset Lists"
            End If
        End If
    End Sub

    Private Sub btnSearchTransaction_AccessDenied_Click(sender As Object, e As EventArgs) Handles btnSearchTransaction_AccessDenied.Click
        AssetFisikBindingSource.RemoveFilter()
        AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Access Denied%')"
        If txtSearchTransaction_AccessDenied.Text = "" Then
            lblTotalData_AccessDenied.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Physic Asset Lists"
        Else

            If comboTransaction_AccessDenied.Text = "All" Then 'Filter txt kosong OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchTransaction_AccessDenied.Text & "%') OR (ComputerName LIKE '%" & txtSearchTransaction_AccessDenied.Text & "%') and (Keterangan LIKE 'Access Denied%')"

            ElseIf comboTransaction_AccessDenied.Text = "Status" Then 'Filter boolean : true/false, outside of that 'Null'
                Try
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status = '" & txtSearchTransaction_AccessDenied.Text & "') and (Keterangan LIKE 'Access Denied%')"
                Catch ex As Exception
                    AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status <> 'true') and (Status <> 'false') and (Keterangan LIKE 'Access Denied%')"
                End Try

            ElseIf comboTransaction_AccessDenied.Text = "Uploaded" Then 'Filter OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchTransaction_AccessDenied.Text & "%') and (Keterangan LIKE 'Access Denied%')"

            ElseIf comboTransaction_AccessDenied.Text = "Computer Name" Then 'Filter OK
                AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ComputerName LIKE '%" & txtSearchTransaction_AccessDenied.Text & "%') and (Keterangan LIKE 'Access Denied%')"
            End If

            If AssetFisikBindingSource.Count <> 0 Then
                With dgvAssetFisik_DeniedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_AccessDenied.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Physic Asset Lists"
            Else
                MsgBox("The Search item was not found")
                With dgvAssetFisik_DeniedData
                    .DataSource = AssetFisikBindingSource
                End With
                lblTotalData_AccessDenied.Text = "Displaying " + AssetFisikBindingSource.Count.ToString + " Physic Asset Lists"
            End If
        End If
    End Sub

    Private Sub txtSearchTransaction_ConnectedData_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearchTransaction_ConnectedData.KeyDown
        If e.KeyCode = Keys.Enter Then
            AssetFisikBindingSource.RemoveFilter()
            AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Connected%')"
            btnSearchTransaction_ConnectedData.PerformClick()
        End If
    End Sub
    Private Sub txtSearch_NotConnected_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch_NotConnected.KeyDown
        If e.KeyCode = Keys.Enter Then
            AssetFisikBindingSource.RemoveFilter()
            AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Not Connected%')"
            btnSearch_NotConnected.PerformClick()
        End If
    End Sub
    Private Sub txtSearchTransaction_AccessDenied_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearchTransaction_AccessDenied.KeyDown
        If e.KeyCode = Keys.Enter Then
            AssetFisikBindingSource.RemoveFilter()
            AssetFisikBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Keterangan LIKE 'Access Denied%')"
            btnSearchTransaction_AccessDenied.PerformClick()
        End If
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub btnDownload_ConnectedData_Click(sender As Object, e As EventArgs) Handles btnDownload_ConnectedData.Click
        'Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application
        'If xlApp Is Nothing Then
        '    MsgBox("Excel Is Not properly installed.")
        '    Return
        'End If

        'Dim xlWorkBook As Excel.Workbook
        'Dim xlWorkSheet As Excel.Worksheet
        'Dim misValue As Object = System.Reflection.Missing.Value

        'xlWorkBook = xlApp.Workbooks.Add(misValue)
        'xlWorkSheet = xlWorkBook.Sheets("Sheet1")
        'xlWorkSheet.Cells(1, 1) = "Sheet 1 content"

        'xlWorkBook.SaveAs(".\test.xlsx", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
        ' Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
        'xlWorkBook.Close(True, misValue, misValue)
        'xlApp.Quit()

        'releaseObject(xlWorkSheet)
        'releaseObject(xlWorkBook)
        'releaseObject(xlApp)

        MessageBox.Show("Excel file created , you can find the file d:\csharp-Excel.xls")

    End Sub
    Private Sub btnSeach_AssetBaru_Click(sender As Object, e As EventArgs) Handles btnSeach_AssetBaru.Click
        NewPhysicAssetBindingSource.RemoveFilter()
        NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "')"
        If txtSearchAssetBaru.Text = "" Then
            lblTotalData_AssetBaru.Text = "Displaying " + NewPhysicAssetBindingSource.Count.ToString + " Physic Asset Lists"
        Else

            If comboSearchAssetBaru.Text = "All" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchAssetBaru.Text & "%') OR (ComputerName LIKE '%" & txtSearchTransaction_ConnectedData.Text & "%')" &
                "OR (Domain LIKE '%" & txtSearchAssetBaru.Text & "%') OR (Model LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (OSVersion LIKE '%" & txtSearchAssetBaru.Text & "%') OR (ServiceTagSN LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemSN1 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemCapacity1 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemManufacturer1 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemSN2 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemCapacity2 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemManufacturer2 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemSN3 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemCapacity3 LIKE '" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemManufacturer3 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemSN4 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (MemCapacity4 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (MemManufacturer4 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDSN1 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDCapacity1 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDModel1 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDSN2 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDCapacity2 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDModel2 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDSN3 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDCapacity3 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDModel3 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDSN4 LIKE '%" & txtSearchAssetBaru.Text & "%')" &
                "OR (HDDCapacity4 LIKE '%" & txtSearchAssetBaru.Text & "%') OR (HDDModel4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Status" Then
                Try
                    NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status = '" & txtSearchAssetBaru.Text & "')"
                Catch ex As Exception
                    NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Status <> 'true') and (Status <> 'false')"
                End Try


            ElseIf comboSearchAssetBaru.Text = "Uploaded" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Uploaded LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Computer Name" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ComputerName LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Domain" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Domain LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Model" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (Model LIKE '%" & txtSearchAssetBaru.Text & "%"

            ElseIf comboSearchAssetBaru.Text = "OS Version" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (OSVersion LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Service Tag SN" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ServiceTagSN LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory SN 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemorySN1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Capacity 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Manufacturer 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory SN 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Capacity 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Manufacturer 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory SN 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Capacity 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Manufacturer 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory SN 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemSN4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Capacity 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemCapacity4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "Memory Manufacturer 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (MemManufacturer4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD SN 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Capacity 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Model 1" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel1 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD SN 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Capacity 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Model 2" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel2 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD SN 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Capacity 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Model 3" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel3 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD SN 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDSN4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Capacity 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDCapacity4 LIKE '%" & txtSearchAssetBaru.Text & "%')"

            ElseIf comboSearchAssetBaru.Text = "HDD Model 4" Then
                NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (HDDModel4 LIKE '%" & txtSearchAssetBaru.Text & "%')"
            End If

            If NewPhysicAssetBindingSource.Count <> 0 Then
                With dgvAssetBaru
                    .DataSource = NewPhysicAssetBindingSource
                End With
                lblTotalData_AssetBaru.Text = "Displaying " + NewPhysicAssetBindingSource.Count.ToString + " Asset Lists"
            Else
                MsgBox("The Search item was not found")
                With dgvAssetBaru
                    .DataSource = NewPhysicAssetBindingSource
                End With
                lblTotalData_AssetBaru.Text = "Displaying " + NewPhysicAssetBindingSource.Count.ToString + " Asset Lists"
            End If
        End If
    End Sub

    Private Sub btnUpload_AssetBaru_Click(sender As Object, e As EventArgs) Handles btnUpload_AssetBaru.Click
        lblStatus_AssetBaru.Show()
        lblPercentage_AssetBaru.Show()
        progressBar_AssetBaru.Show()

        'lblStatusNA.Text = "Starting, Please Wait. . ."
        'lblStatusNA.Text = "Please Wait. . .(Uploading " & lineCount & " Data)"
        NewPhysicAssetBackgroundWorker.RunWorkerAsync()
    End Sub

    Private Sub NewPhysicAssetBackgroundWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles NewPhysicAssetBackgroundWorker.DoWork
        Call Koneksi()
        Dim percent = 0
        Dim indeks = 1
        Dim pathAssetBaru = "D:\Audit Asset and Monitoring 2019\Supported Files\7060 60unit 20190909.txt"
        Dim lineCount = File.ReadAllLines(pathAssetBaru).Length

        Using objReader As New StreamReader(pathAssetBaru)
            Do While objReader.Peek() <> -1
                Dim line As String = objReader.ReadLine()
                Dim splitLine() As String = line.Split(";")
                If line <> "" And indeks <> 0 Then
                    percent = (indeks * 100) / lineCount
                    NewPhysicAssetBackgroundWorker.ReportProgress(percent)
                    'Checking data has been added in database
                    NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "') and (ServiceTagSN = '" & splitLine(4).ToString & "')"
                    'MsgBox(line)
                    If NewPhysicAssetBindingSource.Count = 0 Then
                        Dim insertNewAsset = "Insert into Master_NewAsset([KodeSite],[Status],[Uploaded],[ComputerName],[Domain]," &
                            "[Model],[OSVersion],[ServiceTagSN],[MemSN1],[MemCapacity1]," &
                            "[MemManufacturer1],[MemSN2],[MemCapacity2],[MemManufacturer2],[MemSN3]," &
                            "[MemCapacity3],[MemManufacturer3],[MemSN4],[MemCapacity4],[MemManufacturer4]," &
                            "[HDDSN1],[HDDCapacity1],[HDDModel1],[HDDSN2],[HDDCapacity2]," &
                            "[HDDModel2],[HDDSN3],[HDDCapacity3],[HDDModel3],[HDDSN4]," &
                            "[HDDCapacity4],[HDDModel4])" &
                            " Values ('" & kodeSite & "','0','" & DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") & "', '" & splitLine(0).ToString & "'," &
                            "'" & splitLine(1).ToString & "','" & splitLine(2).ToString & "','" & splitLine(3).ToString & "','" & splitLine(4).ToString & "'," &
                            "'" & splitLine(5).ToString & "','" & splitLine(6).ToString & "','" & splitLine(7).ToString & "','" & splitLine(8).ToString & "'," &
                            "'" & splitLine(9).ToString & "','" & splitLine(10).ToString & "','" & splitLine(11).ToString & "','" & splitLine(12).ToString & "'," &
                            "'" & splitLine(13).ToString & "','" & splitLine(14).ToString & "','" & splitLine(15).ToString & "','" & splitLine(16).ToString & "'," &
                            "'" & splitLine(17).ToString & "','" & splitLine(18).ToString & "','" & splitLine(19).ToString & "','" & splitLine(20).ToString & "'," &
                            "'" & splitLine(21).ToString & "','" & splitLine(22).ToString & "','" & splitLine(23).ToString & "','" & splitLine(24).ToString & "'," &
                            "'" & splitLine(25).ToString & "','" & splitLine(26).ToString & "','" & splitLine(27).ToString & "','" & splitLine(28).ToString & "')"
                        insert(insertNewAsset)
                    Else
                        Dim updateNewAsset As String = "Update Master_NewAsset SET [ComputerName]='" & splitLine(0).ToString & "', [Domain]='" & splitLine(1).ToString & "', [Model]='" & splitLine(2).ToString & "', [OSVersion]='" & splitLine(3).ToString & "', [MemSN1]='" & splitLine(5).ToString & "', " &
                        "[MemCapacity1]='" & splitLine(6).ToString & "', [MemManufacturer1]='" & splitLine(7).ToString & "', [MemSN2]='" & splitLine(8).ToString & "', [MemCapacity2]='" & splitLine(9).ToString & "', [MemManufacturer2]='" & splitLine(10).ToString & "', " &
                        "[MemSN3]='" & splitLine(11).ToString & "', [MemCapacity3]='" & splitLine(12).ToString & "', [MemManufacturer3]='" & splitLine(13).ToString & "', [MemSN4]='" & splitLine(14).ToString & "', [MemCapacity4]='" & splitLine(15).ToString & "'," &
                        "[MemManufacturer4]='" & splitLine(16).ToString & "', [HDDSN1]='" & splitLine(17).ToString & "', [HDDCapacity1]='" & splitLine(18).ToString & "',[HDDModel1]='" & splitLine(19).ToString & "', [HDDSN2]='" & splitLine(20).ToString & "'," &
                        "[HDDCapacity2]='" & splitLine(21).ToString & "',[HDDModel2]='" & splitLine(22).ToString & "', [HDDSN3]='" & splitLine(23).ToString & "', [HDDCapacity3]='" & splitLine(24).ToString & "',[HDDModel3]='" & splitLine(25).ToString & "'," &
                        "[HDDSN4]='" & splitLine(26).ToString & "', [HDDCapacity4]='" & splitLine(27).ToString & "',[HDDModel4]='" & splitLine(28).ToString & "' where [KodeSite]='" & kodeSite & "' and [ServiceTagSN]='" & splitLine(4).ToString & "'"
                        AAMModule.update(updateNewAsset)
                    End If
                End If
                indeks += 1
            Loop
        End Using
        Conn.Close()
    End Sub

    Private Sub NewPhysicAssetBackgroundWorker_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles NewPhysicAssetBackgroundWorker.ProgressChanged
        progressBar_AssetBaru.Value = e.ProgressPercentage
        lblPercentage_AssetBaru.Text = e.ProgressPercentage.ToString & "%"
    End Sub

    Private Sub NewPhysicAssetBackgroundWorker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles NewPhysicAssetBackgroundWorker.RunWorkerCompleted
        Msgbox_okUIDesign.Show()
        NewPhysicAssetBindingSource.Filter = "(KodeSite = '" & kodeSite & "')"
        With dgvAssetBaru
            .DataSource = NewPhysicAssetBindingSource
        End With
        lblStatus_AssetBaru.Hide()
        lblPercentage_AssetBaru.Hide()
        progressBar_AssetBaru.Hide()
    End Sub


End Class