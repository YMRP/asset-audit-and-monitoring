﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Transaction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Transaction))
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TabControlTransaction = New System.Windows.Forms.TabControl()
        Me.TabAssetBaru = New System.Windows.Forms.TabPage()
        Me.dgvAssetBaru = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.UploadedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComputerNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DomainDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OSVersionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServiceTagSNDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NewPhysicAssetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DBASSETAUDITV2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_ASSET_AUDIT_V2 = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2()
        Me.lblTotalData_AssetBaru = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.comboSearchAssetBaru = New System.Windows.Forms.ComboBox()
        Me.txtSearchAssetBaru = New System.Windows.Forms.TextBox()
        Me.btnSeach_AssetBaru = New System.Windows.Forms.Button()
        Me.btnDownload_AssetBaru = New System.Windows.Forms.Button()
        Me.lblPercentage_AssetBaru = New System.Windows.Forms.Label()
        Me.lblStatus_AssetBaru = New System.Windows.Forms.Label()
        Me.progressBar_AssetBaru = New System.Windows.Forms.ProgressBar()
        Me.lbl_formatNewAsset = New System.Windows.Forms.Label()
        Me.btnUpload_AssetBaru = New System.Windows.Forms.Button()
        Me.btnSaveNewAsset = New System.Windows.Forms.Button()
        Me.lblDateTimeNewRegis = New System.Windows.Forms.Label()
        Me.TabAssetFisik = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblTotal_AccessDeniedData = New System.Windows.Forms.Label()
        Me.lblTotal_NotConnectedData = New System.Windows.Forms.Label()
        Me.lblTotal_ConnectedData = New System.Windows.Forms.Label()
        Me.lblTotal_All = New System.Windows.Forms.Label()
        Me.lbl_ketTotalConOldAsset = New System.Windows.Forms.Label()
        Me.lbl_ketTotalDeniedOldAsset = New System.Windows.Forms.Label()
        Me.lbl_ketTotalNotConOldAsset = New System.Windows.Forms.Label()
        Me.lbl_ketTotalAllOldAsset = New System.Windows.Forms.Label()
        Me.btnDownloadPhysicAsset = New System.Windows.Forms.Button()
        Me.panelGetPhysicForm = New System.Windows.Forms.Panel()
        Me.btnGetComputer = New System.Windows.Forms.Button()
        Me.lblSubAreaRequired = New System.Windows.Forms.Label()
        Me.lblSubAreaTitle = New System.Windows.Forms.Label()
        Me.ComboBoxSubAreaField = New System.Windows.Forms.ComboBox()
        Me.lblRequiredArea = New System.Windows.Forms.Label()
        Me.lblRunDataFromRequired = New System.Windows.Forms.Label()
        Me.ProgressBarProcessPhysicAsset = New System.Windows.Forms.ProgressBar()
        Me.lblShowPleaseWaitGetPhysicAsset = New System.Windows.Forms.Label()
        Me.ComboBoxAreaField = New System.Windows.Forms.ComboBox()
        Me.lblAreaTitle = New System.Windows.Forms.Label()
        Me.ComboBoxCategoryRunDataField = New System.Windows.Forms.ComboBox()
        Me.lblRunDataFromTitle = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnSave_ConnectedData = New System.Windows.Forms.Button()
        Me.lblTotalData_Connected = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.comboTransaction_ConnectedData = New System.Windows.Forms.ComboBox()
        Me.txtSearchTransaction_ConnectedData = New System.Windows.Forms.TextBox()
        Me.btnSearchTransaction_ConnectedData = New System.Windows.Forms.Button()
        Me.btnDownload_ConnectedData = New System.Windows.Forms.Button()
        Me.dgvAssetFisik_ConnectedData = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.UploadedDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComputerNameDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DomainDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModelDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OSVersionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServiceTagSNDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemSN4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemCapacity4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemManufacturer4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN1DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN2DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN3DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN4DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDCapacity4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDModel4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonSN1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonModel1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonSN2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonModel2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPSSNDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPSTimeLeftDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KeteranganDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AssetFisikBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lblTotalData_NotConnected = New System.Windows.Forms.Label()
        Me.btnSave_NotConnectedData = New System.Windows.Forms.Button()
        Me.PanelSearch_NotConnected = New System.Windows.Forms.Panel()
        Me.comboField_NotConnected = New System.Windows.Forms.ComboBox()
        Me.txtSearch_NotConnected = New System.Windows.Forms.TextBox()
        Me.btnSearch_NotConnected = New System.Windows.Forms.Button()
        Me.btnDownload_NotConnectedData = New System.Windows.Forms.Button()
        Me.dgvAssetFisik_NotConnectedData = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.UploadedDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComputerNameDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KeteranganDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.lblTotalData_AccessDenied = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.comboTransaction_AccessDenied = New System.Windows.Forms.ComboBox()
        Me.txtSearchTransaction_AccessDenied = New System.Windows.Forms.TextBox()
        Me.btnSearchTransaction_AccessDenied = New System.Windows.Forms.Button()
        Me.btnDownload_DeniedData = New System.Windows.Forms.Button()
        Me.btnSave_DeniedData = New System.Windows.Forms.Button()
        Me.dgvAssetFisik_DeniedData = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewCheckBoxColumn4 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.UploadedDataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComputerNameDataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KeteranganDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabCompareData = New System.Windows.Forms.TabPage()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.comboSearch_ComparableData = New System.Windows.Forms.ComboBox()
        Me.txtSearch_ComparableData = New System.Windows.Forms.TextBox()
        Me.btnSearch_ComparableData = New System.Windows.Forms.Button()
        Me.btnDownload_ComparableData = New System.Windows.Forms.Button()
        Me.lblTotal_ComparableData = New System.Windows.Forms.Label()
        Me.btnShow_CompareData = New System.Windows.Forms.Button()
        Me.btnSave_CompareData = New System.Windows.Forms.Button()
        Me.dgvCompareData = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.StatusNMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComputerNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServiceTagSNDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN2DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HDDSN4DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemorySizeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemoryQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonSN1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonSN2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPSSNDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompareDataBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblPercent_CompareData = New System.Windows.Forms.Label()
        Me.progressBarCompareData = New System.Windows.Forms.ProgressBar()
        Me.btnRefresh_CompareData = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblSumSimilar = New System.Windows.Forms.Label()
        Me.lbl_ketHijauCompData = New System.Windows.Forms.Label()
        Me.lblCountDataTidakSesuai_CompareData = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblSumBeda = New System.Windows.Forms.Label()
        Me.lblCountDataSesuai_CompareData = New System.Windows.Forms.Label()
        Me.lblCountDataMendekatiSesuai_CompareData = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.sumAllData = New System.Windows.Forms.Label()
        Me.lblAllData = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblSumNotChecked = New System.Windows.Forms.Label()
        Me.lblSumChecked = New System.Windows.Forms.Label()
        Me.sumChecked_CompareData = New System.Windows.Forms.Label()
        Me.sumNotChecked_CompareData = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_ketMerahCompData = New System.Windows.Forms.Label()
        Me.TabCompareAVADUC = New System.Windows.Forms.TabPage()
        Me.AssetBaruBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LineShape5 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.panelMenu = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnLogout = New System.Windows.Forms.Label()
        Me.lblSite = New System.Windows.Forms.Label()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.btnBackup = New System.Windows.Forms.Button()
        Me.lbl_PenandaTabTrans = New System.Windows.Forms.Label()
        Me.lblLogout = New System.Windows.Forms.Label()
        Me.btnValidation = New System.Windows.Forms.Button()
        Me.btnMaster = New System.Windows.Forms.Button()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.btnTransaction = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.container = New System.Windows.Forms.Panel()
        Me.BackgroundWorkerGetPhysic = New System.ComponentModel.BackgroundWorker()
        Me.Master_NewAssetTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Master_NewAssetTableAdapter()
        Me.CompareDataTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.CompareDataTableAdapter()
        Me.Trans_AuditAssetTableAdapter = New Asset_Audit_and_Monitoring.DB_ASSET_AUDIT_V2TableAdapters.Trans_AuditAssetTableAdapter()
        Me.NewPhysicAssetBackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.TabControlTransaction.SuspendLayout()
        Me.TabAssetBaru.SuspendLayout()
        CType(Me.dgvAssetBaru, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewPhysicAssetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBASSETAUDITV2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_ASSET_AUDIT_V2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        Me.TabAssetFisik.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.panelGetPhysicForm.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.dgvAssetFisik_ConnectedData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AssetFisikBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.PanelSearch_NotConnected.SuspendLayout()
        CType(Me.dgvAssetFisik_NotConnectedData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.dgvAssetFisik_DeniedData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCompareData.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.dgvCompareData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompareDataBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.AssetBaruBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelMenu.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.container.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControlTransaction
        '
        Me.TabControlTransaction.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControlTransaction.Controls.Add(Me.TabAssetBaru)
        Me.TabControlTransaction.Controls.Add(Me.TabAssetFisik)
        Me.TabControlTransaction.Controls.Add(Me.TabCompareData)
        Me.TabControlTransaction.Controls.Add(Me.TabCompareAVADUC)
        Me.TabControlTransaction.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlTransaction.ItemSize = New System.Drawing.Size(57, 32)
        Me.TabControlTransaction.Location = New System.Drawing.Point(-2, 0)
        Me.TabControlTransaction.Multiline = True
        Me.TabControlTransaction.Name = "TabControlTransaction"
        Me.TabControlTransaction.SelectedIndex = 0
        Me.TabControlTransaction.Size = New System.Drawing.Size(1181, 623)
        Me.TabControlTransaction.TabIndex = 3
        '
        'TabAssetBaru
        '
        Me.TabAssetBaru.BackColor = System.Drawing.Color.White
        Me.TabAssetBaru.Controls.Add(Me.dgvAssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.lblTotalData_AssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.Panel10)
        Me.TabAssetBaru.Controls.Add(Me.lblPercentage_AssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.lblStatus_AssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.progressBar_AssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.lbl_formatNewAsset)
        Me.TabAssetBaru.Controls.Add(Me.btnUpload_AssetBaru)
        Me.TabAssetBaru.Controls.Add(Me.btnSaveNewAsset)
        Me.TabAssetBaru.Controls.Add(Me.lblDateTimeNewRegis)
        Me.TabAssetBaru.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabAssetBaru.Location = New System.Drawing.Point(4, 36)
        Me.TabAssetBaru.Name = "TabAssetBaru"
        Me.TabAssetBaru.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAssetBaru.Size = New System.Drawing.Size(1173, 583)
        Me.TabAssetBaru.TabIndex = 99
        Me.TabAssetBaru.Text = "Asset Baru"
        '
        'dgvAssetBaru
        '
        Me.dgvAssetBaru.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAssetBaru.AutoGenerateColumns = False
        Me.dgvAssetBaru.BackgroundColor = System.Drawing.Color.White
        Me.dgvAssetBaru.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAssetBaru.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvAssetBaru.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssetBaru.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewCheckBoxColumn, Me.UploadedDataGridViewTextBoxColumn, Me.ComputerNameDataGridViewTextBoxColumn, Me.DomainDataGridViewTextBoxColumn, Me.ModelDataGridViewTextBoxColumn, Me.OSVersionDataGridViewTextBoxColumn, Me.ServiceTagSNDataGridViewTextBoxColumn, Me.MemSN1DataGridViewTextBoxColumn, Me.MemCapacity1DataGridViewTextBoxColumn, Me.MemManufacturer1DataGridViewTextBoxColumn, Me.MemSN2DataGridViewTextBoxColumn, Me.MemCapacity2DataGridViewTextBoxColumn, Me.MemManufacturer2DataGridViewTextBoxColumn, Me.MemSN3DataGridViewTextBoxColumn, Me.MemCapacity3DataGridViewTextBoxColumn, Me.MemManufacturer3DataGridViewTextBoxColumn, Me.MemSN4DataGridViewTextBoxColumn, Me.MemCapacity4DataGridViewTextBoxColumn, Me.MemManufacturer4DataGridViewTextBoxColumn, Me.HDDSN1DataGridViewTextBoxColumn, Me.HDDCapacity1DataGridViewTextBoxColumn, Me.HDDModel1DataGridViewTextBoxColumn, Me.HDDSN2DataGridViewTextBoxColumn, Me.HDDCapacity2DataGridViewTextBoxColumn, Me.HDDModel2DataGridViewTextBoxColumn, Me.HDDSN3DataGridViewTextBoxColumn, Me.HDDCapacity3DataGridViewTextBoxColumn, Me.HDDModel3DataGridViewTextBoxColumn, Me.HDDSN4DataGridViewTextBoxColumn, Me.HDDCapacity4DataGridViewTextBoxColumn, Me.HDDModel4DataGridViewTextBoxColumn})
        Me.dgvAssetBaru.DataSource = Me.NewPhysicAssetBindingSource
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAssetBaru.DefaultCellStyle = DataGridViewCellStyle18
        Me.dgvAssetBaru.Location = New System.Drawing.Point(1, 33)
        Me.dgvAssetBaru.Name = "dgvAssetBaru"
        Me.dgvAssetBaru.Size = New System.Drawing.Size(1169, 531)
        Me.dgvAssetBaru.TabIndex = 112
        '
        'StatusDataGridViewCheckBoxColumn
        '
        Me.StatusDataGridViewCheckBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn.Name = "StatusDataGridViewCheckBoxColumn"
        '
        'UploadedDataGridViewTextBoxColumn
        '
        Me.UploadedDataGridViewTextBoxColumn.DataPropertyName = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn.HeaderText = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn.Name = "UploadedDataGridViewTextBoxColumn"
        '
        'ComputerNameDataGridViewTextBoxColumn
        '
        Me.ComputerNameDataGridViewTextBoxColumn.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn.Name = "ComputerNameDataGridViewTextBoxColumn"
        '
        'DomainDataGridViewTextBoxColumn
        '
        Me.DomainDataGridViewTextBoxColumn.DataPropertyName = "Domain"
        Me.DomainDataGridViewTextBoxColumn.HeaderText = "Domain"
        Me.DomainDataGridViewTextBoxColumn.Name = "DomainDataGridViewTextBoxColumn"
        '
        'ModelDataGridViewTextBoxColumn
        '
        Me.ModelDataGridViewTextBoxColumn.DataPropertyName = "Model"
        Me.ModelDataGridViewTextBoxColumn.HeaderText = "Model"
        Me.ModelDataGridViewTextBoxColumn.Name = "ModelDataGridViewTextBoxColumn"
        '
        'OSVersionDataGridViewTextBoxColumn
        '
        Me.OSVersionDataGridViewTextBoxColumn.DataPropertyName = "OSVersion"
        Me.OSVersionDataGridViewTextBoxColumn.HeaderText = "OS Version"
        Me.OSVersionDataGridViewTextBoxColumn.Name = "OSVersionDataGridViewTextBoxColumn"
        '
        'ServiceTagSNDataGridViewTextBoxColumn
        '
        Me.ServiceTagSNDataGridViewTextBoxColumn.DataPropertyName = "ServiceTagSN"
        Me.ServiceTagSNDataGridViewTextBoxColumn.HeaderText = "Service Tag SN"
        Me.ServiceTagSNDataGridViewTextBoxColumn.Name = "ServiceTagSNDataGridViewTextBoxColumn"
        '
        'MemSN1DataGridViewTextBoxColumn
        '
        Me.MemSN1DataGridViewTextBoxColumn.DataPropertyName = "MemSN1"
        Me.MemSN1DataGridViewTextBoxColumn.HeaderText = "Memory SN 1"
        Me.MemSN1DataGridViewTextBoxColumn.Name = "MemSN1DataGridViewTextBoxColumn"
        '
        'MemCapacity1DataGridViewTextBoxColumn
        '
        Me.MemCapacity1DataGridViewTextBoxColumn.DataPropertyName = "MemCapacity1"
        Me.MemCapacity1DataGridViewTextBoxColumn.HeaderText = "Memory Capacity 1"
        Me.MemCapacity1DataGridViewTextBoxColumn.Name = "MemCapacity1DataGridViewTextBoxColumn"
        '
        'MemManufacturer1DataGridViewTextBoxColumn
        '
        Me.MemManufacturer1DataGridViewTextBoxColumn.DataPropertyName = "MemManufacturer1"
        Me.MemManufacturer1DataGridViewTextBoxColumn.HeaderText = "Memory Manufacturer 1"
        Me.MemManufacturer1DataGridViewTextBoxColumn.Name = "MemManufacturer1DataGridViewTextBoxColumn"
        '
        'MemSN2DataGridViewTextBoxColumn
        '
        Me.MemSN2DataGridViewTextBoxColumn.DataPropertyName = "MemSN2"
        Me.MemSN2DataGridViewTextBoxColumn.HeaderText = "Memory SN 2"
        Me.MemSN2DataGridViewTextBoxColumn.Name = "MemSN2DataGridViewTextBoxColumn"
        '
        'MemCapacity2DataGridViewTextBoxColumn
        '
        Me.MemCapacity2DataGridViewTextBoxColumn.DataPropertyName = "MemCapacity2"
        Me.MemCapacity2DataGridViewTextBoxColumn.HeaderText = "Memory Capacity 2"
        Me.MemCapacity2DataGridViewTextBoxColumn.Name = "MemCapacity2DataGridViewTextBoxColumn"
        '
        'MemManufacturer2DataGridViewTextBoxColumn
        '
        Me.MemManufacturer2DataGridViewTextBoxColumn.DataPropertyName = "MemManufacturer2"
        Me.MemManufacturer2DataGridViewTextBoxColumn.HeaderText = "Memory Manufacturer 2"
        Me.MemManufacturer2DataGridViewTextBoxColumn.Name = "MemManufacturer2DataGridViewTextBoxColumn"
        '
        'MemSN3DataGridViewTextBoxColumn
        '
        Me.MemSN3DataGridViewTextBoxColumn.DataPropertyName = "MemSN3"
        Me.MemSN3DataGridViewTextBoxColumn.HeaderText = "Memory SN 3"
        Me.MemSN3DataGridViewTextBoxColumn.Name = "MemSN3DataGridViewTextBoxColumn"
        '
        'MemCapacity3DataGridViewTextBoxColumn
        '
        Me.MemCapacity3DataGridViewTextBoxColumn.DataPropertyName = "MemCapacity3"
        Me.MemCapacity3DataGridViewTextBoxColumn.HeaderText = "Memory Capacity 3"
        Me.MemCapacity3DataGridViewTextBoxColumn.Name = "MemCapacity3DataGridViewTextBoxColumn"
        '
        'MemManufacturer3DataGridViewTextBoxColumn
        '
        Me.MemManufacturer3DataGridViewTextBoxColumn.DataPropertyName = "MemManufacturer3"
        Me.MemManufacturer3DataGridViewTextBoxColumn.HeaderText = "Memory Manufacturer 3"
        Me.MemManufacturer3DataGridViewTextBoxColumn.Name = "MemManufacturer3DataGridViewTextBoxColumn"
        '
        'MemSN4DataGridViewTextBoxColumn
        '
        Me.MemSN4DataGridViewTextBoxColumn.DataPropertyName = "MemSN4"
        Me.MemSN4DataGridViewTextBoxColumn.HeaderText = "Memory SN 4"
        Me.MemSN4DataGridViewTextBoxColumn.Name = "MemSN4DataGridViewTextBoxColumn"
        '
        'MemCapacity4DataGridViewTextBoxColumn
        '
        Me.MemCapacity4DataGridViewTextBoxColumn.DataPropertyName = "MemCapacity4"
        Me.MemCapacity4DataGridViewTextBoxColumn.HeaderText = "Memory Capacity 4"
        Me.MemCapacity4DataGridViewTextBoxColumn.Name = "MemCapacity4DataGridViewTextBoxColumn"
        '
        'MemManufacturer4DataGridViewTextBoxColumn
        '
        Me.MemManufacturer4DataGridViewTextBoxColumn.DataPropertyName = "MemManufacturer4"
        Me.MemManufacturer4DataGridViewTextBoxColumn.HeaderText = "Memory Manufacturer 4"
        Me.MemManufacturer4DataGridViewTextBoxColumn.Name = "MemManufacturer4DataGridViewTextBoxColumn"
        '
        'HDDSN1DataGridViewTextBoxColumn
        '
        Me.HDDSN1DataGridViewTextBoxColumn.DataPropertyName = "HDDSN1"
        Me.HDDSN1DataGridViewTextBoxColumn.HeaderText = "HDD SN 1"
        Me.HDDSN1DataGridViewTextBoxColumn.Name = "HDDSN1DataGridViewTextBoxColumn"
        '
        'HDDCapacity1DataGridViewTextBoxColumn
        '
        Me.HDDCapacity1DataGridViewTextBoxColumn.DataPropertyName = "HDDCapacity1"
        Me.HDDCapacity1DataGridViewTextBoxColumn.HeaderText = "HDD Capacity 1"
        Me.HDDCapacity1DataGridViewTextBoxColumn.Name = "HDDCapacity1DataGridViewTextBoxColumn"
        '
        'HDDModel1DataGridViewTextBoxColumn
        '
        Me.HDDModel1DataGridViewTextBoxColumn.DataPropertyName = "HDDModel1"
        Me.HDDModel1DataGridViewTextBoxColumn.HeaderText = "HDD Model 1"
        Me.HDDModel1DataGridViewTextBoxColumn.Name = "HDDModel1DataGridViewTextBoxColumn"
        '
        'HDDSN2DataGridViewTextBoxColumn
        '
        Me.HDDSN2DataGridViewTextBoxColumn.DataPropertyName = "HDDSN2"
        Me.HDDSN2DataGridViewTextBoxColumn.HeaderText = "HDD SN 2"
        Me.HDDSN2DataGridViewTextBoxColumn.Name = "HDDSN2DataGridViewTextBoxColumn"
        '
        'HDDCapacity2DataGridViewTextBoxColumn
        '
        Me.HDDCapacity2DataGridViewTextBoxColumn.DataPropertyName = "HDDCapacity2"
        Me.HDDCapacity2DataGridViewTextBoxColumn.HeaderText = "HDD Capacity 2"
        Me.HDDCapacity2DataGridViewTextBoxColumn.Name = "HDDCapacity2DataGridViewTextBoxColumn"
        '
        'HDDModel2DataGridViewTextBoxColumn
        '
        Me.HDDModel2DataGridViewTextBoxColumn.DataPropertyName = "HDDModel2"
        Me.HDDModel2DataGridViewTextBoxColumn.HeaderText = "HDD Model 2"
        Me.HDDModel2DataGridViewTextBoxColumn.Name = "HDDModel2DataGridViewTextBoxColumn"
        '
        'HDDSN3DataGridViewTextBoxColumn
        '
        Me.HDDSN3DataGridViewTextBoxColumn.DataPropertyName = "HDDSN3"
        Me.HDDSN3DataGridViewTextBoxColumn.HeaderText = "HDD SN 3"
        Me.HDDSN3DataGridViewTextBoxColumn.Name = "HDDSN3DataGridViewTextBoxColumn"
        '
        'HDDCapacity3DataGridViewTextBoxColumn
        '
        Me.HDDCapacity3DataGridViewTextBoxColumn.DataPropertyName = "HDDCapacity3"
        Me.HDDCapacity3DataGridViewTextBoxColumn.HeaderText = "HDD Capacity 3"
        Me.HDDCapacity3DataGridViewTextBoxColumn.Name = "HDDCapacity3DataGridViewTextBoxColumn"
        '
        'HDDModel3DataGridViewTextBoxColumn
        '
        Me.HDDModel3DataGridViewTextBoxColumn.DataPropertyName = "HDDModel3"
        Me.HDDModel3DataGridViewTextBoxColumn.HeaderText = "HDD Model 3"
        Me.HDDModel3DataGridViewTextBoxColumn.Name = "HDDModel3DataGridViewTextBoxColumn"
        '
        'HDDSN4DataGridViewTextBoxColumn
        '
        Me.HDDSN4DataGridViewTextBoxColumn.DataPropertyName = "HDDSN4"
        Me.HDDSN4DataGridViewTextBoxColumn.HeaderText = "HDD SN 4"
        Me.HDDSN4DataGridViewTextBoxColumn.Name = "HDDSN4DataGridViewTextBoxColumn"
        '
        'HDDCapacity4DataGridViewTextBoxColumn
        '
        Me.HDDCapacity4DataGridViewTextBoxColumn.DataPropertyName = "HDDCapacity4"
        Me.HDDCapacity4DataGridViewTextBoxColumn.HeaderText = "HDD Capacity 4"
        Me.HDDCapacity4DataGridViewTextBoxColumn.Name = "HDDCapacity4DataGridViewTextBoxColumn"
        '
        'HDDModel4DataGridViewTextBoxColumn
        '
        Me.HDDModel4DataGridViewTextBoxColumn.DataPropertyName = "HDDModel4"
        Me.HDDModel4DataGridViewTextBoxColumn.HeaderText = "HDD Model 4"
        Me.HDDModel4DataGridViewTextBoxColumn.Name = "HDDModel4DataGridViewTextBoxColumn"
        '
        'NewPhysicAssetBindingSource
        '
        Me.NewPhysicAssetBindingSource.DataMember = "Master_NewAsset"
        Me.NewPhysicAssetBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'DBASSETAUDITV2BindingSource
        '
        Me.DBASSETAUDITV2BindingSource.DataSource = Me.DB_ASSET_AUDIT_V2
        Me.DBASSETAUDITV2BindingSource.Position = 0
        '
        'DB_ASSET_AUDIT_V2
        '
        Me.DB_ASSET_AUDIT_V2.DataSetName = "DB_ASSET_AUDIT_V2"
        Me.DB_ASSET_AUDIT_V2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblTotalData_AssetBaru
        '
        Me.lblTotalData_AssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalData_AssetBaru.AutoSize = True
        Me.lblTotalData_AssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalData_AssetBaru.Location = New System.Drawing.Point(3, 567)
        Me.lblTotalData_AssetBaru.Name = "lblTotalData_AssetBaru"
        Me.lblTotalData_AssetBaru.Size = New System.Drawing.Size(170, 15)
        Me.lblTotalData_AssetBaru.TabIndex = 115
        Me.lblTotalData_AssetBaru.Text = "Displaying 0 New Asset List(s)"
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.Controls.Add(Me.comboSearchAssetBaru)
        Me.Panel10.Controls.Add(Me.txtSearchAssetBaru)
        Me.Panel10.Controls.Add(Me.btnSeach_AssetBaru)
        Me.Panel10.Controls.Add(Me.btnDownload_AssetBaru)
        Me.Panel10.Location = New System.Drawing.Point(706, 2)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(468, 30)
        Me.Panel10.TabIndex = 114
        '
        'comboSearchAssetBaru
        '
        Me.comboSearchAssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSearchAssetBaru.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboSearchAssetBaru.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSearchAssetBaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboSearchAssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboSearchAssetBaru.FormattingEnabled = True
        Me.comboSearchAssetBaru.ItemHeight = 15
        Me.comboSearchAssetBaru.Items.AddRange(New Object() {"All", "Status", "Uploaded", "Computer Name", "Domain", "Model", "OS Version", "Service Tag SN", "Memory SN 1", "Memory Capacity 1", "Memory Manufacturer 1", "Memory SN 2", "Memory Capacity 2", "Memory Manufacturer 2", "Memory SN 3", "Memory Capacity 3", "Memory Manufacturer 3", "Memory SN 4", "Memory Capacity 4", "Memory Manufacturer 4", "HDD SN 1", "HDD Capacity 1", "HDD Model 1", "HDD SN 2", "HDD Capacity 2", "HDD Model 2", "HDD SN 3", "HDD Capacity 3", "HDD Model 3", "HDD SN 4", "HDD Capacity 4", "HDD Model 4"})
        Me.comboSearchAssetBaru.Location = New System.Drawing.Point(19, 5)
        Me.comboSearchAssetBaru.Name = "comboSearchAssetBaru"
        Me.comboSearchAssetBaru.Size = New System.Drawing.Size(213, 23)
        Me.comboSearchAssetBaru.TabIndex = 111
        '
        'txtSearchAssetBaru
        '
        Me.txtSearchAssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchAssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchAssetBaru.Location = New System.Drawing.Point(235, 6)
        Me.txtSearchAssetBaru.Name = "txtSearchAssetBaru"
        Me.txtSearchAssetBaru.Size = New System.Drawing.Size(177, 22)
        Me.txtSearchAssetBaru.TabIndex = 99
        '
        'btnSeach_AssetBaru
        '
        Me.btnSeach_AssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSeach_AssetBaru.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSeach_AssetBaru.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSeach_AssetBaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSeach_AssetBaru.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeach_AssetBaru.ForeColor = System.Drawing.Color.White
        Me.btnSeach_AssetBaru.Image = CType(resources.GetObject("btnSeach_AssetBaru.Image"), System.Drawing.Image)
        Me.btnSeach_AssetBaru.Location = New System.Drawing.Point(412, 5)
        Me.btnSeach_AssetBaru.Name = "btnSeach_AssetBaru"
        Me.btnSeach_AssetBaru.Size = New System.Drawing.Size(26, 23)
        Me.btnSeach_AssetBaru.TabIndex = 99
        Me.btnSeach_AssetBaru.UseVisualStyleBackColor = False
        '
        'btnDownload_AssetBaru
        '
        Me.btnDownload_AssetBaru.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownload_AssetBaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownload_AssetBaru.ForeColor = System.Drawing.Color.White
        Me.btnDownload_AssetBaru.Image = CType(resources.GetObject("btnDownload_AssetBaru.Image"), System.Drawing.Image)
        Me.btnDownload_AssetBaru.Location = New System.Drawing.Point(439, 5)
        Me.btnDownload_AssetBaru.Name = "btnDownload_AssetBaru"
        Me.btnDownload_AssetBaru.Size = New System.Drawing.Size(26, 23)
        Me.btnDownload_AssetBaru.TabIndex = 101
        Me.btnDownload_AssetBaru.UseVisualStyleBackColor = False
        '
        'lblPercentage_AssetBaru
        '
        Me.lblPercentage_AssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercentage_AssetBaru.AutoSize = True
        Me.lblPercentage_AssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage_AssetBaru.Location = New System.Drawing.Point(429, 15)
        Me.lblPercentage_AssetBaru.Name = "lblPercentage_AssetBaru"
        Me.lblPercentage_AssetBaru.Size = New System.Drawing.Size(21, 13)
        Me.lblPercentage_AssetBaru.TabIndex = 102
        Me.lblPercentage_AssetBaru.Text = "0%"
        '
        'lblStatus_AssetBaru
        '
        Me.lblStatus_AssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus_AssetBaru.AutoSize = True
        Me.lblStatus_AssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus_AssetBaru.Location = New System.Drawing.Point(259, 14)
        Me.lblStatus_AssetBaru.Name = "lblStatus_AssetBaru"
        Me.lblStatus_AssetBaru.Size = New System.Drawing.Size(164, 13)
        Me.lblStatus_AssetBaru.TabIndex = 101
        Me.lblStatus_AssetBaru.Text = "Starting. . . Please wait a moment"
        '
        'progressBar_AssetBaru
        '
        Me.progressBar_AssetBaru.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progressBar_AssetBaru.Location = New System.Drawing.Point(502, 12)
        Me.progressBar_AssetBaru.Name = "progressBar_AssetBaru"
        Me.progressBar_AssetBaru.Size = New System.Drawing.Size(74, 19)
        Me.progressBar_AssetBaru.TabIndex = 100
        '
        'lbl_formatNewAsset
        '
        Me.lbl_formatNewAsset.AutoSize = True
        Me.lbl_formatNewAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_formatNewAsset.Location = New System.Drawing.Point(138, 15)
        Me.lbl_formatNewAsset.Name = "lbl_formatNewAsset"
        Me.lbl_formatNewAsset.Size = New System.Drawing.Size(82, 13)
        Me.lbl_formatNewAsset.TabIndex = 99
        Me.lbl_formatNewAsset.Text = "(format file .*txt) "
        '
        'btnUpload_AssetBaru
        '
        Me.btnUpload_AssetBaru.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnUpload_AssetBaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpload_AssetBaru.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload_AssetBaru.ForeColor = System.Drawing.Color.White
        Me.btnUpload_AssetBaru.Image = CType(resources.GetObject("btnUpload_AssetBaru.Image"), System.Drawing.Image)
        Me.btnUpload_AssetBaru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpload_AssetBaru.Location = New System.Drawing.Point(63, 4)
        Me.btnUpload_AssetBaru.Name = "btnUpload_AssetBaru"
        Me.btnUpload_AssetBaru.Size = New System.Drawing.Size(75, 26)
        Me.btnUpload_AssetBaru.TabIndex = 99
        Me.btnUpload_AssetBaru.Text = "Upload"
        Me.btnUpload_AssetBaru.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpload_AssetBaru.UseVisualStyleBackColor = False
        '
        'btnSaveNewAsset
        '
        Me.btnSaveNewAsset.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSaveNewAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveNewAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveNewAsset.ForeColor = System.Drawing.Color.White
        Me.btnSaveNewAsset.Image = CType(resources.GetObject("btnSaveNewAsset.Image"), System.Drawing.Image)
        Me.btnSaveNewAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSaveNewAsset.Location = New System.Drawing.Point(0, 4)
        Me.btnSaveNewAsset.Name = "btnSaveNewAsset"
        Me.btnSaveNewAsset.Size = New System.Drawing.Size(62, 26)
        Me.btnSaveNewAsset.TabIndex = 99
        Me.btnSaveNewAsset.Text = "Save"
        Me.btnSaveNewAsset.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSaveNewAsset.UseVisualStyleBackColor = False
        '
        'lblDateTimeNewRegis
        '
        Me.lblDateTimeNewRegis.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDateTimeNewRegis.AutoSize = True
        Me.lblDateTimeNewRegis.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTimeNewRegis.Image = CType(resources.GetObject("lblDateTimeNewRegis.Image"), System.Drawing.Image)
        Me.lblDateTimeNewRegis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDateTimeNewRegis.Location = New System.Drawing.Point(987, 734)
        Me.lblDateTimeNewRegis.Name = "lblDateTimeNewRegis"
        Me.lblDateTimeNewRegis.Size = New System.Drawing.Size(216, 17)
        Me.lblDateTimeNewRegis.TabIndex = 99
        Me.lblDateTimeNewRegis.Text = "       31 Oktober 2019  |  version 1.0"
        '
        'TabAssetFisik
        '
        Me.TabAssetFisik.BackColor = System.Drawing.Color.White
        Me.TabAssetFisik.Controls.Add(Me.Panel5)
        Me.TabAssetFisik.Controls.Add(Me.btnDownloadPhysicAsset)
        Me.TabAssetFisik.Controls.Add(Me.panelGetPhysicForm)
        Me.TabAssetFisik.Controls.Add(Me.TabControl1)
        Me.TabAssetFisik.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabAssetFisik.Location = New System.Drawing.Point(4, 36)
        Me.TabAssetFisik.Name = "TabAssetFisik"
        Me.TabAssetFisik.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAssetFisik.Size = New System.Drawing.Size(1173, 583)
        Me.TabAssetFisik.TabIndex = 99
        Me.TabAssetFisik.Text = "Asset Fisik"
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.Controls.Add(Me.lblTotal_AccessDeniedData)
        Me.Panel5.Controls.Add(Me.lblTotal_NotConnectedData)
        Me.Panel5.Controls.Add(Me.lblTotal_ConnectedData)
        Me.Panel5.Controls.Add(Me.lblTotal_All)
        Me.Panel5.Controls.Add(Me.lbl_ketTotalConOldAsset)
        Me.Panel5.Controls.Add(Me.lbl_ketTotalDeniedOldAsset)
        Me.Panel5.Controls.Add(Me.lbl_ketTotalNotConOldAsset)
        Me.Panel5.Controls.Add(Me.lbl_ketTotalAllOldAsset)
        Me.Panel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel5.Location = New System.Drawing.Point(559, 46)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(508, 26)
        Me.Panel5.TabIndex = 104
        '
        'lblTotal_AccessDeniedData
        '
        Me.lblTotal_AccessDeniedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal_AccessDeniedData.AutoSize = True
        Me.lblTotal_AccessDeniedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal_AccessDeniedData.Location = New System.Drawing.Point(476, 6)
        Me.lblTotal_AccessDeniedData.Name = "lblTotal_AccessDeniedData"
        Me.lblTotal_AccessDeniedData.Size = New System.Drawing.Size(28, 15)
        Me.lblTotal_AccessDeniedData.TabIndex = 99
        Me.lblTotal_AccessDeniedData.Text = "xxx "
        '
        'lblTotal_NotConnectedData
        '
        Me.lblTotal_NotConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal_NotConnectedData.AutoSize = True
        Me.lblTotal_NotConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal_NotConnectedData.Location = New System.Drawing.Point(360, 6)
        Me.lblTotal_NotConnectedData.Name = "lblTotal_NotConnectedData"
        Me.lblTotal_NotConnectedData.Size = New System.Drawing.Size(28, 15)
        Me.lblTotal_NotConnectedData.TabIndex = 99
        Me.lblTotal_NotConnectedData.Text = "xxx "
        '
        'lblTotal_ConnectedData
        '
        Me.lblTotal_ConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal_ConnectedData.AutoSize = True
        Me.lblTotal_ConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal_ConnectedData.Location = New System.Drawing.Point(201, 6)
        Me.lblTotal_ConnectedData.Name = "lblTotal_ConnectedData"
        Me.lblTotal_ConnectedData.Size = New System.Drawing.Size(28, 15)
        Me.lblTotal_ConnectedData.TabIndex = 99
        Me.lblTotal_ConnectedData.Text = "xxx "
        '
        'lblTotal_All
        '
        Me.lblTotal_All.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal_All.AutoSize = True
        Me.lblTotal_All.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal_All.Location = New System.Drawing.Point(60, 6)
        Me.lblTotal_All.Name = "lblTotal_All"
        Me.lblTotal_All.Size = New System.Drawing.Size(28, 15)
        Me.lblTotal_All.TabIndex = 99
        Me.lblTotal_All.Text = "xxx "
        '
        'lbl_ketTotalConOldAsset
        '
        Me.lbl_ketTotalConOldAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketTotalConOldAsset.AutoSize = True
        Me.lbl_ketTotalConOldAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketTotalConOldAsset.Image = CType(resources.GetObject("lbl_ketTotalConOldAsset.Image"), System.Drawing.Image)
        Me.lbl_ketTotalConOldAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl_ketTotalConOldAsset.Location = New System.Drawing.Point(116, 6)
        Me.lbl_ketTotalConOldAsset.Name = "lbl_ketTotalConOldAsset"
        Me.lbl_ketTotalConOldAsset.Size = New System.Drawing.Size(87, 15)
        Me.lbl_ketTotalConOldAsset.TabIndex = 99
        Me.lbl_ketTotalConOldAsset.Text = "     Connected: "
        '
        'lbl_ketTotalDeniedOldAsset
        '
        Me.lbl_ketTotalDeniedOldAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketTotalDeniedOldAsset.AutoSize = True
        Me.lbl_ketTotalDeniedOldAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketTotalDeniedOldAsset.Image = CType(resources.GetObject("lbl_ketTotalDeniedOldAsset.Image"), System.Drawing.Image)
        Me.lbl_ketTotalDeniedOldAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl_ketTotalDeniedOldAsset.Location = New System.Drawing.Point(410, 6)
        Me.lbl_ketTotalDeniedOldAsset.Name = "lbl_ketTotalDeniedOldAsset"
        Me.lbl_ketTotalDeniedOldAsset.Size = New System.Drawing.Size(68, 15)
        Me.lbl_ketTotalDeniedOldAsset.TabIndex = 99
        Me.lbl_ketTotalDeniedOldAsset.Text = "     Denied: "
        '
        'lbl_ketTotalNotConOldAsset
        '
        Me.lbl_ketTotalNotConOldAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketTotalNotConOldAsset.AutoSize = True
        Me.lbl_ketTotalNotConOldAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketTotalNotConOldAsset.Image = CType(resources.GetObject("lbl_ketTotalNotConOldAsset.Image"), System.Drawing.Image)
        Me.lbl_ketTotalNotConOldAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl_ketTotalNotConOldAsset.Location = New System.Drawing.Point(253, 6)
        Me.lbl_ketTotalNotConOldAsset.Name = "lbl_ketTotalNotConOldAsset"
        Me.lbl_ketTotalNotConOldAsset.Size = New System.Drawing.Size(109, 15)
        Me.lbl_ketTotalNotConOldAsset.TabIndex = 99
        Me.lbl_ketTotalNotConOldAsset.Text = "     Not Connected: "
        '
        'lbl_ketTotalAllOldAsset
        '
        Me.lbl_ketTotalAllOldAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketTotalAllOldAsset.AutoSize = True
        Me.lbl_ketTotalAllOldAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketTotalAllOldAsset.Location = New System.Drawing.Point(23, 6)
        Me.lbl_ketTotalAllOldAsset.Name = "lbl_ketTotalAllOldAsset"
        Me.lbl_ketTotalAllOldAsset.Size = New System.Drawing.Size(37, 15)
        Me.lbl_ketTotalAllOldAsset.TabIndex = 99
        Me.lbl_ketTotalAllOldAsset.Text = "Total:"
        '
        'btnDownloadPhysicAsset
        '
        Me.btnDownloadPhysicAsset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDownloadPhysicAsset.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownloadPhysicAsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownloadPhysicAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownloadPhysicAsset.ForeColor = System.Drawing.Color.White
        Me.btnDownloadPhysicAsset.Image = CType(resources.GetObject("btnDownloadPhysicAsset.Image"), System.Drawing.Image)
        Me.btnDownloadPhysicAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDownloadPhysicAsset.Location = New System.Drawing.Point(1065, 46)
        Me.btnDownloadPhysicAsset.Name = "btnDownloadPhysicAsset"
        Me.btnDownloadPhysicAsset.Size = New System.Drawing.Size(107, 26)
        Me.btnDownloadPhysicAsset.TabIndex = 20
        Me.btnDownloadPhysicAsset.Text = "     Download All"
        Me.btnDownloadPhysicAsset.UseVisualStyleBackColor = False
        '
        'panelGetPhysicForm
        '
        Me.panelGetPhysicForm.BackColor = System.Drawing.Color.White
        Me.panelGetPhysicForm.Controls.Add(Me.btnGetComputer)
        Me.panelGetPhysicForm.Controls.Add(Me.lblSubAreaRequired)
        Me.panelGetPhysicForm.Controls.Add(Me.lblSubAreaTitle)
        Me.panelGetPhysicForm.Controls.Add(Me.ComboBoxSubAreaField)
        Me.panelGetPhysicForm.Controls.Add(Me.lblRequiredArea)
        Me.panelGetPhysicForm.Controls.Add(Me.lblRunDataFromRequired)
        Me.panelGetPhysicForm.Controls.Add(Me.ProgressBarProcessPhysicAsset)
        Me.panelGetPhysicForm.Controls.Add(Me.lblShowPleaseWaitGetPhysicAsset)
        Me.panelGetPhysicForm.Controls.Add(Me.ComboBoxAreaField)
        Me.panelGetPhysicForm.Controls.Add(Me.lblAreaTitle)
        Me.panelGetPhysicForm.Controls.Add(Me.ComboBoxCategoryRunDataField)
        Me.panelGetPhysicForm.Controls.Add(Me.lblRunDataFromTitle)
        Me.panelGetPhysicForm.Location = New System.Drawing.Point(-7, 0)
        Me.panelGetPhysicForm.Margin = New System.Windows.Forms.Padding(0)
        Me.panelGetPhysicForm.Name = "panelGetPhysicForm"
        Me.panelGetPhysicForm.Size = New System.Drawing.Size(592, 76)
        Me.panelGetPhysicForm.TabIndex = 101
        '
        'btnGetComputer
        '
        Me.btnGetComputer.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnGetComputer.FlatAppearance.BorderSize = 0
        Me.btnGetComputer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGetComputer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetComputer.ForeColor = System.Drawing.Color.White
        Me.btnGetComputer.Image = CType(resources.GetObject("btnGetComputer.Image"), System.Drawing.Image)
        Me.btnGetComputer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGetComputer.Location = New System.Drawing.Point(9, 48)
        Me.btnGetComputer.Name = "btnGetComputer"
        Me.btnGetComputer.Size = New System.Drawing.Size(113, 24)
        Me.btnGetComputer.TabIndex = 108
        Me.btnGetComputer.Text = "Get Computer"
        Me.btnGetComputer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGetComputer.UseVisualStyleBackColor = False
        '
        'lblSubAreaRequired
        '
        Me.lblSubAreaRequired.AutoSize = True
        Me.lblSubAreaRequired.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubAreaRequired.ForeColor = System.Drawing.Color.Red
        Me.lblSubAreaRequired.Location = New System.Drawing.Point(358, 5)
        Me.lblSubAreaRequired.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSubAreaRequired.Name = "lblSubAreaRequired"
        Me.lblSubAreaRequired.Size = New System.Drawing.Size(11, 13)
        Me.lblSubAreaRequired.TabIndex = 112
        Me.lblSubAreaRequired.Text = "*"
        '
        'lblSubAreaTitle
        '
        Me.lblSubAreaTitle.AutoSize = True
        Me.lblSubAreaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubAreaTitle.Location = New System.Drawing.Point(312, 5)
        Me.lblSubAreaTitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSubAreaTitle.Name = "lblSubAreaTitle"
        Me.lblSubAreaTitle.Size = New System.Drawing.Size(51, 13)
        Me.lblSubAreaTitle.TabIndex = 105
        Me.lblSubAreaTitle.Text = "Sub Area"
        '
        'ComboBoxSubAreaField
        '
        Me.ComboBoxSubAreaField.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ComboBoxSubAreaField.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxSubAreaField.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxSubAreaField.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxSubAreaField.FormattingEnabled = True
        Me.ComboBoxSubAreaField.Location = New System.Drawing.Point(314, 22)
        Me.ComboBoxSubAreaField.Margin = New System.Windows.Forms.Padding(2)
        Me.ComboBoxSubAreaField.Name = "ComboBoxSubAreaField"
        Me.ComboBoxSubAreaField.Size = New System.Drawing.Size(255, 21)
        Me.ComboBoxSubAreaField.TabIndex = 107
        '
        'lblRequiredArea
        '
        Me.lblRequiredArea.AutoSize = True
        Me.lblRequiredArea.BackColor = System.Drawing.Color.Transparent
        Me.lblRequiredArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequiredArea.ForeColor = System.Drawing.Color.Red
        Me.lblRequiredArea.Location = New System.Drawing.Point(175, 5)
        Me.lblRequiredArea.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblRequiredArea.Name = "lblRequiredArea"
        Me.lblRequiredArea.Size = New System.Drawing.Size(11, 13)
        Me.lblRequiredArea.TabIndex = 111
        Me.lblRequiredArea.Text = "*"
        '
        'lblRunDataFromRequired
        '
        Me.lblRunDataFromRequired.AutoSize = True
        Me.lblRunDataFromRequired.BackColor = System.Drawing.Color.Transparent
        Me.lblRunDataFromRequired.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunDataFromRequired.ForeColor = System.Drawing.Color.Red
        Me.lblRunDataFromRequired.Location = New System.Drawing.Point(52, 5)
        Me.lblRunDataFromRequired.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblRunDataFromRequired.Name = "lblRunDataFromRequired"
        Me.lblRunDataFromRequired.Size = New System.Drawing.Size(11, 13)
        Me.lblRunDataFromRequired.TabIndex = 110
        Me.lblRunDataFromRequired.Text = "*"
        '
        'ProgressBarProcessPhysicAsset
        '
        Me.ProgressBarProcessPhysicAsset.Location = New System.Drawing.Point(160, 50)
        Me.ProgressBarProcessPhysicAsset.Margin = New System.Windows.Forms.Padding(2)
        Me.ProgressBarProcessPhysicAsset.Name = "ProgressBarProcessPhysicAsset"
        Me.ProgressBarProcessPhysicAsset.Size = New System.Drawing.Size(55, 20)
        Me.ProgressBarProcessPhysicAsset.TabIndex = 109
        '
        'lblShowPleaseWaitGetPhysicAsset
        '
        Me.lblShowPleaseWaitGetPhysicAsset.AutoSize = True
        Me.lblShowPleaseWaitGetPhysicAsset.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShowPleaseWaitGetPhysicAsset.Location = New System.Drawing.Point(234, 57)
        Me.lblShowPleaseWaitGetPhysicAsset.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblShowPleaseWaitGetPhysicAsset.Name = "lblShowPleaseWaitGetPhysicAsset"
        Me.lblShowPleaseWaitGetPhysicAsset.Size = New System.Drawing.Size(196, 13)
        Me.lblShowPleaseWaitGetPhysicAsset.TabIndex = 108
        Me.lblShowPleaseWaitGetPhysicAsset.Text = "Please wait... system is getting xxx asset"
        '
        'ComboBoxAreaField
        '
        Me.ComboBoxAreaField.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ComboBoxAreaField.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxAreaField.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxAreaField.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxAreaField.FormattingEnabled = True
        Me.ComboBoxAreaField.Location = New System.Drawing.Point(153, 22)
        Me.ComboBoxAreaField.Margin = New System.Windows.Forms.Padding(2)
        Me.ComboBoxAreaField.Name = "ComboBoxAreaField"
        Me.ComboBoxAreaField.Size = New System.Drawing.Size(160, 21)
        Me.ComboBoxAreaField.TabIndex = 106
        '
        'lblAreaTitle
        '
        Me.lblAreaTitle.AutoSize = True
        Me.lblAreaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAreaTitle.Location = New System.Drawing.Point(151, 5)
        Me.lblAreaTitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAreaTitle.Name = "lblAreaTitle"
        Me.lblAreaTitle.Size = New System.Drawing.Size(32, 13)
        Me.lblAreaTitle.TabIndex = 104
        Me.lblAreaTitle.Text = "Area "
        '
        'ComboBoxCategoryRunDataField
        '
        Me.ComboBoxCategoryRunDataField.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ComboBoxCategoryRunDataField.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxCategoryRunDataField.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCategoryRunDataField.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxCategoryRunDataField.FormattingEnabled = True
        Me.ComboBoxCategoryRunDataField.Items.AddRange(New Object() {"All", "PC Not Connected", "PC Denied"})
        Me.ComboBoxCategoryRunDataField.Location = New System.Drawing.Point(8, 22)
        Me.ComboBoxCategoryRunDataField.Margin = New System.Windows.Forms.Padding(2)
        Me.ComboBoxCategoryRunDataField.Name = "ComboBoxCategoryRunDataField"
        Me.ComboBoxCategoryRunDataField.Size = New System.Drawing.Size(144, 21)
        Me.ComboBoxCategoryRunDataField.TabIndex = 101
        '
        'lblRunDataFromTitle
        '
        Me.lblRunDataFromTitle.AutoSize = True
        Me.lblRunDataFromTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunDataFromTitle.Location = New System.Drawing.Point(7, 5)
        Me.lblRunDataFromTitle.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblRunDataFromTitle.Name = "lblRunDataFromTitle"
        Me.lblRunDataFromTitle.Size = New System.Drawing.Size(49, 13)
        Me.lblRunDataFromTitle.TabIndex = 100
        Me.lblRunDataFromTitle.Text = "Category"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(-3, 77)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1183, 590)
        Me.TabControl1.TabIndex = 105
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.btnSave_ConnectedData)
        Me.TabPage2.Controls.Add(Me.lblTotalData_Connected)
        Me.TabPage2.Controls.Add(Me.Panel8)
        Me.TabPage2.Controls.Add(Me.dgvAssetFisik_ConnectedData)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1175, 562)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Data Connected"
        '
        'btnSave_ConnectedData
        '
        Me.btnSave_ConnectedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSave_ConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave_ConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave_ConnectedData.ForeColor = System.Drawing.Color.White
        Me.btnSave_ConnectedData.Image = CType(resources.GetObject("btnSave_ConnectedData.Image"), System.Drawing.Image)
        Me.btnSave_ConnectedData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave_ConnectedData.Location = New System.Drawing.Point(0, 2)
        Me.btnSave_ConnectedData.Name = "btnSave_ConnectedData"
        Me.btnSave_ConnectedData.Size = New System.Drawing.Size(64, 26)
        Me.btnSave_ConnectedData.TabIndex = 100
        Me.btnSave_ConnectedData.Text = "      Save"
        Me.btnSave_ConnectedData.UseVisualStyleBackColor = False
        '
        'lblTotalData_Connected
        '
        Me.lblTotalData_Connected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalData_Connected.AutoSize = True
        Me.lblTotalData_Connected.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalData_Connected.Location = New System.Drawing.Point(6, 673)
        Me.lblTotalData_Connected.Name = "lblTotalData_Connected"
        Me.lblTotalData_Connected.Size = New System.Drawing.Size(172, 15)
        Me.lblTotalData_Connected.TabIndex = 114
        Me.lblTotalData_Connected.Text = "Displaying 0 Physic Asset Lists"
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.Controls.Add(Me.comboTransaction_ConnectedData)
        Me.Panel8.Controls.Add(Me.txtSearchTransaction_ConnectedData)
        Me.Panel8.Controls.Add(Me.btnSearchTransaction_ConnectedData)
        Me.Panel8.Controls.Add(Me.btnDownload_ConnectedData)
        Me.Panel8.Location = New System.Drawing.Point(706, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(468, 30)
        Me.Panel8.TabIndex = 113
        '
        'comboTransaction_ConnectedData
        '
        Me.comboTransaction_ConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboTransaction_ConnectedData.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboTransaction_ConnectedData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboTransaction_ConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboTransaction_ConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboTransaction_ConnectedData.FormattingEnabled = True
        Me.comboTransaction_ConnectedData.ItemHeight = 15
        Me.comboTransaction_ConnectedData.Items.AddRange(New Object() {"All", "Status", "Uploaded", "Computer Name", "Domain", "Model", "OS Version", "Service Tag SN", "Memory SN 1", "Memory Capacity 1", "Memory Manufacturer 1", "Memory SN 2", "Memory Capacity 2", "Memory Manufacturer 2", "Memory SN 3", "Memory Capacity 3", "Memory Manufacturer 3", "Memory SN 4", "Memory Capacity 4", "Memory Manufacturer 4", "HDD SN 1", "HDD Capacity 1", "HDD Model 1", "HDD SN 2", "HDD Capacity 2", "HDD Model 2", "HDD SN 3", "HDD Capacity 3", "HDD Model 3", "HDD SN 4", "HDD Capacity 4", "HDD Model 4", "Monitor SN 1", "Monitor Model 1", "Monitor SN 2", "Monitor Model 2", "UPS SN", "UPS Time Left", "Keterangan"})
        Me.comboTransaction_ConnectedData.Location = New System.Drawing.Point(19, 5)
        Me.comboTransaction_ConnectedData.Name = "comboTransaction_ConnectedData"
        Me.comboTransaction_ConnectedData.Size = New System.Drawing.Size(213, 23)
        Me.comboTransaction_ConnectedData.TabIndex = 111
        '
        'txtSearchTransaction_ConnectedData
        '
        Me.txtSearchTransaction_ConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchTransaction_ConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchTransaction_ConnectedData.Location = New System.Drawing.Point(235, 6)
        Me.txtSearchTransaction_ConnectedData.Name = "txtSearchTransaction_ConnectedData"
        Me.txtSearchTransaction_ConnectedData.Size = New System.Drawing.Size(177, 22)
        Me.txtSearchTransaction_ConnectedData.TabIndex = 99
        '
        'btnSearchTransaction_ConnectedData
        '
        Me.btnSearchTransaction_ConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchTransaction_ConnectedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchTransaction_ConnectedData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchTransaction_ConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchTransaction_ConnectedData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchTransaction_ConnectedData.ForeColor = System.Drawing.Color.White
        Me.btnSearchTransaction_ConnectedData.Image = CType(resources.GetObject("btnSearchTransaction_ConnectedData.Image"), System.Drawing.Image)
        Me.btnSearchTransaction_ConnectedData.Location = New System.Drawing.Point(412, 5)
        Me.btnSearchTransaction_ConnectedData.Name = "btnSearchTransaction_ConnectedData"
        Me.btnSearchTransaction_ConnectedData.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchTransaction_ConnectedData.TabIndex = 99
        Me.btnSearchTransaction_ConnectedData.UseVisualStyleBackColor = False
        '
        'btnDownload_ConnectedData
        '
        Me.btnDownload_ConnectedData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDownload_ConnectedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownload_ConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownload_ConnectedData.ForeColor = System.Drawing.Color.White
        Me.btnDownload_ConnectedData.Image = CType(resources.GetObject("btnDownload_ConnectedData.Image"), System.Drawing.Image)
        Me.btnDownload_ConnectedData.Location = New System.Drawing.Point(439, 5)
        Me.btnDownload_ConnectedData.Name = "btnDownload_ConnectedData"
        Me.btnDownload_ConnectedData.Size = New System.Drawing.Size(26, 23)
        Me.btnDownload_ConnectedData.TabIndex = 101
        Me.btnDownload_ConnectedData.UseVisualStyleBackColor = False
        '
        'dgvAssetFisik_ConnectedData
        '
        Me.dgvAssetFisik_ConnectedData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAssetFisik_ConnectedData.AutoGenerateColumns = False
        Me.dgvAssetFisik_ConnectedData.BackgroundColor = System.Drawing.Color.White
        Me.dgvAssetFisik_ConnectedData.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAssetFisik_ConnectedData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dgvAssetFisik_ConnectedData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssetFisik_ConnectedData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewCheckBoxColumn2, Me.UploadedDataGridViewTextBoxColumn1, Me.ComputerNameDataGridViewTextBoxColumn2, Me.DomainDataGridViewTextBoxColumn1, Me.ModelDataGridViewTextBoxColumn1, Me.OSVersionDataGridViewTextBoxColumn1, Me.ServiceTagSNDataGridViewTextBoxColumn2, Me.MemSN1DataGridViewTextBoxColumn1, Me.MemCapacity1DataGridViewTextBoxColumn1, Me.MemManufacturer1DataGridViewTextBoxColumn1, Me.MemSN2DataGridViewTextBoxColumn1, Me.MemCapacity2DataGridViewTextBoxColumn1, Me.MemManufacturer2DataGridViewTextBoxColumn1, Me.MemSN3DataGridViewTextBoxColumn1, Me.MemCapacity3DataGridViewTextBoxColumn1, Me.MemManufacturer3DataGridViewTextBoxColumn1, Me.MemSN4DataGridViewTextBoxColumn1, Me.MemCapacity4DataGridViewTextBoxColumn1, Me.MemManufacturer4DataGridViewTextBoxColumn1, Me.HDDSN1DataGridViewTextBoxColumn2, Me.HDDCapacity1DataGridViewTextBoxColumn1, Me.HDDModel1DataGridViewTextBoxColumn1, Me.HDDSN2DataGridViewTextBoxColumn2, Me.HDDCapacity2DataGridViewTextBoxColumn1, Me.HDDModel2DataGridViewTextBoxColumn1, Me.HDDSN3DataGridViewTextBoxColumn2, Me.HDDCapacity3DataGridViewTextBoxColumn1, Me.HDDModel3DataGridViewTextBoxColumn1, Me.HDDSN4DataGridViewTextBoxColumn2, Me.HDDCapacity4DataGridViewTextBoxColumn1, Me.HDDModel4DataGridViewTextBoxColumn1, Me.MonSN1DataGridViewTextBoxColumn1, Me.MonModel1DataGridViewTextBoxColumn, Me.MonSN2DataGridViewTextBoxColumn1, Me.MonModel2DataGridViewTextBoxColumn, Me.UPSSNDataGridViewTextBoxColumn1, Me.UPSTimeLeftDataGridViewTextBoxColumn, Me.KeteranganDataGridViewTextBoxColumn})
        Me.dgvAssetFisik_ConnectedData.DataSource = Me.AssetFisikBindingSource
        Me.dgvAssetFisik_ConnectedData.Location = New System.Drawing.Point(0, 31)
        Me.dgvAssetFisik_ConnectedData.Name = "dgvAssetFisik_ConnectedData"
        Me.dgvAssetFisik_ConnectedData.RowHeadersWidth = 62
        Me.dgvAssetFisik_ConnectedData.Size = New System.Drawing.Size(1171, 452)
        Me.dgvAssetFisik_ConnectedData.TabIndex = 0
        '
        'StatusDataGridViewCheckBoxColumn2
        '
        Me.StatusDataGridViewCheckBoxColumn2.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn2.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn2.MinimumWidth = 8
        Me.StatusDataGridViewCheckBoxColumn2.Name = "StatusDataGridViewCheckBoxColumn2"
        Me.StatusDataGridViewCheckBoxColumn2.Width = 150
        '
        'UploadedDataGridViewTextBoxColumn1
        '
        Me.UploadedDataGridViewTextBoxColumn1.DataPropertyName = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn1.HeaderText = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.UploadedDataGridViewTextBoxColumn1.Name = "UploadedDataGridViewTextBoxColumn1"
        Me.UploadedDataGridViewTextBoxColumn1.Width = 150
        '
        'ComputerNameDataGridViewTextBoxColumn2
        '
        Me.ComputerNameDataGridViewTextBoxColumn2.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn2.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.ComputerNameDataGridViewTextBoxColumn2.Name = "ComputerNameDataGridViewTextBoxColumn2"
        Me.ComputerNameDataGridViewTextBoxColumn2.Width = 150
        '
        'DomainDataGridViewTextBoxColumn1
        '
        Me.DomainDataGridViewTextBoxColumn1.DataPropertyName = "Domain"
        Me.DomainDataGridViewTextBoxColumn1.HeaderText = "Domain"
        Me.DomainDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.DomainDataGridViewTextBoxColumn1.Name = "DomainDataGridViewTextBoxColumn1"
        Me.DomainDataGridViewTextBoxColumn1.Width = 150
        '
        'ModelDataGridViewTextBoxColumn1
        '
        Me.ModelDataGridViewTextBoxColumn1.DataPropertyName = "Model"
        Me.ModelDataGridViewTextBoxColumn1.HeaderText = "Model"
        Me.ModelDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.ModelDataGridViewTextBoxColumn1.Name = "ModelDataGridViewTextBoxColumn1"
        Me.ModelDataGridViewTextBoxColumn1.Width = 150
        '
        'OSVersionDataGridViewTextBoxColumn1
        '
        Me.OSVersionDataGridViewTextBoxColumn1.DataPropertyName = "OSVersion"
        Me.OSVersionDataGridViewTextBoxColumn1.HeaderText = "OS Version"
        Me.OSVersionDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.OSVersionDataGridViewTextBoxColumn1.Name = "OSVersionDataGridViewTextBoxColumn1"
        Me.OSVersionDataGridViewTextBoxColumn1.Width = 150
        '
        'ServiceTagSNDataGridViewTextBoxColumn2
        '
        Me.ServiceTagSNDataGridViewTextBoxColumn2.DataPropertyName = "ServiceTagSN"
        Me.ServiceTagSNDataGridViewTextBoxColumn2.HeaderText = "Service Tag SN"
        Me.ServiceTagSNDataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.ServiceTagSNDataGridViewTextBoxColumn2.Name = "ServiceTagSNDataGridViewTextBoxColumn2"
        Me.ServiceTagSNDataGridViewTextBoxColumn2.Width = 150
        '
        'MemSN1DataGridViewTextBoxColumn1
        '
        Me.MemSN1DataGridViewTextBoxColumn1.DataPropertyName = "MemSN1"
        Me.MemSN1DataGridViewTextBoxColumn1.HeaderText = "Memory SN 1"
        Me.MemSN1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemSN1DataGridViewTextBoxColumn1.Name = "MemSN1DataGridViewTextBoxColumn1"
        Me.MemSN1DataGridViewTextBoxColumn1.Width = 150
        '
        'MemCapacity1DataGridViewTextBoxColumn1
        '
        Me.MemCapacity1DataGridViewTextBoxColumn1.DataPropertyName = "MemCapacity1"
        Me.MemCapacity1DataGridViewTextBoxColumn1.HeaderText = "Memory Capacity 1"
        Me.MemCapacity1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemCapacity1DataGridViewTextBoxColumn1.Name = "MemCapacity1DataGridViewTextBoxColumn1"
        Me.MemCapacity1DataGridViewTextBoxColumn1.Width = 150
        '
        'MemManufacturer1DataGridViewTextBoxColumn1
        '
        Me.MemManufacturer1DataGridViewTextBoxColumn1.DataPropertyName = "MemManufacturer1"
        Me.MemManufacturer1DataGridViewTextBoxColumn1.HeaderText = "Memory Manufacturer 1"
        Me.MemManufacturer1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemManufacturer1DataGridViewTextBoxColumn1.Name = "MemManufacturer1DataGridViewTextBoxColumn1"
        Me.MemManufacturer1DataGridViewTextBoxColumn1.Width = 150
        '
        'MemSN2DataGridViewTextBoxColumn1
        '
        Me.MemSN2DataGridViewTextBoxColumn1.DataPropertyName = "MemSN2"
        Me.MemSN2DataGridViewTextBoxColumn1.HeaderText = "Memory SN 2"
        Me.MemSN2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemSN2DataGridViewTextBoxColumn1.Name = "MemSN2DataGridViewTextBoxColumn1"
        Me.MemSN2DataGridViewTextBoxColumn1.Width = 150
        '
        'MemCapacity2DataGridViewTextBoxColumn1
        '
        Me.MemCapacity2DataGridViewTextBoxColumn1.DataPropertyName = "MemCapacity2"
        Me.MemCapacity2DataGridViewTextBoxColumn1.HeaderText = "Memory Capacity 2"
        Me.MemCapacity2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemCapacity2DataGridViewTextBoxColumn1.Name = "MemCapacity2DataGridViewTextBoxColumn1"
        Me.MemCapacity2DataGridViewTextBoxColumn1.Width = 150
        '
        'MemManufacturer2DataGridViewTextBoxColumn1
        '
        Me.MemManufacturer2DataGridViewTextBoxColumn1.DataPropertyName = "MemManufacturer2"
        Me.MemManufacturer2DataGridViewTextBoxColumn1.HeaderText = "Memory Manufacturer 2"
        Me.MemManufacturer2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemManufacturer2DataGridViewTextBoxColumn1.Name = "MemManufacturer2DataGridViewTextBoxColumn1"
        Me.MemManufacturer2DataGridViewTextBoxColumn1.Width = 150
        '
        'MemSN3DataGridViewTextBoxColumn1
        '
        Me.MemSN3DataGridViewTextBoxColumn1.DataPropertyName = "MemSN3"
        Me.MemSN3DataGridViewTextBoxColumn1.HeaderText = "Memory SN 3"
        Me.MemSN3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemSN3DataGridViewTextBoxColumn1.Name = "MemSN3DataGridViewTextBoxColumn1"
        Me.MemSN3DataGridViewTextBoxColumn1.Width = 150
        '
        'MemCapacity3DataGridViewTextBoxColumn1
        '
        Me.MemCapacity3DataGridViewTextBoxColumn1.DataPropertyName = "MemCapacity3"
        Me.MemCapacity3DataGridViewTextBoxColumn1.HeaderText = "Memory Capacity 3"
        Me.MemCapacity3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemCapacity3DataGridViewTextBoxColumn1.Name = "MemCapacity3DataGridViewTextBoxColumn1"
        Me.MemCapacity3DataGridViewTextBoxColumn1.Width = 150
        '
        'MemManufacturer3DataGridViewTextBoxColumn1
        '
        Me.MemManufacturer3DataGridViewTextBoxColumn1.DataPropertyName = "MemManufacturer3"
        Me.MemManufacturer3DataGridViewTextBoxColumn1.HeaderText = "Memory Manufacturer 3"
        Me.MemManufacturer3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemManufacturer3DataGridViewTextBoxColumn1.Name = "MemManufacturer3DataGridViewTextBoxColumn1"
        Me.MemManufacturer3DataGridViewTextBoxColumn1.Width = 150
        '
        'MemSN4DataGridViewTextBoxColumn1
        '
        Me.MemSN4DataGridViewTextBoxColumn1.DataPropertyName = "MemSN4"
        Me.MemSN4DataGridViewTextBoxColumn1.HeaderText = "Memory SN 4"
        Me.MemSN4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemSN4DataGridViewTextBoxColumn1.Name = "MemSN4DataGridViewTextBoxColumn1"
        Me.MemSN4DataGridViewTextBoxColumn1.Width = 150
        '
        'MemCapacity4DataGridViewTextBoxColumn1
        '
        Me.MemCapacity4DataGridViewTextBoxColumn1.DataPropertyName = "MemCapacity4"
        Me.MemCapacity4DataGridViewTextBoxColumn1.HeaderText = "Memory Capacity 4"
        Me.MemCapacity4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemCapacity4DataGridViewTextBoxColumn1.Name = "MemCapacity4DataGridViewTextBoxColumn1"
        Me.MemCapacity4DataGridViewTextBoxColumn1.Width = 150
        '
        'MemManufacturer4DataGridViewTextBoxColumn1
        '
        Me.MemManufacturer4DataGridViewTextBoxColumn1.DataPropertyName = "MemManufacturer4"
        Me.MemManufacturer4DataGridViewTextBoxColumn1.HeaderText = "Memory Manufacturer 4"
        Me.MemManufacturer4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MemManufacturer4DataGridViewTextBoxColumn1.Name = "MemManufacturer4DataGridViewTextBoxColumn1"
        Me.MemManufacturer4DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN1DataGridViewTextBoxColumn2
        '
        Me.HDDSN1DataGridViewTextBoxColumn2.DataPropertyName = "HDDSN1"
        Me.HDDSN1DataGridViewTextBoxColumn2.HeaderText = "HDD SN 1"
        Me.HDDSN1DataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.HDDSN1DataGridViewTextBoxColumn2.Name = "HDDSN1DataGridViewTextBoxColumn2"
        Me.HDDSN1DataGridViewTextBoxColumn2.Width = 150
        '
        'HDDCapacity1DataGridViewTextBoxColumn1
        '
        Me.HDDCapacity1DataGridViewTextBoxColumn1.DataPropertyName = "HDDCapacity1"
        Me.HDDCapacity1DataGridViewTextBoxColumn1.HeaderText = "HDD Capacity 1"
        Me.HDDCapacity1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDCapacity1DataGridViewTextBoxColumn1.Name = "HDDCapacity1DataGridViewTextBoxColumn1"
        Me.HDDCapacity1DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDModel1DataGridViewTextBoxColumn1
        '
        Me.HDDModel1DataGridViewTextBoxColumn1.DataPropertyName = "HDDModel1"
        Me.HDDModel1DataGridViewTextBoxColumn1.HeaderText = "HDD Model 1"
        Me.HDDModel1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDModel1DataGridViewTextBoxColumn1.Name = "HDDModel1DataGridViewTextBoxColumn1"
        Me.HDDModel1DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN2DataGridViewTextBoxColumn2
        '
        Me.HDDSN2DataGridViewTextBoxColumn2.DataPropertyName = "HDDSN2"
        Me.HDDSN2DataGridViewTextBoxColumn2.HeaderText = "HDD SN 2"
        Me.HDDSN2DataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.HDDSN2DataGridViewTextBoxColumn2.Name = "HDDSN2DataGridViewTextBoxColumn2"
        Me.HDDSN2DataGridViewTextBoxColumn2.Width = 150
        '
        'HDDCapacity2DataGridViewTextBoxColumn1
        '
        Me.HDDCapacity2DataGridViewTextBoxColumn1.DataPropertyName = "HDDCapacity2"
        Me.HDDCapacity2DataGridViewTextBoxColumn1.HeaderText = "HDD Capacity 2"
        Me.HDDCapacity2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDCapacity2DataGridViewTextBoxColumn1.Name = "HDDCapacity2DataGridViewTextBoxColumn1"
        Me.HDDCapacity2DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDModel2DataGridViewTextBoxColumn1
        '
        Me.HDDModel2DataGridViewTextBoxColumn1.DataPropertyName = "HDDModel2"
        Me.HDDModel2DataGridViewTextBoxColumn1.HeaderText = "HDD Model 2"
        Me.HDDModel2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDModel2DataGridViewTextBoxColumn1.Name = "HDDModel2DataGridViewTextBoxColumn1"
        Me.HDDModel2DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN3DataGridViewTextBoxColumn2
        '
        Me.HDDSN3DataGridViewTextBoxColumn2.DataPropertyName = "HDDSN3"
        Me.HDDSN3DataGridViewTextBoxColumn2.HeaderText = "HDD SN 3"
        Me.HDDSN3DataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.HDDSN3DataGridViewTextBoxColumn2.Name = "HDDSN3DataGridViewTextBoxColumn2"
        Me.HDDSN3DataGridViewTextBoxColumn2.Width = 150
        '
        'HDDCapacity3DataGridViewTextBoxColumn1
        '
        Me.HDDCapacity3DataGridViewTextBoxColumn1.DataPropertyName = "HDDCapacity3"
        Me.HDDCapacity3DataGridViewTextBoxColumn1.HeaderText = "HDD Capacity 3"
        Me.HDDCapacity3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDCapacity3DataGridViewTextBoxColumn1.Name = "HDDCapacity3DataGridViewTextBoxColumn1"
        Me.HDDCapacity3DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDModel3DataGridViewTextBoxColumn1
        '
        Me.HDDModel3DataGridViewTextBoxColumn1.DataPropertyName = "HDDModel3"
        Me.HDDModel3DataGridViewTextBoxColumn1.HeaderText = "HDD Model 3"
        Me.HDDModel3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDModel3DataGridViewTextBoxColumn1.Name = "HDDModel3DataGridViewTextBoxColumn1"
        Me.HDDModel3DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN4DataGridViewTextBoxColumn2
        '
        Me.HDDSN4DataGridViewTextBoxColumn2.DataPropertyName = "HDDSN4"
        Me.HDDSN4DataGridViewTextBoxColumn2.HeaderText = "HDD SN 4"
        Me.HDDSN4DataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.HDDSN4DataGridViewTextBoxColumn2.Name = "HDDSN4DataGridViewTextBoxColumn2"
        Me.HDDSN4DataGridViewTextBoxColumn2.Width = 150
        '
        'HDDCapacity4DataGridViewTextBoxColumn1
        '
        Me.HDDCapacity4DataGridViewTextBoxColumn1.DataPropertyName = "HDDCapacity4"
        Me.HDDCapacity4DataGridViewTextBoxColumn1.HeaderText = "HDD Capacity 4"
        Me.HDDCapacity4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDCapacity4DataGridViewTextBoxColumn1.Name = "HDDCapacity4DataGridViewTextBoxColumn1"
        Me.HDDCapacity4DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDModel4DataGridViewTextBoxColumn1
        '
        Me.HDDModel4DataGridViewTextBoxColumn1.DataPropertyName = "HDDModel4"
        Me.HDDModel4DataGridViewTextBoxColumn1.HeaderText = "HDD Model 4"
        Me.HDDModel4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDModel4DataGridViewTextBoxColumn1.Name = "HDDModel4DataGridViewTextBoxColumn1"
        Me.HDDModel4DataGridViewTextBoxColumn1.Width = 150
        '
        'MonSN1DataGridViewTextBoxColumn1
        '
        Me.MonSN1DataGridViewTextBoxColumn1.DataPropertyName = "MonSN1"
        Me.MonSN1DataGridViewTextBoxColumn1.HeaderText = "Monitor SN 1"
        Me.MonSN1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MonSN1DataGridViewTextBoxColumn1.Name = "MonSN1DataGridViewTextBoxColumn1"
        Me.MonSN1DataGridViewTextBoxColumn1.Width = 150
        '
        'MonModel1DataGridViewTextBoxColumn
        '
        Me.MonModel1DataGridViewTextBoxColumn.DataPropertyName = "MonModel1"
        Me.MonModel1DataGridViewTextBoxColumn.HeaderText = "Monitor Model 1"
        Me.MonModel1DataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MonModel1DataGridViewTextBoxColumn.Name = "MonModel1DataGridViewTextBoxColumn"
        Me.MonModel1DataGridViewTextBoxColumn.Width = 150
        '
        'MonSN2DataGridViewTextBoxColumn1
        '
        Me.MonSN2DataGridViewTextBoxColumn1.DataPropertyName = "MonSN2"
        Me.MonSN2DataGridViewTextBoxColumn1.HeaderText = "Monitor SN 2"
        Me.MonSN2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.MonSN2DataGridViewTextBoxColumn1.Name = "MonSN2DataGridViewTextBoxColumn1"
        Me.MonSN2DataGridViewTextBoxColumn1.Width = 150
        '
        'MonModel2DataGridViewTextBoxColumn
        '
        Me.MonModel2DataGridViewTextBoxColumn.DataPropertyName = "MonModel2"
        Me.MonModel2DataGridViewTextBoxColumn.HeaderText = "Monitor Model 2"
        Me.MonModel2DataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MonModel2DataGridViewTextBoxColumn.Name = "MonModel2DataGridViewTextBoxColumn"
        Me.MonModel2DataGridViewTextBoxColumn.Width = 150
        '
        'UPSSNDataGridViewTextBoxColumn1
        '
        Me.UPSSNDataGridViewTextBoxColumn1.DataPropertyName = "UPSSN"
        Me.UPSSNDataGridViewTextBoxColumn1.HeaderText = "UPS SN"
        Me.UPSSNDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.UPSSNDataGridViewTextBoxColumn1.Name = "UPSSNDataGridViewTextBoxColumn1"
        Me.UPSSNDataGridViewTextBoxColumn1.Width = 150
        '
        'UPSTimeLeftDataGridViewTextBoxColumn
        '
        Me.UPSTimeLeftDataGridViewTextBoxColumn.DataPropertyName = "UPSTimeLeft"
        Me.UPSTimeLeftDataGridViewTextBoxColumn.HeaderText = "UPS Time Left"
        Me.UPSTimeLeftDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.UPSTimeLeftDataGridViewTextBoxColumn.Name = "UPSTimeLeftDataGridViewTextBoxColumn"
        Me.UPSTimeLeftDataGridViewTextBoxColumn.Width = 150
        '
        'KeteranganDataGridViewTextBoxColumn
        '
        Me.KeteranganDataGridViewTextBoxColumn.DataPropertyName = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn.HeaderText = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.KeteranganDataGridViewTextBoxColumn.Name = "KeteranganDataGridViewTextBoxColumn"
        Me.KeteranganDataGridViewTextBoxColumn.Width = 150
        '
        'AssetFisikBindingSource
        '
        Me.AssetFisikBindingSource.DataMember = "Trans_AuditAsset"
        Me.AssetFisikBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Controls.Add(Me.lblTotalData_NotConnected)
        Me.TabPage4.Controls.Add(Me.btnSave_NotConnectedData)
        Me.TabPage4.Controls.Add(Me.PanelSearch_NotConnected)
        Me.TabPage4.Controls.Add(Me.dgvAssetFisik_NotConnectedData)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1175, 562)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Data Not Connected"
        '
        'lblTotalData_NotConnected
        '
        Me.lblTotalData_NotConnected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalData_NotConnected.AutoSize = True
        Me.lblTotalData_NotConnected.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalData_NotConnected.Location = New System.Drawing.Point(6, 673)
        Me.lblTotalData_NotConnected.Name = "lblTotalData_NotConnected"
        Me.lblTotalData_NotConnected.Size = New System.Drawing.Size(172, 15)
        Me.lblTotalData_NotConnected.TabIndex = 117
        Me.lblTotalData_NotConnected.Text = "Displaying 0 Physic Asset Lists"
        '
        'btnSave_NotConnectedData
        '
        Me.btnSave_NotConnectedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSave_NotConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave_NotConnectedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave_NotConnectedData.ForeColor = System.Drawing.Color.White
        Me.btnSave_NotConnectedData.Image = CType(resources.GetObject("btnSave_NotConnectedData.Image"), System.Drawing.Image)
        Me.btnSave_NotConnectedData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave_NotConnectedData.Location = New System.Drawing.Point(0, 2)
        Me.btnSave_NotConnectedData.Name = "btnSave_NotConnectedData"
        Me.btnSave_NotConnectedData.Size = New System.Drawing.Size(64, 26)
        Me.btnSave_NotConnectedData.TabIndex = 112
        Me.btnSave_NotConnectedData.Text = "      Save"
        Me.btnSave_NotConnectedData.UseVisualStyleBackColor = False
        '
        'PanelSearch_NotConnected
        '
        Me.PanelSearch_NotConnected.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelSearch_NotConnected.BackColor = System.Drawing.Color.Transparent
        Me.PanelSearch_NotConnected.Controls.Add(Me.comboField_NotConnected)
        Me.PanelSearch_NotConnected.Controls.Add(Me.txtSearch_NotConnected)
        Me.PanelSearch_NotConnected.Controls.Add(Me.btnSearch_NotConnected)
        Me.PanelSearch_NotConnected.Controls.Add(Me.btnDownload_NotConnectedData)
        Me.PanelSearch_NotConnected.Location = New System.Drawing.Point(706, 0)
        Me.PanelSearch_NotConnected.Name = "PanelSearch_NotConnected"
        Me.PanelSearch_NotConnected.Size = New System.Drawing.Size(468, 30)
        Me.PanelSearch_NotConnected.TabIndex = 114
        '
        'comboField_NotConnected
        '
        Me.comboField_NotConnected.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboField_NotConnected.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboField_NotConnected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboField_NotConnected.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboField_NotConnected.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboField_NotConnected.FormattingEnabled = True
        Me.comboField_NotConnected.ItemHeight = 15
        Me.comboField_NotConnected.Items.AddRange(New Object() {"All", "Status", "Uploaded", "Computer Name"})
        Me.comboField_NotConnected.Location = New System.Drawing.Point(19, 5)
        Me.comboField_NotConnected.Name = "comboField_NotConnected"
        Me.comboField_NotConnected.Size = New System.Drawing.Size(213, 23)
        Me.comboField_NotConnected.TabIndex = 111
        '
        'txtSearch_NotConnected
        '
        Me.txtSearch_NotConnected.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearch_NotConnected.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch_NotConnected.Location = New System.Drawing.Point(235, 6)
        Me.txtSearch_NotConnected.Name = "txtSearch_NotConnected"
        Me.txtSearch_NotConnected.Size = New System.Drawing.Size(177, 22)
        Me.txtSearch_NotConnected.TabIndex = 99
        '
        'btnSearch_NotConnected
        '
        Me.btnSearch_NotConnected.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch_NotConnected.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearch_NotConnected.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearch_NotConnected.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch_NotConnected.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch_NotConnected.ForeColor = System.Drawing.Color.White
        Me.btnSearch_NotConnected.Image = CType(resources.GetObject("btnSearch_NotConnected.Image"), System.Drawing.Image)
        Me.btnSearch_NotConnected.Location = New System.Drawing.Point(412, 5)
        Me.btnSearch_NotConnected.Name = "btnSearch_NotConnected"
        Me.btnSearch_NotConnected.Size = New System.Drawing.Size(26, 23)
        Me.btnSearch_NotConnected.TabIndex = 99
        Me.btnSearch_NotConnected.UseVisualStyleBackColor = False
        '
        'btnDownload_NotConnectedData
        '
        Me.btnDownload_NotConnectedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownload_NotConnectedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownload_NotConnectedData.ForeColor = System.Drawing.Color.White
        Me.btnDownload_NotConnectedData.Image = CType(resources.GetObject("btnDownload_NotConnectedData.Image"), System.Drawing.Image)
        Me.btnDownload_NotConnectedData.Location = New System.Drawing.Point(439, 5)
        Me.btnDownload_NotConnectedData.Name = "btnDownload_NotConnectedData"
        Me.btnDownload_NotConnectedData.Size = New System.Drawing.Size(26, 23)
        Me.btnDownload_NotConnectedData.TabIndex = 101
        Me.btnDownload_NotConnectedData.UseVisualStyleBackColor = False
        '
        'dgvAssetFisik_NotConnectedData
        '
        Me.dgvAssetFisik_NotConnectedData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAssetFisik_NotConnectedData.AutoGenerateColumns = False
        Me.dgvAssetFisik_NotConnectedData.BackgroundColor = System.Drawing.Color.White
        Me.dgvAssetFisik_NotConnectedData.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAssetFisik_NotConnectedData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgvAssetFisik_NotConnectedData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssetFisik_NotConnectedData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewCheckBoxColumn3, Me.UploadedDataGridViewTextBoxColumn2, Me.ComputerNameDataGridViewTextBoxColumn3, Me.KeteranganDataGridViewTextBoxColumn1})
        Me.dgvAssetFisik_NotConnectedData.DataSource = Me.AssetFisikBindingSource
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAssetFisik_NotConnectedData.DefaultCellStyle = DataGridViewCellStyle21
        Me.dgvAssetFisik_NotConnectedData.Location = New System.Drawing.Point(0, 31)
        Me.dgvAssetFisik_NotConnectedData.Name = "dgvAssetFisik_NotConnectedData"
        Me.dgvAssetFisik_NotConnectedData.RowHeadersWidth = 62
        Me.dgvAssetFisik_NotConnectedData.Size = New System.Drawing.Size(1171, 452)
        Me.dgvAssetFisik_NotConnectedData.TabIndex = 10
        '
        'StatusDataGridViewCheckBoxColumn3
        '
        Me.StatusDataGridViewCheckBoxColumn3.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn3.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn3.MinimumWidth = 8
        Me.StatusDataGridViewCheckBoxColumn3.Name = "StatusDataGridViewCheckBoxColumn3"
        Me.StatusDataGridViewCheckBoxColumn3.Width = 150
        '
        'UploadedDataGridViewTextBoxColumn2
        '
        Me.UploadedDataGridViewTextBoxColumn2.DataPropertyName = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn2.HeaderText = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.UploadedDataGridViewTextBoxColumn2.Name = "UploadedDataGridViewTextBoxColumn2"
        Me.UploadedDataGridViewTextBoxColumn2.Width = 150
        '
        'ComputerNameDataGridViewTextBoxColumn3
        '
        Me.ComputerNameDataGridViewTextBoxColumn3.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn3.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn3.MinimumWidth = 8
        Me.ComputerNameDataGridViewTextBoxColumn3.Name = "ComputerNameDataGridViewTextBoxColumn3"
        Me.ComputerNameDataGridViewTextBoxColumn3.Width = 150
        '
        'KeteranganDataGridViewTextBoxColumn1
        '
        Me.KeteranganDataGridViewTextBoxColumn1.DataPropertyName = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn1.HeaderText = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.KeteranganDataGridViewTextBoxColumn1.Name = "KeteranganDataGridViewTextBoxColumn1"
        Me.KeteranganDataGridViewTextBoxColumn1.Width = 150
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.White
        Me.TabPage5.Controls.Add(Me.lblTotalData_AccessDenied)
        Me.TabPage5.Controls.Add(Me.Panel9)
        Me.TabPage5.Controls.Add(Me.btnSave_DeniedData)
        Me.TabPage5.Controls.Add(Me.dgvAssetFisik_DeniedData)
        Me.TabPage5.Location = New System.Drawing.Point(4, 24)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1175, 562)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "Data Denied"
        '
        'lblTotalData_AccessDenied
        '
        Me.lblTotalData_AccessDenied.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalData_AccessDenied.AutoSize = True
        Me.lblTotalData_AccessDenied.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalData_AccessDenied.Location = New System.Drawing.Point(6, 673)
        Me.lblTotalData_AccessDenied.Name = "lblTotalData_AccessDenied"
        Me.lblTotalData_AccessDenied.Size = New System.Drawing.Size(172, 15)
        Me.lblTotalData_AccessDenied.TabIndex = 120
        Me.lblTotalData_AccessDenied.Text = "Displaying 0 Physic Asset Lists"
        '
        'Panel9
        '
        Me.Panel9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Panel9.Controls.Add(Me.comboTransaction_AccessDenied)
        Me.Panel9.Controls.Add(Me.txtSearchTransaction_AccessDenied)
        Me.Panel9.Controls.Add(Me.btnSearchTransaction_AccessDenied)
        Me.Panel9.Controls.Add(Me.btnDownload_DeniedData)
        Me.Panel9.Location = New System.Drawing.Point(706, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(468, 30)
        Me.Panel9.TabIndex = 119
        '
        'comboTransaction_AccessDenied
        '
        Me.comboTransaction_AccessDenied.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboTransaction_AccessDenied.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboTransaction_AccessDenied.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboTransaction_AccessDenied.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboTransaction_AccessDenied.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboTransaction_AccessDenied.FormattingEnabled = True
        Me.comboTransaction_AccessDenied.ItemHeight = 15
        Me.comboTransaction_AccessDenied.Items.AddRange(New Object() {"All", "Status", "Uploaded", "Computer Name"})
        Me.comboTransaction_AccessDenied.Location = New System.Drawing.Point(19, 5)
        Me.comboTransaction_AccessDenied.Name = "comboTransaction_AccessDenied"
        Me.comboTransaction_AccessDenied.Size = New System.Drawing.Size(213, 23)
        Me.comboTransaction_AccessDenied.TabIndex = 111
        '
        'txtSearchTransaction_AccessDenied
        '
        Me.txtSearchTransaction_AccessDenied.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchTransaction_AccessDenied.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchTransaction_AccessDenied.Location = New System.Drawing.Point(235, 6)
        Me.txtSearchTransaction_AccessDenied.Name = "txtSearchTransaction_AccessDenied"
        Me.txtSearchTransaction_AccessDenied.Size = New System.Drawing.Size(177, 22)
        Me.txtSearchTransaction_AccessDenied.TabIndex = 99
        '
        'btnSearchTransaction_AccessDenied
        '
        Me.btnSearchTransaction_AccessDenied.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchTransaction_AccessDenied.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearchTransaction_AccessDenied.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearchTransaction_AccessDenied.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchTransaction_AccessDenied.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchTransaction_AccessDenied.ForeColor = System.Drawing.Color.White
        Me.btnSearchTransaction_AccessDenied.Image = CType(resources.GetObject("btnSearchTransaction_AccessDenied.Image"), System.Drawing.Image)
        Me.btnSearchTransaction_AccessDenied.Location = New System.Drawing.Point(412, 5)
        Me.btnSearchTransaction_AccessDenied.Name = "btnSearchTransaction_AccessDenied"
        Me.btnSearchTransaction_AccessDenied.Size = New System.Drawing.Size(26, 23)
        Me.btnSearchTransaction_AccessDenied.TabIndex = 99
        Me.btnSearchTransaction_AccessDenied.UseVisualStyleBackColor = False
        '
        'btnDownload_DeniedData
        '
        Me.btnDownload_DeniedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownload_DeniedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownload_DeniedData.ForeColor = System.Drawing.Color.White
        Me.btnDownload_DeniedData.Image = CType(resources.GetObject("btnDownload_DeniedData.Image"), System.Drawing.Image)
        Me.btnDownload_DeniedData.Location = New System.Drawing.Point(439, 5)
        Me.btnDownload_DeniedData.Name = "btnDownload_DeniedData"
        Me.btnDownload_DeniedData.Size = New System.Drawing.Size(26, 23)
        Me.btnDownload_DeniedData.TabIndex = 101
        Me.btnDownload_DeniedData.UseVisualStyleBackColor = False
        '
        'btnSave_DeniedData
        '
        Me.btnSave_DeniedData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSave_DeniedData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave_DeniedData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave_DeniedData.ForeColor = System.Drawing.Color.White
        Me.btnSave_DeniedData.Image = CType(resources.GetObject("btnSave_DeniedData.Image"), System.Drawing.Image)
        Me.btnSave_DeniedData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave_DeniedData.Location = New System.Drawing.Point(0, 2)
        Me.btnSave_DeniedData.Name = "btnSave_DeniedData"
        Me.btnSave_DeniedData.Size = New System.Drawing.Size(64, 26)
        Me.btnSave_DeniedData.TabIndex = 114
        Me.btnSave_DeniedData.Text = "      Save"
        Me.btnSave_DeniedData.UseVisualStyleBackColor = False
        '
        'dgvAssetFisik_DeniedData
        '
        Me.dgvAssetFisik_DeniedData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAssetFisik_DeniedData.AutoGenerateColumns = False
        Me.dgvAssetFisik_DeniedData.BackgroundColor = System.Drawing.Color.White
        Me.dgvAssetFisik_DeniedData.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAssetFisik_DeniedData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvAssetFisik_DeniedData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssetFisik_DeniedData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewCheckBoxColumn4, Me.UploadedDataGridViewTextBoxColumn3, Me.ComputerNameDataGridViewTextBoxColumn4, Me.KeteranganDataGridViewTextBoxColumn2})
        Me.dgvAssetFisik_DeniedData.DataSource = Me.AssetFisikBindingSource
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.Desktop
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAssetFisik_DeniedData.DefaultCellStyle = DataGridViewCellStyle16
        Me.dgvAssetFisik_DeniedData.Location = New System.Drawing.Point(0, 31)
        Me.dgvAssetFisik_DeniedData.Name = "dgvAssetFisik_DeniedData"
        Me.dgvAssetFisik_DeniedData.RowHeadersWidth = 62
        Me.dgvAssetFisik_DeniedData.Size = New System.Drawing.Size(1171, 452)
        Me.dgvAssetFisik_DeniedData.TabIndex = 24
        '
        'StatusDataGridViewCheckBoxColumn4
        '
        Me.StatusDataGridViewCheckBoxColumn4.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn4.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn4.MinimumWidth = 8
        Me.StatusDataGridViewCheckBoxColumn4.Name = "StatusDataGridViewCheckBoxColumn4"
        Me.StatusDataGridViewCheckBoxColumn4.Width = 150
        '
        'UploadedDataGridViewTextBoxColumn3
        '
        Me.UploadedDataGridViewTextBoxColumn3.DataPropertyName = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn3.HeaderText = "Uploaded"
        Me.UploadedDataGridViewTextBoxColumn3.MinimumWidth = 8
        Me.UploadedDataGridViewTextBoxColumn3.Name = "UploadedDataGridViewTextBoxColumn3"
        Me.UploadedDataGridViewTextBoxColumn3.Width = 150
        '
        'ComputerNameDataGridViewTextBoxColumn4
        '
        Me.ComputerNameDataGridViewTextBoxColumn4.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn4.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn4.MinimumWidth = 8
        Me.ComputerNameDataGridViewTextBoxColumn4.Name = "ComputerNameDataGridViewTextBoxColumn4"
        Me.ComputerNameDataGridViewTextBoxColumn4.Width = 150
        '
        'KeteranganDataGridViewTextBoxColumn2
        '
        Me.KeteranganDataGridViewTextBoxColumn2.DataPropertyName = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn2.HeaderText = "Keterangan"
        Me.KeteranganDataGridViewTextBoxColumn2.MinimumWidth = 8
        Me.KeteranganDataGridViewTextBoxColumn2.Name = "KeteranganDataGridViewTextBoxColumn2"
        Me.KeteranganDataGridViewTextBoxColumn2.Width = 150
        '
        'TabCompareData
        '
        Me.TabCompareData.Controls.Add(Me.Panel6)
        Me.TabCompareData.Controls.Add(Me.Panel2)
        Me.TabCompareData.Controls.Add(Me.Panel1)
        Me.TabCompareData.Controls.Add(Me.Label4)
        Me.TabCompareData.Controls.Add(Me.Label3)
        Me.TabCompareData.Controls.Add(Me.Label2)
        Me.TabCompareData.Controls.Add(Me.Label1)
        Me.TabCompareData.Controls.Add(Me.lbl_ketMerahCompData)
        Me.TabCompareData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabCompareData.Location = New System.Drawing.Point(4, 36)
        Me.TabCompareData.Name = "TabCompareData"
        Me.TabCompareData.Size = New System.Drawing.Size(1173, 583)
        Me.TabCompareData.TabIndex = 99
        Me.TabCompareData.Text = "Compare Data"
        Me.TabCompareData.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.Panel11)
        Me.Panel6.Controls.Add(Me.lblTotal_ComparableData)
        Me.Panel6.Controls.Add(Me.btnShow_CompareData)
        Me.Panel6.Controls.Add(Me.btnSave_CompareData)
        Me.Panel6.Controls.Add(Me.dgvCompareData)
        Me.Panel6.Controls.Add(Me.lblPercent_CompareData)
        Me.Panel6.Controls.Add(Me.progressBarCompareData)
        Me.Panel6.Controls.Add(Me.btnRefresh_CompareData)
        Me.Panel6.Location = New System.Drawing.Point(7, 157)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1158, 424)
        Me.Panel6.TabIndex = 114
        '
        'Panel11
        '
        Me.Panel11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Panel11.Controls.Add(Me.comboSearch_ComparableData)
        Me.Panel11.Controls.Add(Me.txtSearch_ComparableData)
        Me.Panel11.Controls.Add(Me.btnSearch_ComparableData)
        Me.Panel11.Controls.Add(Me.btnDownload_ComparableData)
        Me.Panel11.Location = New System.Drawing.Point(684, 7)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(468, 30)
        Me.Panel11.TabIndex = 116
        '
        'comboSearch_ComparableData
        '
        Me.comboSearch_ComparableData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.comboSearch_ComparableData.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.comboSearch_ComparableData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboSearch_ComparableData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboSearch_ComparableData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboSearch_ComparableData.FormattingEnabled = True
        Me.comboSearch_ComparableData.ItemHeight = 15
        Me.comboSearch_ComparableData.Items.AddRange(New Object() {"All", "Status", "Uploaded", "Computer Name", "Domain", "Model", "OS Version", "Service Tag SN", "Memory SN 1", "Memory Capacity 1", "Memory Manufacturer 1", "Memory SN 2", "Memory Capacity 2", "Memory Manufacturer 2", "Memory SN 3", "Memory Capacity 3", "Memory Manufacturer 3", "Memory SN 4", "Memory Capacity 4", "Memory Manufacturer 4", "HDD SN 1", "HDD Capacity 1", "HDD Model 1", "HDD SN 2", "HDD Capacity 2", "HDD Model 2", "HDD SN 3", "HDD Capacity 3", "HDD Model 3", "HDD SN 4", "HDD Capacity 4", "HDD Model 4", "Monitor SN 1", "Monitor Model 1", "Monitor SN 2", "Monitor Model 2", "UPS SN", "UPS Time Left", "Keterangan"})
        Me.comboSearch_ComparableData.Location = New System.Drawing.Point(22, 5)
        Me.comboSearch_ComparableData.Name = "comboSearch_ComparableData"
        Me.comboSearch_ComparableData.Size = New System.Drawing.Size(213, 23)
        Me.comboSearch_ComparableData.TabIndex = 111
        '
        'txtSearch_ComparableData
        '
        Me.txtSearch_ComparableData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearch_ComparableData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch_ComparableData.Location = New System.Drawing.Point(238, 6)
        Me.txtSearch_ComparableData.Name = "txtSearch_ComparableData"
        Me.txtSearch_ComparableData.Size = New System.Drawing.Size(177, 21)
        Me.txtSearch_ComparableData.TabIndex = 99
        '
        'btnSearch_ComparableData
        '
        Me.btnSearch_ComparableData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch_ComparableData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSearch_ComparableData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSearch_ComparableData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch_ComparableData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch_ComparableData.ForeColor = System.Drawing.Color.White
        Me.btnSearch_ComparableData.Image = CType(resources.GetObject("btnSearch_ComparableData.Image"), System.Drawing.Image)
        Me.btnSearch_ComparableData.Location = New System.Drawing.Point(413, 5)
        Me.btnSearch_ComparableData.Name = "btnSearch_ComparableData"
        Me.btnSearch_ComparableData.Size = New System.Drawing.Size(26, 23)
        Me.btnSearch_ComparableData.TabIndex = 99
        Me.btnSearch_ComparableData.UseVisualStyleBackColor = False
        '
        'btnDownload_ComparableData
        '
        Me.btnDownload_ComparableData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDownload_ComparableData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDownload_ComparableData.ForeColor = System.Drawing.Color.White
        Me.btnDownload_ComparableData.Image = CType(resources.GetObject("btnDownload_ComparableData.Image"), System.Drawing.Image)
        Me.btnDownload_ComparableData.Location = New System.Drawing.Point(439, 5)
        Me.btnDownload_ComparableData.Name = "btnDownload_ComparableData"
        Me.btnDownload_ComparableData.Size = New System.Drawing.Size(26, 23)
        Me.btnDownload_ComparableData.TabIndex = 101
        Me.btnDownload_ComparableData.UseVisualStyleBackColor = False
        '
        'lblTotal_ComparableData
        '
        Me.lblTotal_ComparableData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotal_ComparableData.AutoSize = True
        Me.lblTotal_ComparableData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal_ComparableData.Location = New System.Drawing.Point(6, 400)
        Me.lblTotal_ComparableData.Name = "lblTotal_ComparableData"
        Me.lblTotal_ComparableData.Size = New System.Drawing.Size(202, 15)
        Me.lblTotal_ComparableData.TabIndex = 115
        Me.lblTotal_ComparableData.Text = "Displaying 0 Comparable Data Lists"
        '
        'btnShow_CompareData
        '
        Me.btnShow_CompareData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnShow_CompareData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShow_CompareData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShow_CompareData.ForeColor = System.Drawing.Color.White
        Me.btnShow_CompareData.Image = CType(resources.GetObject("btnShow_CompareData.Image"), System.Drawing.Image)
        Me.btnShow_CompareData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShow_CompareData.Location = New System.Drawing.Point(83, 5)
        Me.btnShow_CompareData.Name = "btnShow_CompareData"
        Me.btnShow_CompareData.Size = New System.Drawing.Size(84, 34)
        Me.btnShow_CompareData.TabIndex = 99
        Me.btnShow_CompareData.Text = "    Show"
        Me.btnShow_CompareData.UseVisualStyleBackColor = False
        '
        'btnSave_CompareData
        '
        Me.btnSave_CompareData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnSave_CompareData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave_CompareData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave_CompareData.ForeColor = System.Drawing.Color.White
        Me.btnSave_CompareData.Image = CType(resources.GetObject("btnSave_CompareData.Image"), System.Drawing.Image)
        Me.btnSave_CompareData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave_CompareData.Location = New System.Drawing.Point(4, 4)
        Me.btnSave_CompareData.Name = "btnSave_CompareData"
        Me.btnSave_CompareData.Size = New System.Drawing.Size(77, 35)
        Me.btnSave_CompareData.TabIndex = 99
        Me.btnSave_CompareData.Text = "    Save"
        Me.btnSave_CompareData.UseVisualStyleBackColor = False
        '
        'dgvCompareData
        '
        Me.dgvCompareData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCompareData.AutoGenerateColumns = False
        Me.dgvCompareData.BackgroundColor = System.Drawing.Color.White
        Me.dgvCompareData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCompareData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewCheckBoxColumn1, Me.StatusNMDataGridViewTextBoxColumn, Me.ComputerNameDataGridViewTextBoxColumn1, Me.ServiceTagSNDataGridViewTextBoxColumn1, Me.HDDSN1DataGridViewTextBoxColumn1, Me.HDDSN2DataGridViewTextBoxColumn1, Me.HDDSN3DataGridViewTextBoxColumn1, Me.HDDSN4DataGridViewTextBoxColumn1, Me.MemorySizeDataGridViewTextBoxColumn, Me.MemoryQtyDataGridViewTextBoxColumn, Me.MonSN1DataGridViewTextBoxColumn, Me.MonSN2DataGridViewTextBoxColumn, Me.UPSSNDataGridViewTextBoxColumn})
        Me.dgvCompareData.DataSource = Me.CompareDataBindingSource
        Me.dgvCompareData.Location = New System.Drawing.Point(6, 44)
        Me.dgvCompareData.Name = "dgvCompareData"
        Me.dgvCompareData.RowHeadersWidth = 62
        Me.dgvCompareData.Size = New System.Drawing.Size(1146, 347)
        Me.dgvCompareData.TabIndex = 99
        '
        'StatusDataGridViewCheckBoxColumn1
        '
        Me.StatusDataGridViewCheckBoxColumn1.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn1.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn1.MinimumWidth = 8
        Me.StatusDataGridViewCheckBoxColumn1.Name = "StatusDataGridViewCheckBoxColumn1"
        Me.StatusDataGridViewCheckBoxColumn1.Width = 150
        '
        'StatusNMDataGridViewTextBoxColumn
        '
        Me.StatusNMDataGridViewTextBoxColumn.DataPropertyName = "StatusNM"
        Me.StatusNMDataGridViewTextBoxColumn.HeaderText = "Status NM"
        Me.StatusNMDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.StatusNMDataGridViewTextBoxColumn.Name = "StatusNMDataGridViewTextBoxColumn"
        Me.StatusNMDataGridViewTextBoxColumn.Width = 150
        '
        'ComputerNameDataGridViewTextBoxColumn1
        '
        Me.ComputerNameDataGridViewTextBoxColumn1.DataPropertyName = "ComputerName"
        Me.ComputerNameDataGridViewTextBoxColumn1.HeaderText = "Computer Name"
        Me.ComputerNameDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.ComputerNameDataGridViewTextBoxColumn1.Name = "ComputerNameDataGridViewTextBoxColumn1"
        Me.ComputerNameDataGridViewTextBoxColumn1.Width = 150
        '
        'ServiceTagSNDataGridViewTextBoxColumn1
        '
        Me.ServiceTagSNDataGridViewTextBoxColumn1.DataPropertyName = "ServiceTagSN"
        Me.ServiceTagSNDataGridViewTextBoxColumn1.HeaderText = "Service Tag SN"
        Me.ServiceTagSNDataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.ServiceTagSNDataGridViewTextBoxColumn1.Name = "ServiceTagSNDataGridViewTextBoxColumn1"
        Me.ServiceTagSNDataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN1DataGridViewTextBoxColumn1
        '
        Me.HDDSN1DataGridViewTextBoxColumn1.DataPropertyName = "HDDSN1"
        Me.HDDSN1DataGridViewTextBoxColumn1.HeaderText = "HDD SN 1"
        Me.HDDSN1DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDSN1DataGridViewTextBoxColumn1.Name = "HDDSN1DataGridViewTextBoxColumn1"
        Me.HDDSN1DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN2DataGridViewTextBoxColumn1
        '
        Me.HDDSN2DataGridViewTextBoxColumn1.DataPropertyName = "HDDSN2"
        Me.HDDSN2DataGridViewTextBoxColumn1.HeaderText = "HDD SN 2"
        Me.HDDSN2DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDSN2DataGridViewTextBoxColumn1.Name = "HDDSN2DataGridViewTextBoxColumn1"
        Me.HDDSN2DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN3DataGridViewTextBoxColumn1
        '
        Me.HDDSN3DataGridViewTextBoxColumn1.DataPropertyName = "HDDSN3"
        Me.HDDSN3DataGridViewTextBoxColumn1.HeaderText = "HDD SN 3"
        Me.HDDSN3DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDSN3DataGridViewTextBoxColumn1.Name = "HDDSN3DataGridViewTextBoxColumn1"
        Me.HDDSN3DataGridViewTextBoxColumn1.Width = 150
        '
        'HDDSN4DataGridViewTextBoxColumn1
        '
        Me.HDDSN4DataGridViewTextBoxColumn1.DataPropertyName = "HDDSN4"
        Me.HDDSN4DataGridViewTextBoxColumn1.HeaderText = "HDD SN 4"
        Me.HDDSN4DataGridViewTextBoxColumn1.MinimumWidth = 8
        Me.HDDSN4DataGridViewTextBoxColumn1.Name = "HDDSN4DataGridViewTextBoxColumn1"
        Me.HDDSN4DataGridViewTextBoxColumn1.Width = 150
        '
        'MemorySizeDataGridViewTextBoxColumn
        '
        Me.MemorySizeDataGridViewTextBoxColumn.DataPropertyName = "MemorySize"
        Me.MemorySizeDataGridViewTextBoxColumn.HeaderText = "Memory Size"
        Me.MemorySizeDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MemorySizeDataGridViewTextBoxColumn.Name = "MemorySizeDataGridViewTextBoxColumn"
        Me.MemorySizeDataGridViewTextBoxColumn.Width = 150
        '
        'MemoryQtyDataGridViewTextBoxColumn
        '
        Me.MemoryQtyDataGridViewTextBoxColumn.DataPropertyName = "MemoryQty"
        Me.MemoryQtyDataGridViewTextBoxColumn.HeaderText = "Memory Qty"
        Me.MemoryQtyDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MemoryQtyDataGridViewTextBoxColumn.Name = "MemoryQtyDataGridViewTextBoxColumn"
        Me.MemoryQtyDataGridViewTextBoxColumn.Width = 150
        '
        'MonSN1DataGridViewTextBoxColumn
        '
        Me.MonSN1DataGridViewTextBoxColumn.DataPropertyName = "MonSN1"
        Me.MonSN1DataGridViewTextBoxColumn.HeaderText = "Monitor SN 1"
        Me.MonSN1DataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MonSN1DataGridViewTextBoxColumn.Name = "MonSN1DataGridViewTextBoxColumn"
        Me.MonSN1DataGridViewTextBoxColumn.Width = 150
        '
        'MonSN2DataGridViewTextBoxColumn
        '
        Me.MonSN2DataGridViewTextBoxColumn.DataPropertyName = "MonSN2"
        Me.MonSN2DataGridViewTextBoxColumn.HeaderText = "Monitor SN 2"
        Me.MonSN2DataGridViewTextBoxColumn.MinimumWidth = 8
        Me.MonSN2DataGridViewTextBoxColumn.Name = "MonSN2DataGridViewTextBoxColumn"
        Me.MonSN2DataGridViewTextBoxColumn.Width = 150
        '
        'UPSSNDataGridViewTextBoxColumn
        '
        Me.UPSSNDataGridViewTextBoxColumn.DataPropertyName = "UPSSN"
        Me.UPSSNDataGridViewTextBoxColumn.HeaderText = "UPS SN"
        Me.UPSSNDataGridViewTextBoxColumn.MinimumWidth = 8
        Me.UPSSNDataGridViewTextBoxColumn.Name = "UPSSNDataGridViewTextBoxColumn"
        Me.UPSSNDataGridViewTextBoxColumn.Width = 150
        '
        'CompareDataBindingSource
        '
        Me.CompareDataBindingSource.DataMember = "CompareData"
        Me.CompareDataBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'lblPercent_CompareData
        '
        Me.lblPercent_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercent_CompareData.AutoSize = True
        Me.lblPercent_CompareData.Location = New System.Drawing.Point(1050, 321)
        Me.lblPercent_CompareData.Name = "lblPercent_CompareData"
        Me.lblPercent_CompareData.Size = New System.Drawing.Size(26, 17)
        Me.lblPercent_CompareData.TabIndex = 105
        Me.lblPercent_CompareData.Text = "0%"
        '
        'progressBarCompareData
        '
        Me.progressBarCompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progressBarCompareData.Location = New System.Drawing.Point(1082, 318)
        Me.progressBarCompareData.Name = "progressBarCompareData"
        Me.progressBarCompareData.Size = New System.Drawing.Size(70, 21)
        Me.progressBarCompareData.TabIndex = 103
        '
        'btnRefresh_CompareData
        '
        Me.btnRefresh_CompareData.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnRefresh_CompareData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh_CompareData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh_CompareData.ForeColor = System.Drawing.Color.White
        Me.btnRefresh_CompareData.Image = CType(resources.GetObject("btnRefresh_CompareData.Image"), System.Drawing.Image)
        Me.btnRefresh_CompareData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh_CompareData.Location = New System.Drawing.Point(169, 6)
        Me.btnRefresh_CompareData.Name = "btnRefresh_CompareData"
        Me.btnRefresh_CompareData.Size = New System.Drawing.Size(89, 34)
        Me.btnRefresh_CompareData.TabIndex = 99
        Me.btnRefresh_CompareData.Text = "      Refresh"
        Me.btnRefresh_CompareData.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Location = New System.Drawing.Point(279, 32)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(525, 121)
        Me.Panel2.TabIndex = 113
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.DimGray
        Me.Panel3.Controls.Add(Me.lblSumSimilar)
        Me.Panel3.Controls.Add(Me.lbl_ketHijauCompData)
        Me.Panel3.Controls.Add(Me.lblCountDataTidakSesuai_CompareData)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.lblSumBeda)
        Me.Panel3.Controls.Add(Me.lblCountDataSesuai_CompareData)
        Me.Panel3.Controls.Add(Me.lblCountDataMendekatiSesuai_CompareData)
        Me.Panel3.Location = New System.Drawing.Point(7, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(510, 108)
        Me.Panel3.TabIndex = 111
        '
        'lblSumSimilar
        '
        Me.lblSumSimilar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSumSimilar.AutoSize = True
        Me.lblSumSimilar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumSimilar.ForeColor = System.Drawing.Color.White
        Me.lblSumSimilar.Image = CType(resources.GetObject("lblSumSimilar.Image"), System.Drawing.Image)
        Me.lblSumSimilar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSumSimilar.Location = New System.Drawing.Point(179, 68)
        Me.lblSumSimilar.Name = "lblSumSimilar"
        Me.lblSumSimilar.Size = New System.Drawing.Size(139, 13)
        Me.lblSumSimilar.TabIndex = 99
        Me.lblSumSimilar.Text = "       Data Mendekati Sesuai"
        '
        'lbl_ketHijauCompData
        '
        Me.lbl_ketHijauCompData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketHijauCompData.AutoSize = True
        Me.lbl_ketHijauCompData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketHijauCompData.ForeColor = System.Drawing.Color.White
        Me.lbl_ketHijauCompData.Image = CType(resources.GetObject("lbl_ketHijauCompData.Image"), System.Drawing.Image)
        Me.lbl_ketHijauCompData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl_ketHijauCompData.Location = New System.Drawing.Point(59, 69)
        Me.lbl_ketHijauCompData.Name = "lbl_ketHijauCompData"
        Me.lbl_ketHijauCompData.Size = New System.Drawing.Size(89, 13)
        Me.lbl_ketHijauCompData.TabIndex = 99
        Me.lbl_ketHijauCompData.Text = "        Data Sesuai"
        '
        'lblCountDataTidakSesuai_CompareData
        '
        Me.lblCountDataTidakSesuai_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCountDataTidakSesuai_CompareData.AutoSize = True
        Me.lblCountDataTidakSesuai_CompareData.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountDataTidakSesuai_CompareData.ForeColor = System.Drawing.Color.White
        Me.lblCountDataTidakSesuai_CompareData.Location = New System.Drawing.Point(368, 16)
        Me.lblCountDataTidakSesuai_CompareData.Name = "lblCountDataTidakSesuai_CompareData"
        Me.lblCountDataTidakSesuai_CompareData.Size = New System.Drawing.Size(98, 46)
        Me.lblCountDataTidakSesuai_CompareData.TabIndex = 102
        Me.lblCountDataTidakSesuai_CompareData.Text = "xxx "
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Image = CType(resources.GetObject("Label5.Image"), System.Drawing.Image)
        Me.Label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label5.Location = New System.Drawing.Point(15, 1)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 16)
        Me.Label5.TabIndex = 101
        Me.Label5.Text = "      Kesesuaian Data"
        '
        'lblSumBeda
        '
        Me.lblSumBeda.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSumBeda.AutoSize = True
        Me.lblSumBeda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumBeda.ForeColor = System.Drawing.Color.White
        Me.lblSumBeda.Image = CType(resources.GetObject("lblSumBeda.Image"), System.Drawing.Image)
        Me.lblSumBeda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSumBeda.Location = New System.Drawing.Point(347, 68)
        Me.lblSumBeda.Name = "lblSumBeda"
        Me.lblSumBeda.Size = New System.Drawing.Size(119, 13)
        Me.lblSumBeda.TabIndex = 99
        Me.lblSumBeda.Text = "        Data Tidak Sesuai"
        '
        'lblCountDataSesuai_CompareData
        '
        Me.lblCountDataSesuai_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCountDataSesuai_CompareData.AutoSize = True
        Me.lblCountDataSesuai_CompareData.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountDataSesuai_CompareData.ForeColor = System.Drawing.Color.White
        Me.lblCountDataSesuai_CompareData.Location = New System.Drawing.Point(62, 16)
        Me.lblCountDataSesuai_CompareData.Name = "lblCountDataSesuai_CompareData"
        Me.lblCountDataSesuai_CompareData.Size = New System.Drawing.Size(98, 46)
        Me.lblCountDataSesuai_CompareData.TabIndex = 99
        Me.lblCountDataSesuai_CompareData.Text = "xxx "
        '
        'lblCountDataMendekatiSesuai_CompareData
        '
        Me.lblCountDataMendekatiSesuai_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCountDataMendekatiSesuai_CompareData.AutoSize = True
        Me.lblCountDataMendekatiSesuai_CompareData.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountDataMendekatiSesuai_CompareData.ForeColor = System.Drawing.Color.White
        Me.lblCountDataMendekatiSesuai_CompareData.Location = New System.Drawing.Point(210, 17)
        Me.lblCountDataMendekatiSesuai_CompareData.Name = "lblCountDataMendekatiSesuai_CompareData"
        Me.lblCountDataMendekatiSesuai_CompareData.Size = New System.Drawing.Size(98, 46)
        Me.lblCountDataMendekatiSesuai_CompareData.TabIndex = 99
        Me.lblCountDataMendekatiSesuai_CompareData.Text = "xxx "
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(165, -206)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 26)
        Me.Label22.TabIndex = 99
        Me.Label22.Text = "0 Data"
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(19, -204)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(163, 17)
        Me.Label23.TabIndex = 99
        Me.Label23.Text = "Total PC - Connected :   "
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.sumAllData)
        Me.Panel1.Controls.Add(Me.lblAllData)
        Me.Panel1.Controls.Add(Me.Panel7)
        Me.Panel1.Location = New System.Drawing.Point(8, 32)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(250, 121)
        Me.Panel1.TabIndex = 112
        '
        'sumAllData
        '
        Me.sumAllData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.sumAllData.AutoSize = True
        Me.sumAllData.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sumAllData.Location = New System.Drawing.Point(165, -206)
        Me.sumAllData.Name = "sumAllData"
        Me.sumAllData.Size = New System.Drawing.Size(76, 26)
        Me.sumAllData.TabIndex = 99
        Me.sumAllData.Text = "0 Data"
        '
        'lblAllData
        '
        Me.lblAllData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAllData.AutoSize = True
        Me.lblAllData.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllData.Location = New System.Drawing.Point(19, -204)
        Me.lblAllData.Name = "lblAllData"
        Me.lblAllData.Size = New System.Drawing.Size(163, 17)
        Me.lblAllData.TabIndex = 99
        Me.lblAllData.Text = "Total PC - Connected :   "
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.BackColor = System.Drawing.Color.DimGray
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.lblSumNotChecked)
        Me.Panel7.Controls.Add(Me.lblSumChecked)
        Me.Panel7.Controls.Add(Me.sumChecked_CompareData)
        Me.Panel7.Controls.Add(Me.sumNotChecked_CompareData)
        Me.Panel7.ForeColor = System.Drawing.Color.White
        Me.Panel7.Location = New System.Drawing.Point(8, 6)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(234, 108)
        Me.Panel7.TabIndex = 109
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Image = CType(resources.GetObject("Label6.Image"), System.Drawing.Image)
        Me.Label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label6.Location = New System.Drawing.Point(10, 4)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(148, 16)
        Me.Label6.TabIndex = 100
        Me.Label6.Text = "      Total Checking Data"
        '
        'lblSumNotChecked
        '
        Me.lblSumNotChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSumNotChecked.AutoSize = True
        Me.lblSumNotChecked.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumNotChecked.Image = CType(resources.GetObject("lblSumNotChecked.Image"), System.Drawing.Image)
        Me.lblSumNotChecked.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSumNotChecked.Location = New System.Drawing.Point(123, 69)
        Me.lblSumNotChecked.Name = "lblSumNotChecked"
        Me.lblSumNotChecked.Size = New System.Drawing.Size(97, 13)
        Me.lblSumNotChecked.TabIndex = 99
        Me.lblSumNotChecked.Text = "         Not Checked"
        '
        'lblSumChecked
        '
        Me.lblSumChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSumChecked.AutoSize = True
        Me.lblSumChecked.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumChecked.Image = CType(resources.GetObject("lblSumChecked.Image"), System.Drawing.Image)
        Me.lblSumChecked.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSumChecked.Location = New System.Drawing.Point(26, 69)
        Me.lblSumChecked.Name = "lblSumChecked"
        Me.lblSumChecked.Size = New System.Drawing.Size(77, 13)
        Me.lblSumChecked.TabIndex = 99
        Me.lblSumChecked.Text = "         Checked"
        '
        'sumChecked_CompareData
        '
        Me.sumChecked_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.sumChecked_CompareData.AutoSize = True
        Me.sumChecked_CompareData.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sumChecked_CompareData.Location = New System.Drawing.Point(23, 20)
        Me.sumChecked_CompareData.Name = "sumChecked_CompareData"
        Me.sumChecked_CompareData.Size = New System.Drawing.Size(98, 46)
        Me.sumChecked_CompareData.TabIndex = 99
        Me.sumChecked_CompareData.Text = "xxx "
        '
        'sumNotChecked_CompareData
        '
        Me.sumNotChecked_CompareData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.sumNotChecked_CompareData.AutoSize = True
        Me.sumNotChecked_CompareData.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sumNotChecked_CompareData.Location = New System.Drawing.Point(124, 20)
        Me.sumNotChecked_CompareData.Name = "sumNotChecked_CompareData"
        Me.sumNotChecked_CompareData.Size = New System.Drawing.Size(98, 46)
        Me.sumNotChecked_CompareData.TabIndex = 99
        Me.sumNotChecked_CompareData.Text = "xxx "
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(557, 776)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(15, 21)
        Me.Label4.TabIndex = 99
        Me.Label4.Text = "|"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(697, 767)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 15)
        Me.Label3.TabIndex = 99
        Me.Label3.Text = "xxx "
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label2.Location = New System.Drawing.Point(737, 793)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 99
        Me.Label2.Text = "       Total Match : "
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Location = New System.Drawing.Point(577, 793)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 15)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "      Total Not Match  : "
        '
        'lbl_ketMerahCompData
        '
        Me.lbl_ketMerahCompData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbl_ketMerahCompData.AutoSize = True
        Me.lbl_ketMerahCompData.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ketMerahCompData.Image = CType(resources.GetObject("lbl_ketMerahCompData.Image"), System.Drawing.Image)
        Me.lbl_ketMerahCompData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl_ketMerahCompData.Location = New System.Drawing.Point(886, 784)
        Me.lbl_ketMerahCompData.Name = "lbl_ketMerahCompData"
        Me.lbl_ketMerahCompData.Size = New System.Drawing.Size(31, 15)
        Me.lbl_ketMerahCompData.TabIndex = 99
        Me.lbl_ketMerahCompData.Text = "       :"
        '
        'TabCompareAVADUC
        '
        Me.TabCompareAVADUC.Location = New System.Drawing.Point(4, 36)
        Me.TabCompareAVADUC.Margin = New System.Windows.Forms.Padding(2)
        Me.TabCompareAVADUC.Name = "TabCompareAVADUC"
        Me.TabCompareAVADUC.Size = New System.Drawing.Size(1173, 583)
        Me.TabCompareAVADUC.TabIndex = 100
        Me.TabCompareAVADUC.Text = "Compare AV ADUC"
        Me.TabCompareAVADUC.UseVisualStyleBackColor = True
        '
        'AssetBaruBindingSource
        '
        Me.AssetBaruBindingSource.DataMember = "Master_NewAsset"
        Me.AssetBaruBindingSource.DataSource = Me.DBASSETAUDITV2BindingSource
        '
        'LineShape5
        '
        Me.LineShape5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape5.Name = "LineShape5"
        Me.LineShape5.X1 = -6
        Me.LineShape5.X2 = 1848
        Me.LineShape5.Y1 = 44
        Me.LineShape5.Y2 = 46
        '
        'panelMenu
        '
        Me.panelMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelMenu.BackColor = System.Drawing.Color.CornflowerBlue
        Me.panelMenu.Controls.Add(Me.Label7)
        Me.panelMenu.Controls.Add(Me.btnLogout)
        Me.panelMenu.Controls.Add(Me.lblSite)
        Me.panelMenu.Controls.Add(Me.lblUsername)
        Me.panelMenu.Controls.Add(Me.btnBackup)
        Me.panelMenu.Controls.Add(Me.lbl_PenandaTabTrans)
        Me.panelMenu.Controls.Add(Me.lblLogout)
        Me.panelMenu.Controls.Add(Me.btnValidation)
        Me.panelMenu.Controls.Add(Me.btnMaster)
        Me.panelMenu.Controls.Add(Me.btnReport)
        Me.panelMenu.Controls.Add(Me.btnTransaction)
        Me.panelMenu.ForeColor = System.Drawing.Color.White
        Me.panelMenu.Location = New System.Drawing.Point(0, 0)
        Me.panelMenu.Name = "panelMenu"
        Me.panelMenu.Size = New System.Drawing.Size(1176, 44)
        Me.panelMenu.TabIndex = 99
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label7.Location = New System.Drawing.Point(159, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 2)
        Me.Label7.TabIndex = 104
        Me.Label7.Text = "  "
        '
        'btnLogout
        '
        Me.btnLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.Transparent
        Me.btnLogout.Image = CType(resources.GetObject("btnLogout.Image"), System.Drawing.Image)
        Me.btnLogout.Location = New System.Drawing.Point(1141, 9)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(24, 26)
        Me.btnLogout.TabIndex = 106
        Me.btnLogout.Text = "  "
        '
        'lblSite
        '
        Me.lblSite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSite.AutoSize = True
        Me.lblSite.BackColor = System.Drawing.Color.Transparent
        Me.lblSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSite.ForeColor = System.Drawing.Color.White
        Me.lblSite.Location = New System.Drawing.Point(969, 24)
        Me.lblSite.Name = "lblSite"
        Me.lblSite.Size = New System.Drawing.Size(28, 15)
        Me.lblSite.TabIndex = 105
        Me.lblSite.Text = "Site"
        Me.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUsername
        '
        Me.lblUsername.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.ForeColor = System.Drawing.Color.White
        Me.lblUsername.Location = New System.Drawing.Point(969, 4)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(33, 15)
        Me.lblUsername.TabIndex = 104
        Me.lblUsername.Text = "User"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnBackup
        '
        Me.btnBackup.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnBackup.FlatAppearance.BorderSize = 0
        Me.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBackup.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackup.ForeColor = System.Drawing.Color.White
        Me.btnBackup.Image = CType(resources.GetObject("btnBackup.Image"), System.Drawing.Image)
        Me.btnBackup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBackup.Location = New System.Drawing.Point(580, 0)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.Size = New System.Drawing.Size(142, 44)
        Me.btnBackup.TabIndex = 101
        Me.btnBackup.Text = "  Backup Data"
        Me.btnBackup.UseVisualStyleBackColor = False
        '
        'lbl_PenandaTabTrans
        '
        Me.lbl_PenandaTabTrans.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lbl_PenandaTabTrans.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_PenandaTabTrans.Location = New System.Drawing.Point(179, 44)
        Me.lbl_PenandaTabTrans.Name = "lbl_PenandaTabTrans"
        Me.lbl_PenandaTabTrans.Size = New System.Drawing.Size(140, 2)
        Me.lbl_PenandaTabTrans.TabIndex = 99
        Me.lbl_PenandaTabTrans.Text = "  "
        '
        'lblLogout
        '
        Me.lblLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLogout.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lblLogout.Image = CType(resources.GetObject("lblLogout.Image"), System.Drawing.Image)
        Me.lblLogout.Location = New System.Drawing.Point(1232, 14)
        Me.lblLogout.Name = "lblLogout"
        Me.lblLogout.Size = New System.Drawing.Size(38, 26)
        Me.lblLogout.TabIndex = 99
        Me.lblLogout.Text = "  "
        '
        'btnValidation
        '
        Me.btnValidation.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnValidation.FlatAppearance.BorderSize = 0
        Me.btnValidation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValidation.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidation.ForeColor = System.Drawing.Color.White
        Me.btnValidation.Image = CType(resources.GetObject("btnValidation.Image"), System.Drawing.Image)
        Me.btnValidation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidation.Location = New System.Drawing.Point(435, 0)
        Me.btnValidation.Name = "btnValidation"
        Me.btnValidation.Size = New System.Drawing.Size(142, 44)
        Me.btnValidation.TabIndex = 99
        Me.btnValidation.Text = "      Audit Data"
        Me.btnValidation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidation.UseVisualStyleBackColor = False
        '
        'btnMaster
        '
        Me.btnMaster.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnMaster.FlatAppearance.BorderSize = 0
        Me.btnMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMaster.ForeColor = System.Drawing.Color.White
        Me.btnMaster.Image = CType(resources.GetObject("btnMaster.Image"), System.Drawing.Image)
        Me.btnMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMaster.Location = New System.Drawing.Point(0, 0)
        Me.btnMaster.Margin = New System.Windows.Forms.Padding(6, 3, 3, 3)
        Me.btnMaster.Name = "btnMaster"
        Me.btnMaster.Size = New System.Drawing.Size(142, 44)
        Me.btnMaster.TabIndex = 0
        Me.btnMaster.Text = "Master Data"
        Me.btnMaster.UseVisualStyleBackColor = False
        '
        'btnReport
        '
        Me.btnReport.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnReport.FlatAppearance.BorderSize = 0
        Me.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.ForeColor = System.Drawing.Color.White
        Me.btnReport.Image = CType(resources.GetObject("btnReport.Image"), System.Drawing.Image)
        Me.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReport.Location = New System.Drawing.Point(290, 0)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(142, 44)
        Me.btnReport.TabIndex = 99
        Me.btnReport.Text = "      Report"
        Me.btnReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReport.UseVisualStyleBackColor = False
        '
        'btnTransaction
        '
        Me.btnTransaction.BackColor = System.Drawing.Color.White
        Me.btnTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTransaction.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransaction.ForeColor = System.Drawing.Color.Black
        Me.btnTransaction.Image = CType(resources.GetObject("btnTransaction.Image"), System.Drawing.Image)
        Me.btnTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransaction.Location = New System.Drawing.Point(145, 0)
        Me.btnTransaction.Name = "btnTransaction"
        Me.btnTransaction.Size = New System.Drawing.Size(142, 44)
        Me.btnTransaction.TabIndex = 99
        Me.btnTransaction.Text = "Transaction"
        Me.btnTransaction.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.panelMenu)
        Me.Panel4.Controls.Add(Me.container)
        Me.Panel4.Location = New System.Drawing.Point(1, 2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1179, 669)
        Me.Panel4.TabIndex = 99
        '
        'container
        '
        Me.container.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.container.BackColor = System.Drawing.Color.Maroon
        Me.container.Controls.Add(Me.TabControlTransaction)
        Me.container.Location = New System.Drawing.Point(0, 42)
        Me.container.Name = "container"
        Me.container.Size = New System.Drawing.Size(1179, 622)
        Me.container.TabIndex = 116
        '
        'BackgroundWorkerGetPhysic
        '
        Me.BackgroundWorkerGetPhysic.WorkerReportsProgress = True
        '
        'Master_NewAssetTableAdapter
        '
        Me.Master_NewAssetTableAdapter.ClearBeforeFill = True
        '
        'CompareDataTableAdapter
        '
        Me.CompareDataTableAdapter.ClearBeforeFill = True
        '
        'Trans_AuditAssetTableAdapter
        '
        Me.Trans_AuditAssetTableAdapter.ClearBeforeFill = True
        '
        'NewPhysicAssetBackgroundWorker
        '
        Me.NewPhysicAssetBackgroundWorker.WorkerReportsProgress = True
        '
        'Transaction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1179, 669)
        Me.Controls.Add(Me.Panel4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Transaction"
        Me.Text = "Asset Audit and Monitoring"
        Me.TabControlTransaction.ResumeLayout(False)
        Me.TabAssetBaru.ResumeLayout(False)
        Me.TabAssetBaru.PerformLayout()
        CType(Me.dgvAssetBaru, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewPhysicAssetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBASSETAUDITV2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_ASSET_AUDIT_V2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.TabAssetFisik.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.panelGetPhysicForm.ResumeLayout(False)
        Me.panelGetPhysicForm.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.dgvAssetFisik_ConnectedData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AssetFisikBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.PanelSearch_NotConnected.ResumeLayout(False)
        Me.PanelSearch_NotConnected.PerformLayout()
        CType(Me.dgvAssetFisik_NotConnectedData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.dgvAssetFisik_DeniedData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCompareData.ResumeLayout(False)
        Me.TabCompareData.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.dgvCompareData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompareDataBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.AssetBaruBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelMenu.ResumeLayout(False)
        Me.panelMenu.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.container.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControlTransaction As TabControl
    Friend WithEvents LineShape5 As PowerPacks.LineShape
    Friend WithEvents TabAssetBaru As TabPage
    Friend WithEvents lblPercentage_AssetBaru As Label
    Friend WithEvents lblStatus_AssetBaru As Label
    Friend WithEvents progressBar_AssetBaru As ProgressBar
    Friend WithEvents lbl_formatNewAsset As Label
    Friend WithEvents btnUpload_AssetBaru As Button
    Friend WithEvents btnSaveNewAsset As Button
    Friend WithEvents lblDateTimeNewRegis As Label
    Friend WithEvents TabAssetFisik As TabPage
    Friend WithEvents Panel5 As Panel
    Friend WithEvents lblTotal_AccessDeniedData As Label
    Friend WithEvents lblTotal_NotConnectedData As Label
    Friend WithEvents lblTotal_ConnectedData As Label
    Friend WithEvents lblTotal_All As Label
    Friend WithEvents lbl_ketTotalConOldAsset As Label
    Friend WithEvents lbl_ketTotalDeniedOldAsset As Label
    Friend WithEvents lbl_ketTotalNotConOldAsset As Label
    Friend WithEvents lbl_ketTotalAllOldAsset As Label
    Friend WithEvents btnDownloadPhysicAsset As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents panelGetPhysicForm As Panel
    Friend WithEvents ProgressBarProcessPhysicAsset As ProgressBar
    Friend WithEvents lblShowPleaseWaitGetPhysicAsset As Label
    Friend WithEvents ComboBoxSubAreaField As ComboBox
    Friend WithEvents ComboBoxAreaField As ComboBox
    Friend WithEvents lblSubAreaTitle As Label
    Friend WithEvents lblAreaTitle As Label
    Friend WithEvents ComboBoxCategoryRunDataField As ComboBox
    Friend WithEvents lblRunDataFromTitle As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents dgvAssetFisik_ConnectedData As DataGridView
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents dgvAssetFisik_NotConnectedData As DataGridView
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents dgvAssetFisik_DeniedData As DataGridView
    Friend WithEvents panelMenu As Panel
    Friend WithEvents btnLogout As Label
    Friend WithEvents lblSite As Label
    Friend WithEvents lblUsername As Label
    Friend WithEvents btnBackup As Button
    Friend WithEvents lbl_PenandaTabTrans As Label
    Friend WithEvents lblLogout As Label
    Friend WithEvents btnValidation As Button
    Friend WithEvents btnMaster As Button
    Friend WithEvents btnReport As Button
    Friend WithEvents btnTransaction As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents BackgroundWorkerGetPhysic As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblSubAreaRequired As Label
    Friend WithEvents lblRequiredArea As Label
    Friend WithEvents lblRunDataFromRequired As Label
    Friend WithEvents TabCompareAVADUC As TabPage
    Friend WithEvents TabCompareData As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents sumAllData As Label
    Friend WithEvents lblAllData As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents lblSumNotChecked As Label
    Friend WithEvents lblSumChecked As Label
    Friend WithEvents sumChecked_CompareData As Label
    Friend WithEvents sumNotChecked_CompareData As Label
    Friend WithEvents lblPercent_CompareData As Label
    Friend WithEvents progressBarCompareData As ProgressBar
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents dgvCompareData As DataGridView
    Friend WithEvents lbl_ketMerahCompData As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnRefresh_CompareData As Button
    Friend WithEvents btnShow_CompareData As Button
    Friend WithEvents btnSave_CompareData As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents lblCountDataSesuai_CompareData As Label
    Friend WithEvents lblCountDataMendekatiSesuai_CompareData As Label
    Friend WithEvents lblSumSimilar As Label
    Friend WithEvents lbl_ketHijauCompData As Label
    Friend WithEvents lblCountDataTidakSesuai_CompareData As Label
    Friend WithEvents lblSumBeda As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents btnDownload_ConnectedData As Button
    Friend WithEvents btnSave_ConnectedData As Button
    Friend WithEvents DB_ASSET_AUDIT_V2 As DB_ASSET_AUDIT_V2
    Friend WithEvents DBASSETAUDITV2BindingSource As BindingSource
    Friend WithEvents AssetBaruBindingSource As BindingSource
    Friend WithEvents Master_NewAssetTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Master_NewAssetTableAdapter
    Friend WithEvents CompareDataBindingSource As BindingSource
    Friend WithEvents CompareDataTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.CompareDataTableAdapter
    Friend WithEvents StatusDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents StatusNMDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ServiceTagSNDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemorySizeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemoryQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MonSN1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MonSN2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UPSSNDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AssetFisikBindingSource As BindingSource
    Friend WithEvents Trans_AuditAssetTableAdapter As DB_ASSET_AUDIT_V2TableAdapters.Trans_AuditAssetTableAdapter
    Friend WithEvents StatusDataGridViewCheckBoxColumn2 As DataGridViewCheckBoxColumn
    Friend WithEvents UploadedDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DomainDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ModelDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents OSVersionDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ServiceTagSNDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents MemSN1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemSN2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemSN3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemSN4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN1DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN2DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN3DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel3DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN4DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel4DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MonSN1DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MonModel1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MonSN2DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MonModel2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UPSSNDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents UPSTimeLeftDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents KeteranganDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Panel8 As Panel
    Friend WithEvents comboTransaction_ConnectedData As ComboBox
    Friend WithEvents txtSearchTransaction_ConnectedData As TextBox
    Friend WithEvents btnSearchTransaction_ConnectedData As Button
    Friend WithEvents btnGetComputer As Button
    Friend WithEvents btnSave_NotConnectedData As Button
    Friend WithEvents PanelSearch_NotConnected As Panel
    Friend WithEvents comboField_NotConnected As ComboBox
    Friend WithEvents txtSearch_NotConnected As TextBox
    Friend WithEvents btnSearch_NotConnected As Button
    Friend WithEvents btnDownload_NotConnectedData As Button
    Friend WithEvents Panel9 As Panel
    Friend WithEvents comboTransaction_AccessDenied As ComboBox
    Friend WithEvents txtSearchTransaction_AccessDenied As TextBox
    Friend WithEvents btnSearchTransaction_AccessDenied As Button
    Friend WithEvents btnDownload_DeniedData As Button
    Friend WithEvents btnSave_DeniedData As Button
    Friend WithEvents lblTotalData_Connected As Label
    Friend WithEvents lblTotalData_NotConnected As Label
    Friend WithEvents lblTotalData_AccessDenied As Label
    Friend WithEvents StatusDataGridViewCheckBoxColumn3 As DataGridViewCheckBoxColumn
    Friend WithEvents UploadedDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents KeteranganDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewCheckBoxColumn4 As DataGridViewCheckBoxColumn
    Friend WithEvents UploadedDataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents KeteranganDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Panel10 As Panel
    Friend WithEvents comboSearchAssetBaru As ComboBox
    Friend WithEvents txtSearchAssetBaru As TextBox
    Friend WithEvents btnSeach_AssetBaru As Button
    Friend WithEvents btnDownload_AssetBaru As Button
    Friend WithEvents NewPhysicAssetBindingSource As BindingSource
    Friend WithEvents dgvAssetBaru As DataGridView
    Friend WithEvents StatusDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents UploadedDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ComputerNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DomainDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ModelDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents OSVersionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ServiceTagSNDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemSN1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemSN2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemSN3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemSN4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemCapacity4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MemManufacturer4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel3DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDSN4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDCapacity4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HDDModel4DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents lblTotalData_AssetBaru As Label
    Friend WithEvents NewPhysicAssetBackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents Panel11 As Panel
    Friend WithEvents comboSearch_ComparableData As ComboBox
    Friend WithEvents txtSearch_ComparableData As TextBox
    Friend WithEvents btnSearch_ComparableData As Button
    Friend WithEvents btnDownload_ComparableData As Button
    Friend WithEvents lblTotal_ComparableData As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents container As Panel
End Class
