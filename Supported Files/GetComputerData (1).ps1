﻿    #library form 
    Add-Type -AssemblyName System.Windows.Forms

    #set variable
    $path = Get-Location
    $array = Get-Content -Path @(".\Computer.txt")
    $nl = [Environment]::NewLine
    $array = Get-Content -Path @(".\Location.txt")

    #hanya bisa mengakomodir 4 Memory & 4 HDD
    $ErrorActionPreference = 'Stop'
    $array = Get-Content -Path @(".\Computer.txt")
    $summaryOK="Computer Name;Domain;Model;OS Version;Service Tag/SN;"
    $summaryOK= $summaryOK + "Mem SN 1;Mem Capacity 1;Mem Manufacturer 1;Mem SN 2;Mem Capacity 2;Mem Manufacturer 2;Mem SN 3;Mem Capacity 3;Mem Manufacturer 3;Mem SN 4;Mem Capacity 4;Mem Manufacturer 4;"
    $summaryOK= $summaryOK + "HDD SN 1;HDD Capacity 1;HDD Model 1;HDD SN 2;HDD Capacity 2;HDD Model 2;HDD SN 3;HDD Capacity 3;HDD Model 3;HDD SN 4;HDD Capacity 4;HDD Model 4;Mon SN 1;Mon Model 1;Mon SN 2;Mon Model 2;UPS;Time Left;" + $nl # + "`r`n"
    $summaryNOK = ""
    $summaryDen = ""
    $diskreport = ""
    $dataRow = ""
    $res = ""
    $count = 0
    $counter = 0
    $global:tamppassword = $null
    $global:lanjut = 0
    $global:next = 0
    $global:domainforupslocal = 0
    $global:domainforupscred = 0
    $global:countAD = 0


    #------------------------------------------------------------------------------Form input password pc tujuan----------------------------------------------------------------------------------
     $form = New-Object System.Windows.Forms.Form

    #label title
    $labelTitle = New-Object System.Windows.Forms.Label
    $labelTitle.Text = "Form Autentikasi Target PC"
    $labelTitle.AutoSize = $true
    $labelTitle.Location = New-Object System.Drawing.Size(19, 10) #center uk.35
    $fontTitle = New-Object System.Drawing.Font("Segoe UI", 12, [System.Drawing.FontStyle]::Bold) 
    $labelTitle.Font = $fontTitle 
    $form.Controls.Add($labelTitle)

    #label Field
    $labelField = New-Object System.Windows.Forms.Label
    $labelField.Text = "Input Password PC Tujuan :"
    $labelField.AutoSize = $true
    $labelField.Location = New-Object System.Drawing.Size(20, 40)
    $fontField = New-Object System.Drawing.Font("Segoe UI", 10, [System.Drawing.FontStyle]::Regular) 
    $labelField.Font = $fontField 
    $form.Controls.Add($labelField)

    #field password
    $targetPasswordField = New-Object System.Windows.Forms.TextBox
    $targetPasswordField.Size = New-Object System.Drawing.Size(240,100)
    $targetPasswordField.Location = New-Object System.Drawing.Size(21, 63)
    $fontField2 = New-Object System.Drawing.Font("Segoe UI", 12, [System.Drawing.FontStyle]::Regular) 
    $targetPasswordField.Font = $fontField2 
    $form.Controls.Add($targetPasswordField)

    #Button
    $buttonOK = New-Object System.Windows.Forms.Button
    $buttonOK.Text = "Next"
    $fontBtn = New-Object System.Drawing.Font("Segoe UI", 9, [System.Drawing.FontStyle]::Bold) 
    $buttonOK.Font = $fontBtn
    $buttonOK.Size = New-Object System.Drawing.Size(70,30)
    $buttonOK.Location = New-Object System.Drawing.Size(21, 95)
    $form.Controls.Add($buttonOK)
    Add-Type -AssemblyName System.Windows.Forms
    $form = New-Object System.Windows.Forms.Form

    #label title
    $labelTitle = New-Object System.Windows.Forms.Label
    $labelTitle.Text = "Autentikasi PC Tujuan"
    $labelTitle.AutoSize = $true
    $labelTitle.Location = New-Object System.Drawing.Size(19, 10) #center uk.35
    $fontTitle = New-Object System.Drawing.Font("Segoe UI", 12, [System.Drawing.FontStyle]::Bold) 
    $labelTitle.Font = $fontTitle 
    $form.Controls.Add($labelTitle)

    #label Field
    $labelField = New-Object System.Windows.Forms.Label
    $labelField.Text = "Password Admin PC Tujuan :"
    $labelField.AutoSize = $true
    $labelField.Location = New-Object System.Drawing.Size(20, 40)
    $fontField = New-Object System.Drawing.Font("Segoe UI", 10, [System.Drawing.FontStyle]::Regular) 
    $labelField.Font = $fontField 
    $form.Controls.Add($labelField)

    #field password
    #$targetPasswordField = New-Object System.Windows.Forms.AccessibleEvents
    $targetPasswordField = New-Object Windows.Forms.MaskedTextBox
    $targetPasswordField.PasswordChar = '*'
    $targetPasswordField.Size = New-Object System.Drawing.Size(240,100)
    $targetPasswordField.Location = New-Object System.Drawing.Size(21, 63)
    $fontField2 = New-Object System.Drawing.Font("Segoe UI", 12, [System.Drawing.FontStyle]::Regular) 
    $targetPasswordField.Font = $fontField2 
    $form.Controls.Add($targetPasswordField)

    #Button
    $buttonOK = New-Object System.Windows.Forms.Button
    $buttonOK.Text = "Next"
    $fontBtn = New-Object System.Drawing.Font("Segoe UI", 9, [System.Drawing.FontStyle]::Bold) 
    $buttonOK.Font = $fontBtn
    $buttonOK.Size = New-Object System.Drawing.Size(70,30)
    $buttonOK.Location = New-Object System.Drawing.Size(21, 95)
    $buttonOK.Add_Click({
      $global:tamppassword = $targetPasswordField.text
      $form.Close()
    })
    $Form.Controls.Add($buttonOK)

    $form.Size = New-Object System.Drawing.Size(300,190)
    $CenterScreen = [System.Windows.Forms.FormStartPosition]::CenterScreen
    $form.StartPosition = $CenterScreen
    $form.ShowDialog()
            
    #-----------------------------------------------------------------------------progress bar-----------------------------------------------------------------------------------
    #--------------------------------------------------------------------Area membuat progress bar-------------------------------------------------------------------
      
    ## -- Create The Progress-Bar
    $ObjForm = New-Object System.Windows.Forms.Form
    $ObjForm.Text = "Please wait a moment. . ."
    #$ObjForm.WindowState = 'Maximized'
    $ObjForm.Location = New-Object System.Drawing.Point(200,150)
    $ObjForm.Size = New-Object System.Drawing.Size(500,80)
    $ObjForm.BackColor = "WhiteSmoke"

    $ObjForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::None
    $ObjForm.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen

    ## -- Create The Label
    $ObjLabel = New-Object System.Windows.Forms.Label
    $ObjLabel.Text = "Starting. Please wait ... "
    $ObjLabel.Left = 5
    $ObjLabel.Top = 10
    $ObjLabel.Width = 500 - 20
    $ObjLabel.Height = 15
    $ObjLabel.Font = "Tahoma"
    
    ## -- Add the label to the Form
    $ObjForm.Controls.Add($ObjLabel)

    $PB = New-Object System.Windows.Forms.ProgressBar
    $PB.Name = "PowerShellProgressBar"
    $PB.Value = 0
    $PB.Style="Continuous"

    $System_Drawing_Size = New-Object System.Drawing.Size
    $System_Drawing_Size.Width = 484
    $System_Drawing_Size.Height = 20
    $PB.Size = $System_Drawing_Size
    $PB.Left = 5
    $PB.Top = 40
    [Int]$Percentage = 0
    $ObjForm.Controls.Add($PB)

    ## -- Show the Progress-Bar and Start The PowerShell Script
    $ObjForm.Show() | Out-Null
    $ObjForm.Focus() | Out-NUll
    $ObjLabel.Text = "Starting. Please wait ... "
    $ObjForm.Refresh()
    #Start-Sleep -Seconds 1
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------
    $summaryArrOK = 
    
            function Decode {
                If ($args[0] -is [System.Array]) {
                    [System.Text.Encoding]::ASCII.GetString($args[0])
                }
                Else {
                    "Not Found"
                }
            }
        
    #------------------------------------------------------------------------------Desc Pc Local----------------------------------------------------------------------------------
     function writeTxt(){
            Start-Sleep -Milliseconds 150
            $ObjForm.Close() 
           
            $fileOK = ".\Audit.txt"
            $fileNOK = ".\Audit Not Connected.txt"
            $fileDen = ".\Audit Denied.txt"
            $fileSum = ""
            if ($summaryOK -ne "") 
            {
                $summaryOK > $fileOK
                $fileSum += "- " + $fileOK + $nl #+ "`r`n"
            }
            if ($summaryNOK -ne "") 
            {
                $summaryNOK > $fileNOK
                $fileSum += "- " + $fileNOK + $nl #+ "`r`n"
            }
            if ($summaryDen -ne "") 
            {
                $summaryDen > $fileDen
                $fileSum += "- " + $fileDen + $nl #+ "`r`n"
            }

            [System.Windows.Forms.MessageBox]::Show('Audit Done. File: '+$nl+$fileSum,'Information','OK','Information') 
        }
   #--------------------------------------------------------------------Popup konfirmasi untuk melanjutkan proses get computer jika sudah akses denied domain/pswd sudah ada 5 pc-------------------------------------------------------------------
    function showPopup{      
            $ConfirmationForm = New-Object System.Windows.Forms.Form

            #label title
            $labelTitle = New-Object System.Windows.Forms.Label
            $labelTitle.Text = "Form Confirmation!"
            $labelTitle.AutoSize = $true
            $labelTitle.Location = New-Object System.Drawing.Size(19, 10) #center uk.35
            $fontTitle = New-Object System.Drawing.Font("Segoe UI", 12, [System.Drawing.FontStyle]::Bold) 
            $labelTitle.Font = $fontTitle 
            $ConfirmationForm.Controls.Add($labelTitle)

            #label Field
            $labelField = New-Object System.Windows.Forms.Label
            $stringData = "We found the 5 computers did not match password."
            $stringData = $stringData + "`n" + "Do you want to next process?"
            $labelField.Text =  $stringData
            $labelField.AutoSize = $true
            $labelField.Location = New-Object System.Drawing.Size(20, 40)
            $fontField = New-Object System.Drawing.Font("Segoe UI", 10, [System.Drawing.FontStyle]::Regular) 
            $labelField.Font = $fontField 
            $ConfirmationForm.Controls.Add($labelField)

            #ButtonYes - Lanjut
            $Nextbutton = New-Object System.Windows.Forms.Button
            $Nextbutton.Text = "Yes"
            $NextFont = New-Object System.Drawing.Font("Segoe UI", 9, [System.Drawing.FontStyle]::Bold) 
            $Nextbutton.Font = $NextFont
            $Nextbutton.Size = New-Object System.Drawing.Size(70,30)
            $Nextbutton.Location = New-Object System.Drawing.Size(219, 95)
            $Nextbutton.Add_Click({
              $global:lanjut = 1
              $ConfirmationForm.Close()

            })
            $ConfirmationForm.Controls.Add($Nextbutton)
    

            #ButtonNo - Stop
            $buttonOK = New-Object System.Windows.Forms.Button
            $buttonOK.Text = "No"
            $fontBtn = New-Object System.Drawing.Font("Segoe UI", 9, [System.Drawing.FontStyle]::Bold) 
            $buttonOK.Font = $fontBtn
            $buttonOK.Size = New-Object System.Drawing.Size(70,30)
            $buttonOK.Location = New-Object System.Drawing.Size(290, 95)
            $buttonOK.Add_Click({
              $global:lanjut = 0
              $ConfirmationForm.Close()
            })
            $ConfirmationForm.Controls.Add($buttonOK)

            $ConfirmationForm.Size = New-Object System.Drawing.Size(400,185)
            $CenterScreen = [System.Windows.Forms.FormStartPosition]::CenterScreen
            $ConfirmationForm.StartPosition = $CenterScreen
            $ConfirmationForm.ShowDialog() 
            return $global:lanjut       
    }

    #-----------------------------------------------------------------------------------------Main Part-----------------------------------------------------------------------------------
    $comp = Get-WmiObject Win32_computersystem | Select-Object name
    $dataCompName = $comp -split '='     
    $dataCompName1 = $dataCompName[1] -replace '[}]'
    $now = 0
    $total = 0
    $nextuntillast = 0

    $position = 0
    $keterangan = ""
    foreach($item in $array)
    {
        $position += 1
        $keterangan += "`r`n"
        $keterangan += "PCName ke-" + $position + ": " + $item
        $total += 1
        #Write-Output "`r`n"
        #Write-Output $global:countAD
        $counter += 1
        $Percentage = ($counter/$array.Count)*100
        $PB.Value = $percentage
        $ObjLabel.Text = "Getting Asset Computer... "
        $PB.Value = $percentage
        $ObjLabel.Text = "Getting Asset Computer... (" + $Percentage + "%)"
        $ObjForm.Refresh()


        if($global:countAD -eq 5 -and $nextuntillast -eq 0){           
            $global:next = showPopup
            if ($next -eq 1){
                $nextuntillast = 1
            }else{               
              #writeTxt
                Start-Sleep -Milliseconds 150
                $ObjForm.Close() 
           
                $fileOK = ".\Audit.txt"
                $fileNOK = ".\Audit Not Connected.txt"
                $fileDen = ".\Audit Denied.txt"
                $fileSum = ""
                if ($summaryOK -ne "") 
                {
                    $summaryOK > $fileOK
                    $fileSum += "- " + $fileOK + $nl #+ "`r`n"
                }
                if ($summaryNOK -ne "") 
                {
                    $summaryNOK > $fileNOK
                    $fileSum += "- " + $fileNOK + $nl #+ "`r`n"
                }
                if ($summaryDen -ne "") 
                {
                    $summaryDen > $fileDen
                    $fileSum += "- " + $fileDen + $nl #+ "`r`n"
                }
              #Stop-Process -Name $item
              #Get-Process -Name $item | Stop-Process -Force
              break
            }
           
        }if($global:countAD -lt 5 -or (($global:countAD -eq 5 -or $global:countAD -gt 5) -and $next -eq 1 -and $nextuntillast -eq 1)){        
            if ($item -eq $dataCompName1) #jika item = pc name local
            {
                if(!(Test-Connection -Cn $item -BufferSize 16 -Count 1 -ea 0 -quiet))
                {
                    $summaryNOK = $summaryNOK + $item + ";" + "Not Connected; " +$nl
                    $keterangan += ";Not Connected;" 
                
                }    
                else
                {

                    $newDen = 1
                    $dataDomain = ""
                    $dataModel = ""
                    $dataCaption = ""
                    $dataServiceTag = ""
                    $dataMemManufacturer =  ""
                    $dataMemSN =  ""
                    $dataMemCapacity = ""  
                    $dataHDDModel = "" 
                    $dataHDDSN = "" 
                    $dataHDDSize = ""  
                    $dataMonModel = "" 
                    $dataMonSN = "" 

                    #Domain, Model computer
                    try
                    {
                        $res = wmic /NODE:"$item" computersystem list brief /format:list
                        $dataDomain = $res | select-string -pattern "Domain="
                        $dataDomain1 = $dataDomain -split '='            
                        $dataModel = $res | select-string -pattern "Model="
                        $dataModel1 = $dataModel -split '='      
                        $summaryOK = $summaryOK + $item + ";" + $dataDomain1[1] + ";" + $dataModel1[1] + ";" 
                        $keterangan += ";Connected - Domain & Model Computer;"                       
                    }
                    catch 
                    {
                        $keterangan += ";Access Denied - Domain & Model Computer;"
                        $global:countAD +=1
                        $global:domainforupslocal = 1
                        if ($newDen -eq 1) 
                        {                   
                           
                            if ($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Domain & Model Computer" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Domain & Model Computer" + ";"
                            }
                                           
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Domain & Model Computer" + ";"}
                    }        
           
                    # Windows Version
                    try
                    {
                        $res = Get-WmiObject Win32_OperatingSystem -ComputerName $item | select-object version
                        $dataCaption = $res.version             
                        $summaryOK = $summaryOK +  $dataCaption + ";"
                        $keterangan += ";Connected - Windows Version;"
                    }
                    catch 
                    { 
                        $keterangan += ";Access Denied - Windows Version;"                       
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Windows Version" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Windows Version" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {
                        $summaryDen = $summaryDen +"Access Denied - Windows Version" + ";"}
                    }        

                    # Service Tag / SN
                    try
                    {
                        $res = Get-WmiObject Win32_Bios -ComputerName $item | select-object serialnumber
                        $dataServiceTag = $res.serialnumber             
                        $summaryOK = $summaryOK +  $dataServiceTag + ";"
                        $keterangan += ";Connected - Service Tag / SN;"
                    }
                    catch 
                    {     
                        $keterangan += "Access Denied - Service Tag / SN"                   
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Service Tag" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Service Tag" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Service Tag" + ";"}
                    }        
                    
                    # Memory
                    try
                    {
                        $res = Get-WmiObject Win32_PhysicalMemory -ComputerName $item
                        $count=0
                        foreach($key in $res)
                        {
                            $dataMemManufacturer =  $key.manufacturer
                            $dataMemSN =  $key.SerialNumber.trim()
                            $dataMemCapacity = $key.Capacity/1024/1024/1024     
                            $dataMemCapacity = $dataMemCapacity.ToString() + " GB"      
                            $summaryOK = $summaryOK +  $dataMemSN + ";" +  $dataMemCapacity + ";" +  $dataMemManufacturer + ";"
                            $count = $count + 1
                        }   
                
                        for ($i=$count;$i -le 3;$i++)
                        {
                        $summaryOK = $summaryOK +";;;"
                        }    
                        $keterangan += ";Connected - Memory;"     
                    }
                    catch 
                    {
                        $keterangan += ";Access Denied - Memory;"                        
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - Memory" + ";"
                            }else{
                                 $summaryDen = $summaryDen + $item + ";" + "Access Denied - Memory" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Memory" + ";"}
                    }        
            
                    # HDD
                    try
                    {
                        $keterangan += ";Connected - HDD;"
                        $res = Get-WmiObject Win32_DiskDrive -ComputerName $item -filter "MediaType='Fixed hard disk media'" | select-object model,serialnumber,size
                        $count=0
                        foreach($key in $res)
                        {
                            $dataHDDModel = $key.model
                            $dataHDDSN =  $key.serialnumber.trim()
                            $dataHDDSize =  $key.size/1024/1024/1024
                            if ($dataHDDSize -le 80) {$dataHDDSize = "80 GB"}
                            elseif ($dataHDDSize -le 120) {$dataHDDSize = "120 GB"}
                            elseif ($dataHDDSize -le 250) {$dataHDDSize = "250 GB"}
                            elseif ($dataHDDSize -le 500) {$dataHDDSize = "500 GB"}
                            elseif ($dataHDDSize -le 1000) {$dataHDDSize = "1000 GB"}
                            elseif ($dataHDDSize -le 2000) {$dataHDDSize = "2000 GB"}
                            elseif ($dataHDDSize -le 3000) {$dataHDDSize = "3000 GB"}                
                            elseif ($dataHDDSize -le 4000) {$dataHDDSize = "4000 GB"}
                            $summaryOK = $summaryOK + $dataHDDSN + ";" +  $dataHDDSize + ";"+  $dataHDDModel  + ";"
                            $count = $count + 1
                        }
                        for ($i=$count;$i -le 3;$i++)
                        {
                        $summaryOK = $summaryOK +";;;"
                        }                  
                
                    }
                    catch 
                    {            
                        $keterangan += ";Access Denied - HDD;"            
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - HDD" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - HDD" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - HDD" + ";"}
                    }        

                    # Monitor
                    try
                    {                       
                        $keterangan += ";Connected - Monitor;"
                        $Results = gwmi -computername $item WmiMonitorID -Namespace root\wmi | 
                        ForEach-Object {
                            New-Object PSObject -Property @{
                                ComputerName = $env:ComputerName
                                Active = $Monitor.Active
                                Manufacturer = ($_.ManufacturerName -notmatch 0 | foreach {[char]$_}) -join ""
                                UserFriendlyName = Decode $_.userfriendlyname -notmatch 0 
                                SerialNumber = Decode $_.SerialNumberID -notmatch 0 
                                WeekOfManufacture = $Monitor.WeekOfManufacture
                                YearOfManufacture = $Monitor.WeekOfManufacture
                        
                            }
                        }
                       
                        $count=0

                        foreach($result in $Results)
                        {
                            $sn = $Result.SerialNumber.Trim([char]$null)
                            $dataMonModel = $Result.UserFriendlyName.Trim([char]$null)
                            $dataMonSN = $Result.SerialNumber.Substring($sn.Length -7, 7)
                    
                            $summaryOK = $summaryOK + $dataMonSN + ";" +  $dataMonModel + ";"
                            $count = $count + 1
                        }

                        for ($i=$count;$i -le 1;$i++)
                        {
                        $summaryOK = $summaryOK +";;" #tes disini
                        }                                                 
                
                    }
                    catch 
                    {                             
                        $keterangan += ";Access Denied - Monitor;"
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - Monitor" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Monitor" + ";"
                            }
                                                
                            $summaryOK = $summaryOK +";;;;" #tes disini
                        } 
                        else 
                        {
                        $summaryOK = $summaryOK
                        $summaryDen = $summaryDen +"Access Denied - Monitor" + ";"
                        }
                    }
            
                    # UPS    
                    if($dataDomain -eq ""){
                        $summaryOK = $summaryOK
                        
                    }else{
                        $summaryOK += "`r`n"
                    }                                    
                    
                 }
               }else{
               #------------------------------------------------------------------------------Target PC wif Credential----------------------------------------------------------------------------------
               
               $compName = $item #tamp nama komputer
               $pswdDestination = $global:tamppassword
               $LAdmin = $compName + "\Administrator"
               $LPassword = ConvertTo-SecureString $pswdDestination -AsPlainText -Force
               $Credentials = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $LAdmin, $LPassword
                           
               $global:domainforups = 0
               if(!(Test-Connection -Cn $item -BufferSize 16 -Count 1 -ea 0 -quiet))
                {
                    $summaryNOK = $summaryNOK + $item + ";" + "Not Connected; " +$nl
                    $keterangan += ";Not Connected;"
                }    
                else
                {
                    $newDen = 1
                    $dataDomain = ""
                    $dataModel = ""
                    $dataCaption = ""
                    $dataServiceTag = ""
                    $dataMemManufacturer =  ""
                    $dataMemSN =  ""
                    $dataMemCapacity = ""  
                    $dataHDDModel = "" 
                    $dataHDDSN = "" 
                    $dataHDDSize = ""  
                    $dataMonModel = "" 
                    $dataMonSN = "" 

                    #Domain, Model computer
                    try
                    {                        
                        $resDomain = Get-WmiObject -ComputerName $item Win32_computersystem -Credential $Credentials | Select-Object domain
                        $resModel = Get-WmiObject -ComputerName $item Win32_computersystem -Credential $Credentials | Select-Object model                        
                        $dataDomain = $resDomain
                        $dataDomain1 = $dataDomain -split '='   
                        $dataDomain = $dataDomain1[1] -replace '[}]'
                
                        
                        $dataModel = $resModel
                        $dataModel1 = $dataModel -split '='     
                        $dataModel = $dataModel1[1] -replace '[}]'
                        $summaryOK = $summaryOK + $item + ";" + $dataDomain + ";" + $dataModel + ";"
                        $keterangan += ";Connected - Domain & Model Computer;"
                        
                    }
                    catch 
                    {
                        $keterangan += ";Access Denied - Domain & Model Domain;"
                        $global:countAD +=1
                        $global:domainforupscred = 1
                        if ($newDen -eq 1) 
                        {                  
                            if ($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Domain & Model Computer" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Domain & Model Computer" + ";"
                            }
                                           
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Domain & Model Computer" + ";"}
                    }        
           
                    # Windows Version
                    try
                    {
                        $res = Get-WmiObject Win32_OperatingSystem -ComputerName $item -Credential $Credentials | select-object version
                        $dataCaption = $res.version             
                        $summaryOK = $summaryOK +  $dataCaption + ";"
                        $keterangan += ";Connected - Windows Version;"
                    }
                    catch 
                    {
                        $keterangan += ";Access Denied - Windows Version;"
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Windows Version" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Windows Version" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Windows Version" + ";"}
                    }        

                    # Service Tag / SN
                    try
                    {
                        $res = Get-WmiObject Win32_Bios -ComputerName $item -Credential $Credentials | select-object serialnumber
                        $dataServiceTag = $res.serialnumber             
                        $summaryOK = $summaryOK +  $dataServiceTag + ";"
                        $keterangan += ";Connected - Service Tag / SN;"
                    }
                    catch 
                    {                        
                        $keterangan += ";Access Denied - Service Tag / SN;"
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen + "`r`n" + $item + ";" + "Access Denied - Service Tag" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Service Tag" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Service Tag" + ";"}
                    }        
                    
                    # Memory
                    try
                    {
                        $res = Get-WmiObject Win32_PhysicalMemory -ComputerName $item -Credential $Credentials
                        $count=0
                        foreach($key in $res)
                        {
                            $dataMemManufacturer =  $key.manufacturer
                            $dataMemSN =  $key.SerialNumber.trim()
                            $dataMemCapacity = $key.Capacity/1024/1024/1024     
                            $dataMemCapacity = $dataMemCapacity.ToString() + " GB"      
                            $summaryOK = $summaryOK +  $dataMemSN + ";" +  $dataMemCapacity + ";" +  $dataMemManufacturer + ";"
                            $count = $count + 1
                        }   
                
                        for ($i=$count;$i -le 3;$i++)
                        {
                        $summaryOK = $summaryOK +";;;"
                        }       
                        $keterangan += ";Connected - Memory;"  
                    }
                    catch 
                    {       
                        $keterangan += ";Access Denied - Memory;"                 
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - Memory" + ";"
                            }else{
                                 $summaryDen = $summaryDen + $item + ";" + "Access Denied - Memory" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - Memory" + ";"}
                    }        
            
                    # HDD
                    try
                    {
                        $res = Get-WmiObject Win32_DiskDrive -ComputerName $item -filter "MediaType='Fixed hard disk media'" -Credential $Credentials | select-object model,serialnumber,size 
                        $count=0
                        foreach($key in $res)
                        {
                            $dataHDDModel = $key.model
                            $dataHDDSN =  $key.serialnumber.trim()
                            $dataHDDSize =  $key.size/1024/1024/1024
                            if ($dataHDDSize -le 80) {$dataHDDSize = "80 GB"}
                            elseif ($dataHDDSize -le 120) {$dataHDDSize = "120 GB"}
                            elseif ($dataHDDSize -le 250) {$dataHDDSize = "250 GB"}
                            elseif ($dataHDDSize -le 500) {$dataHDDSize = "500 GB"}
                            elseif ($dataHDDSize -le 1000) {$dataHDDSize = "1000 GB"}
                            elseif ($dataHDDSize -le 2000) {$dataHDDSize = "2000 GB"}
                            elseif ($dataHDDSize -le 3000) {$dataHDDSize = "3000 GB"}                
                            elseif ($dataHDDSize -le 4000) {$dataHDDSize = "4000 GB"}
                            $summaryOK = $summaryOK + $dataHDDSN + ";" +  $dataHDDSize + ";"+  $dataHDDModel  + ";"
                            $count = $count + 1
                        }
                        for ($i=$count;$i -le 3;$i++)
                        {
                        $summaryOK = $summaryOK +";;;"
                        }   
                        $keterangan += ";Connected - HDD;"              
                
                    }
                    catch 
                    {                        
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - HDD" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - HDD" + ";"
                            }
                    
                            $newDen = 0
                        } 
                        else 
                        {$summaryDen = $summaryDen +"Access Denied - HDD" + ";"}
                        $keterangan += ";Access Denied - HDD;"
                    }        

                    # Monitor
                    try
                    {                        
                        $Results = gwmi -computername $item WmiMonitorID -Namespace root\wmi -Credential $Credentials | 
                        ForEach-Object {
                            New-Object PSObject -Property @{
                                ComputerName = $env:ComputerName
                                Active = $Monitor.Active
                                Manufacturer = ($_.ManufacturerName -notmatch 0 | foreach {[char]$_}) -join ""
                                UserFriendlyName = Decode $_.userfriendlyname -notmatch 0 
                                SerialNumber = Decode $_.SerialNumberID -notmatch 0 
                                WeekOfManufacture = $Monitor.WeekOfManufacture
                                YearOfManufacture = $Monitor.WeekOfManufacture
                        
                            }
                        }
                        #Write-host $Results

                        $count=0

                        foreach($result in $Results)
                        {
                            $sn = $Result.SerialNumber.Trim([char]$null)
                            $dataMonModel = $Result.UserFriendlyName.Trim([char]$null)
                            $dataMonSN = $Result.SerialNumber.Substring($sn.Length -7, 7)
                    
                            $summaryOK = $summaryOK + $dataMonSN + ";" +  $dataMonModel + ";"
                            $count = $count + 1
                        }

                        for ($i=$count;$i -le 1;$i++)
                        {
                        $summaryOK = $summaryOK +";;" #tes disini
                        }  
                        $keterangan += ";Connected - Monitor;"                       
                
                    }
                    catch 
                    {                           
                        if ($newDen -eq 1) 
                        {
                            if($summaryDen -ne ""){
                                $summaryDen = $summaryDen +"`r`n" + $item + ";" + "Access Denied - Monitor" + ";"
                            }else{
                                $summaryDen = $summaryDen + $item + ";" + "Access Denied - Monitor" + ";"
                            }
                                                
                            $summaryOK = $summaryOK +";;;;" #tes disini
                        } 
                        else 
                        {
                        $summaryOK = $summaryOK
                        $summaryDen = $summaryDen +"Access Denied - Monitor" + ";"
                        }
                        $keterangan += ";Access Denied - Monitor;"
                    }
            
                    # UPS
                    if($dataDomain -eq ""){
                        $summaryOK = $summaryOK
                    }else{
                        $summaryOK += "`r`n" 
                    }
                                        
                                             
                }
            }
            if($total -eq $array.Count){
                #
                      Start-Sleep -Milliseconds 150
                        $ObjForm.Close() 
           
                        $fileOK = ".\Audit.txt"
                        $fileNOK = ".\Audit Not Connected.txt"
                        $fileDen = ".\Audit Denied.txt"
                        $fileSum = ""
                        if ($summaryOK -ne "") 
                        {
                            $summaryOK > $fileOK
                            $fileSum += "- " + $fileOK + $nl #+ "`r`n"
                        }
                        if ($summaryNOK -ne "") 
                        {
                            $summaryNOK > $fileNOK
                            $fileSum += "- " + $fileNOK + $nl #+ "`r`n"
                        }
                        if ($summaryDen -ne "") 
                        {
                            $summaryDen > $fileDen
                            $fileSum += "- " + $fileDen + $nl #+ "`r`n"
                        }
                #
            }
          }
        Write-Output "`r`n"
        Write-Output $keterangan
        $keterangan = ""
        #------------------------------------------------------------------------------PC Local----------------------------------------------------------------------------------
        } #close kurawal dari foreach