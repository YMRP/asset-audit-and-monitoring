﻿Imports System.Threading
Public NotInheritable Class SplashScreen

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        For i = 1 To 100
            Thread.Sleep(15)
            BackgroundWorker1.WorkerReportsProgress = True
            BackgroundWorker1.ReportProgress(i)
        Next
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        ProgressBarSplashScreen.Value = e.ProgressPercentage
        ProgressBarSplashScreen.ForeColor = Color.FromArgb(255, 2, 43, 58)
        Label2.Text = e.ProgressPercentage.ToString & "%"
        If Label2.Text = "100%" Then
            Login.Show()
            Me.Close()
        End If
    End Sub

    Private Sub splashScreen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        BackgroundWorker1.RunWorkerAsync()
        Dim nextYear As Integer = Format(Now, "yyyy") + 1 'set copyright
        label_copyright.Text = "Copyright @" & Format(Now, "yyyy") & "-" & nextYear & " Djarum-SCM. All rights reserved."


    End Sub

    Private Sub splashScreen_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        Dim d As New System.Drawing.Drawing2D.LinearGradientBrush _
            (Me.ClientRectangle, Color.FromArgb(255, 2, 43, 58), Color.FromArgb(255, 31, 122, 140), Drawing2D.LinearGradientMode.ForwardDiagonal)
        e.Graphics.FillRectangle(d, Me.ClientRectangle)
    End Sub


End Class
